package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipaudi;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipaudi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipaudiRepository extends JpaRepository<Tipaudi, Long> {

}
