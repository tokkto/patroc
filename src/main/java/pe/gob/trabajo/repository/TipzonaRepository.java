package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipzona;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipzona entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipzonaRepository extends JpaRepository<Tipzona, Long> {

}
