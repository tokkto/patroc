package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Infosoli;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Infosoli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfosoliRepository extends JpaRepository<Infosoli, Long> {

}
