package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Dettipprov;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Dettipprov entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DettipprovRepository extends JpaRepository<Dettipprov, Long> {

}
