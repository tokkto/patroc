package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Calidenu;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Calidenu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CalidenuRepository extends JpaRepository<Calidenu, Long> {

}
