package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Denunte;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Denunte entity.
 */
public interface DenunteSearchRepository extends ElasticsearchRepository<Denunte, Long> {
}
