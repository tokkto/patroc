package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Materia;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Materia entity.
 */
public interface MateriaSearchRepository extends ElasticsearchRepository<Materia, Long> {
}
