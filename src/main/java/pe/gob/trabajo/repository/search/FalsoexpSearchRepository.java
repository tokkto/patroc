package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Falsoexp;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Falsoexp entity.
 */
public interface FalsoexpSearchRepository extends ElasticsearchRepository<Falsoexp, Long> {
}
