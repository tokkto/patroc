package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Calidenu;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Calidenu entity.
 */
public interface CalidenuSearchRepository extends ElasticsearchRepository<Calidenu, Long> {
}
