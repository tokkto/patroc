package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Motfin;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Motfin entity.
 */
public interface MotfinSearchRepository extends ElasticsearchRepository<Motfin, Long> {
}
