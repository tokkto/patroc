package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipdiligenc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipdiligenc entity.
 */
public interface TipdiligencSearchRepository extends ElasticsearchRepository<Tipdiligenc, Long> {
}
