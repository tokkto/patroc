package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Infosoli;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Infosoli entity.
 */
public interface InfosoliSearchRepository extends ElasticsearchRepository<Infosoli, Long> {
}
