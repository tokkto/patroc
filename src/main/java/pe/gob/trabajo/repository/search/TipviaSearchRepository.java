package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipvia;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipvia entity.
 */
public interface TipviaSearchRepository extends ElasticsearchRepository<Tipvia, Long> {
}
