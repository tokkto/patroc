package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Datdenu;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Datdenu entity.
 */
public interface DatdenuSearchRepository extends ElasticsearchRepository<Datdenu, Long> {
}
