package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Motidenun;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Motidenun entity.
 */
public interface MotidenunSearchRepository extends ElasticsearchRepository<Motidenun, Long> {
}
