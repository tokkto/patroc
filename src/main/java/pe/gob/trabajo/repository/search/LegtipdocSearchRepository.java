package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Legtipdoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Legtipdoc entity.
 */
public interface LegtipdocSearchRepository extends ElasticsearchRepository<Legtipdoc, Long> {
}
