package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipdocexp;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipdocexp entity.
 */
public interface TipdocexpSearchRepository extends ElasticsearchRepository<Tipdocexp, Long> {
}
