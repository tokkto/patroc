package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipaudi;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipaudi entity.
 */
public interface TipaudiSearchRepository extends ElasticsearchRepository<Tipaudi, Long> {
}
