package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Califica;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Califica entity.
 */
public interface CalificaSearchRepository extends ElasticsearchRepository<Califica, Long> {
}
