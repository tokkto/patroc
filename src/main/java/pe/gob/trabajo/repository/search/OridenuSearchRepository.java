package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Oridenu;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Oridenu entity.
 */
public interface OridenuSearchRepository extends ElasticsearchRepository<Oridenu, Long> {
}
