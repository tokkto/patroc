package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipresoluc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipresoluc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipresolucRepository extends JpaRepository<Tipresoluc, Long> {

}
