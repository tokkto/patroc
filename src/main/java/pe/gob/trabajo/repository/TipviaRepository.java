package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipvia;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipvia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipviaRepository extends JpaRepository<Tipvia, Long> {

}
