package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipdocexp;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipdocexp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipdocexpRepository extends JpaRepository<Tipdocexp, Long> {

}
