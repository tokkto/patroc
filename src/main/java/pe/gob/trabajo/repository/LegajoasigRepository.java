package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Legajoasig;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Legajoasig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LegajoasigRepository extends JpaRepository<Legajoasig, Long> {
    @Query("select new map(legajoasig.legajo.id as id_legajo, legajoasig.legajo.tFecreg as tFecreg) " + 
        "from Legajoasig legajoasig where legajoasig.id=?1 and legajoasig.nFlgactivo = true")
   Legajoasig findLegajoByID(Long id);

}
