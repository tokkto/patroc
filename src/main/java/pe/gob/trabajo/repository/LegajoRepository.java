package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Legajo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Legajo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LegajoRepository extends JpaRepository<Legajo, Long> {

}
