package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipnotif;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipnotif entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipnotifRepository extends JpaRepository<Tipnotif, Long> {

}
