package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Denunte;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Denunte entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DenunteRepository extends JpaRepository<Denunte, Long> {

}
