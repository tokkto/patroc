package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Oridenu;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Oridenu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OridenuRepository extends JpaRepository<Oridenu, Long> {

}
