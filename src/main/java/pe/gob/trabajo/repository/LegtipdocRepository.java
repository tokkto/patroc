package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Legtipdoc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Legtipdoc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LegtipdocRepository extends JpaRepository<Legtipdoc, Long> {

}
