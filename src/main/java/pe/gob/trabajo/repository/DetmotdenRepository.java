package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Detmotden;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Detmotden entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetmotdenRepository extends JpaRepository<Detmotden, Long> {

}
