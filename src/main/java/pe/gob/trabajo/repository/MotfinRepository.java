package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Motfin;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Motfin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotfinRepository extends JpaRepository<Motfin, Long> {

}
