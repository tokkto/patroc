package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipresconc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipresconc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipresconcRepository extends JpaRepository<Tipresconc, Long> {

}
