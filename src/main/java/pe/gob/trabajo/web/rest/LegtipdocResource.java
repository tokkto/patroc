package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Legtipdoc;

import pe.gob.trabajo.repository.LegtipdocRepository;
import pe.gob.trabajo.repository.search.LegtipdocSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Legtipdoc.
 */
@RestController
@RequestMapping("/api")
public class LegtipdocResource {

    private final Logger log = LoggerFactory.getLogger(LegtipdocResource.class);

    private static final String ENTITY_NAME = "legtipdoc";

    private final LegtipdocRepository legtipdocRepository;

    private final LegtipdocSearchRepository legtipdocSearchRepository;

    public LegtipdocResource(LegtipdocRepository legtipdocRepository, LegtipdocSearchRepository legtipdocSearchRepository) {
        this.legtipdocRepository = legtipdocRepository;
        this.legtipdocSearchRepository = legtipdocSearchRepository;
    }

    /**
     * POST  /legtipdocs : Create a new legtipdoc.
     *
     * @param legtipdoc the legtipdoc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new legtipdoc, or with status 400 (Bad Request) if the legtipdoc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/legtipdocs")
    @Timed
    public ResponseEntity<Legtipdoc> createLegtipdoc(@Valid @RequestBody Legtipdoc legtipdoc) throws URISyntaxException {
        log.debug("REST request to save Legtipdoc : {}", legtipdoc);
        if (legtipdoc.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new legtipdoc cannot already have an ID")).body(null);
        }
        Legtipdoc result = legtipdocRepository.save(legtipdoc);
        legtipdocSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/legtipdocs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /legtipdocs : Updates an existing legtipdoc.
     *
     * @param legtipdoc the legtipdoc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated legtipdoc,
     * or with status 400 (Bad Request) if the legtipdoc is not valid,
     * or with status 500 (Internal Server Error) if the legtipdoc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/legtipdocs")
    @Timed
    public ResponseEntity<Legtipdoc> updateLegtipdoc(@Valid @RequestBody Legtipdoc legtipdoc) throws URISyntaxException {
        log.debug("REST request to update Legtipdoc : {}", legtipdoc);
        if (legtipdoc.getId() == null) {
            return createLegtipdoc(legtipdoc);
        }
        Legtipdoc result = legtipdocRepository.save(legtipdoc);
        legtipdocSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, legtipdoc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /legtipdocs : get all the legtipdocs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of legtipdocs in body
     */
    @GetMapping("/legtipdocs")
    @Timed
    public List<Legtipdoc> getAllLegtipdocs() {
        log.debug("REST request to get all Legtipdocs");
        return legtipdocRepository.findAll();
        }

    /**
     * GET  /legtipdocs/:id : get the "id" legtipdoc.
     *
     * @param id the id of the legtipdoc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the legtipdoc, or with status 404 (Not Found)
     */
    @GetMapping("/legtipdocs/{id}")
    @Timed
    public ResponseEntity<Legtipdoc> getLegtipdoc(@PathVariable Long id) {
        log.debug("REST request to get Legtipdoc : {}", id);
        Legtipdoc legtipdoc = legtipdocRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(legtipdoc));
    }

    /**
     * DELETE  /legtipdocs/:id : delete the "id" legtipdoc.
     *
     * @param id the id of the legtipdoc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/legtipdocs/{id}")
    @Timed
    public ResponseEntity<Void> deleteLegtipdoc(@PathVariable Long id) {
        log.debug("REST request to delete Legtipdoc : {}", id);
        legtipdocRepository.delete(id);
        legtipdocSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/legtipdocs?query=:query : search for the legtipdoc corresponding
     * to the query.
     *
     * @param query the query of the legtipdoc search
     * @return the result of the search
     */
    @GetMapping("/_search/legtipdocs")
    @Timed
    public List<Legtipdoc> searchLegtipdocs(@RequestParam String query) {
        log.debug("REST request to search Legtipdocs for query {}", query);
        return StreamSupport
            .stream(legtipdocSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
