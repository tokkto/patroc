package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipaudi;

import pe.gob.trabajo.repository.TipaudiRepository;
import pe.gob.trabajo.repository.search.TipaudiSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipaudi.
 */
@RestController
@RequestMapping("/api")
public class TipaudiResource {

    private final Logger log = LoggerFactory.getLogger(TipaudiResource.class);

    private static final String ENTITY_NAME = "tipaudi";

    private final TipaudiRepository tipaudiRepository;

    private final TipaudiSearchRepository tipaudiSearchRepository;

    public TipaudiResource(TipaudiRepository tipaudiRepository, TipaudiSearchRepository tipaudiSearchRepository) {
        this.tipaudiRepository = tipaudiRepository;
        this.tipaudiSearchRepository = tipaudiSearchRepository;
    }

    /**
     * POST  /tipaudis : Create a new tipaudi.
     *
     * @param tipaudi the tipaudi to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipaudi, or with status 400 (Bad Request) if the tipaudi has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipaudis")
    @Timed
    public ResponseEntity<Tipaudi> createTipaudi(@Valid @RequestBody Tipaudi tipaudi) throws URISyntaxException {
        log.debug("REST request to save Tipaudi : {}", tipaudi);
        if (tipaudi.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipaudi cannot already have an ID")).body(null);
        }
        Tipaudi result = tipaudiRepository.save(tipaudi);
        tipaudiSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipaudis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipaudis : Updates an existing tipaudi.
     *
     * @param tipaudi the tipaudi to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipaudi,
     * or with status 400 (Bad Request) if the tipaudi is not valid,
     * or with status 500 (Internal Server Error) if the tipaudi couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipaudis")
    @Timed
    public ResponseEntity<Tipaudi> updateTipaudi(@Valid @RequestBody Tipaudi tipaudi) throws URISyntaxException {
        log.debug("REST request to update Tipaudi : {}", tipaudi);
        if (tipaudi.getId() == null) {
            return createTipaudi(tipaudi);
        }
        Tipaudi result = tipaudiRepository.save(tipaudi);
        tipaudiSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipaudi.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipaudis : get all the tipaudis.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipaudis in body
     */
    @GetMapping("/tipaudis")
    @Timed
    public List<Tipaudi> getAllTipaudis() {
        log.debug("REST request to get all Tipaudis");
        return tipaudiRepository.findAll();
        }

    /**
     * GET  /tipaudis/:id : get the "id" tipaudi.
     *
     * @param id the id of the tipaudi to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipaudi, or with status 404 (Not Found)
     */
    @GetMapping("/tipaudis/{id}")
    @Timed
    public ResponseEntity<Tipaudi> getTipaudi(@PathVariable Long id) {
        log.debug("REST request to get Tipaudi : {}", id);
        Tipaudi tipaudi = tipaudiRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipaudi));
    }

    /**
     * DELETE  /tipaudis/:id : delete the "id" tipaudi.
     *
     * @param id the id of the tipaudi to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipaudis/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipaudi(@PathVariable Long id) {
        log.debug("REST request to delete Tipaudi : {}", id);
        tipaudiRepository.delete(id);
        tipaudiSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipaudis?query=:query : search for the tipaudi corresponding
     * to the query.
     *
     * @param query the query of the tipaudi search
     * @return the result of the search
     */
    @GetMapping("/_search/tipaudis")
    @Timed
    public List<Tipaudi> searchTipaudis(@RequestParam String query) {
        log.debug("REST request to search Tipaudis for query {}", query);
        return StreamSupport
            .stream(tipaudiSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
