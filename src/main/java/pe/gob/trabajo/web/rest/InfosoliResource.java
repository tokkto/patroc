package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Infosoli;

import pe.gob.trabajo.repository.InfosoliRepository;
import pe.gob.trabajo.repository.search.InfosoliSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Infosoli.
 */
@RestController
@RequestMapping("/api")
public class InfosoliResource {

    private final Logger log = LoggerFactory.getLogger(InfosoliResource.class);

    private static final String ENTITY_NAME = "infosoli";

    private final InfosoliRepository infosoliRepository;

    private final InfosoliSearchRepository infosoliSearchRepository;

    public InfosoliResource(InfosoliRepository infosoliRepository, InfosoliSearchRepository infosoliSearchRepository) {
        this.infosoliRepository = infosoliRepository;
        this.infosoliSearchRepository = infosoliSearchRepository;
    }

    /**
     * POST  /infosolis : Create a new infosoli.
     *
     * @param infosoli the infosoli to create
     * @return the ResponseEntity with status 201 (Created) and with body the new infosoli, or with status 400 (Bad Request) if the infosoli has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/infosolis")
    @Timed
    public ResponseEntity<Infosoli> createInfosoli(@Valid @RequestBody Infosoli infosoli) throws URISyntaxException {
        log.debug("REST request to save Infosoli : {}", infosoli);
        if (infosoli.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new infosoli cannot already have an ID")).body(null);
        }
        Infosoli result = infosoliRepository.save(infosoli);
        infosoliSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/infosolis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /infosolis : Updates an existing infosoli.
     *
     * @param infosoli the infosoli to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated infosoli,
     * or with status 400 (Bad Request) if the infosoli is not valid,
     * or with status 500 (Internal Server Error) if the infosoli couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/infosolis")
    @Timed
    public ResponseEntity<Infosoli> updateInfosoli(@Valid @RequestBody Infosoli infosoli) throws URISyntaxException {
        log.debug("REST request to update Infosoli : {}", infosoli);
        if (infosoli.getId() == null) {
            return createInfosoli(infosoli);
        }
        Infosoli result = infosoliRepository.save(infosoli);
        infosoliSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, infosoli.getId().toString()))
            .body(result);
    }

    /**
     * GET  /infosolis : get all the infosolis.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of infosolis in body
     */
    @GetMapping("/infosolis")
    @Timed
    public List<Infosoli> getAllInfosolis() {
        log.debug("REST request to get all Infosolis");
        return infosoliRepository.findAll();
        }

    /**
     * GET  /infosolis/:id : get the "id" infosoli.
     *
     * @param id the id of the infosoli to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the infosoli, or with status 404 (Not Found)
     */
    @GetMapping("/infosolis/{id}")
    @Timed
    public ResponseEntity<Infosoli> getInfosoli(@PathVariable Long id) {
        log.debug("REST request to get Infosoli : {}", id);
        Infosoli infosoli = infosoliRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(infosoli));
    }

    /**
     * DELETE  /infosolis/:id : delete the "id" infosoli.
     *
     * @param id the id of the infosoli to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/infosolis/{id}")
    @Timed
    public ResponseEntity<Void> deleteInfosoli(@PathVariable Long id) {
        log.debug("REST request to delete Infosoli : {}", id);
        infosoliRepository.delete(id);
        infosoliSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/infosolis?query=:query : search for the infosoli corresponding
     * to the query.
     *
     * @param query the query of the infosoli search
     * @return the result of the search
     */
    @GetMapping("/_search/infosolis")
    @Timed
    public List<Infosoli> searchInfosolis(@RequestParam String query) {
        log.debug("REST request to search Infosolis for query {}", query);
        return StreamSupport
            .stream(infosoliSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
