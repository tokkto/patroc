package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipresoluc;

import pe.gob.trabajo.repository.TipresolucRepository;
import pe.gob.trabajo.repository.search.TipresolucSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipresoluc.
 */
@RestController
@RequestMapping("/api")
public class TipresolucResource {

    private final Logger log = LoggerFactory.getLogger(TipresolucResource.class);

    private static final String ENTITY_NAME = "tipresoluc";

    private final TipresolucRepository tipresolucRepository;

    private final TipresolucSearchRepository tipresolucSearchRepository;

    public TipresolucResource(TipresolucRepository tipresolucRepository, TipresolucSearchRepository tipresolucSearchRepository) {
        this.tipresolucRepository = tipresolucRepository;
        this.tipresolucSearchRepository = tipresolucSearchRepository;
    }

    /**
     * POST  /tipresolucs : Create a new tipresoluc.
     *
     * @param tipresoluc the tipresoluc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipresoluc, or with status 400 (Bad Request) if the tipresoluc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipresolucs")
    @Timed
    public ResponseEntity<Tipresoluc> createTipresoluc(@Valid @RequestBody Tipresoluc tipresoluc) throws URISyntaxException {
        log.debug("REST request to save Tipresoluc : {}", tipresoluc);
        if (tipresoluc.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipresoluc cannot already have an ID")).body(null);
        }
        Tipresoluc result = tipresolucRepository.save(tipresoluc);
        tipresolucSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipresolucs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipresolucs : Updates an existing tipresoluc.
     *
     * @param tipresoluc the tipresoluc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipresoluc,
     * or with status 400 (Bad Request) if the tipresoluc is not valid,
     * or with status 500 (Internal Server Error) if the tipresoluc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipresolucs")
    @Timed
    public ResponseEntity<Tipresoluc> updateTipresoluc(@Valid @RequestBody Tipresoluc tipresoluc) throws URISyntaxException {
        log.debug("REST request to update Tipresoluc : {}", tipresoluc);
        if (tipresoluc.getId() == null) {
            return createTipresoluc(tipresoluc);
        }
        Tipresoluc result = tipresolucRepository.save(tipresoluc);
        tipresolucSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipresoluc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipresolucs : get all the tipresolucs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipresolucs in body
     */
    @GetMapping("/tipresolucs")
    @Timed
    public List<Tipresoluc> getAllTipresolucs() {
        log.debug("REST request to get all Tipresolucs");
        return tipresolucRepository.findAll();
        }

    /**
     * GET  /tipresolucs/:id : get the "id" tipresoluc.
     *
     * @param id the id of the tipresoluc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipresoluc, or with status 404 (Not Found)
     */
    @GetMapping("/tipresolucs/{id}")
    @Timed
    public ResponseEntity<Tipresoluc> getTipresoluc(@PathVariable Long id) {
        log.debug("REST request to get Tipresoluc : {}", id);
        Tipresoluc tipresoluc = tipresolucRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipresoluc));
    }

    /**
     * DELETE  /tipresolucs/:id : delete the "id" tipresoluc.
     *
     * @param id the id of the tipresoluc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipresolucs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipresoluc(@PathVariable Long id) {
        log.debug("REST request to delete Tipresoluc : {}", id);
        tipresolucRepository.delete(id);
        tipresolucSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipresolucs?query=:query : search for the tipresoluc corresponding
     * to the query.
     *
     * @param query the query of the tipresoluc search
     * @return the result of the search
     */
    @GetMapping("/_search/tipresolucs")
    @Timed
    public List<Tipresoluc> searchTipresolucs(@RequestParam String query) {
        log.debug("REST request to search Tipresolucs for query {}", query);
        return StreamSupport
            .stream(tipresolucSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
