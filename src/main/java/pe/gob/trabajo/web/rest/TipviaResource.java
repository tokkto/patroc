package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipvia;

import pe.gob.trabajo.repository.TipviaRepository;
import pe.gob.trabajo.repository.search.TipviaSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipvia.
 */
@RestController
@RequestMapping("/api")
public class TipviaResource {

    private final Logger log = LoggerFactory.getLogger(TipviaResource.class);

    private static final String ENTITY_NAME = "tipvia";

    private final TipviaRepository tipviaRepository;

    private final TipviaSearchRepository tipviaSearchRepository;

    public TipviaResource(TipviaRepository tipviaRepository, TipviaSearchRepository tipviaSearchRepository) {
        this.tipviaRepository = tipviaRepository;
        this.tipviaSearchRepository = tipviaSearchRepository;
    }

    /**
     * POST  /tipvias : Create a new tipvia.
     *
     * @param tipvia the tipvia to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipvia, or with status 400 (Bad Request) if the tipvia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipvias")
    @Timed
    public ResponseEntity<Tipvia> createTipvia(@Valid @RequestBody Tipvia tipvia) throws URISyntaxException {
        log.debug("REST request to save Tipvia : {}", tipvia);
        if (tipvia.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipvia cannot already have an ID")).body(null);
        }
        Tipvia result = tipviaRepository.save(tipvia);
        tipviaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipvias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipvias : Updates an existing tipvia.
     *
     * @param tipvia the tipvia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipvia,
     * or with status 400 (Bad Request) if the tipvia is not valid,
     * or with status 500 (Internal Server Error) if the tipvia couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipvias")
    @Timed
    public ResponseEntity<Tipvia> updateTipvia(@Valid @RequestBody Tipvia tipvia) throws URISyntaxException {
        log.debug("REST request to update Tipvia : {}", tipvia);
        if (tipvia.getId() == null) {
            return createTipvia(tipvia);
        }
        Tipvia result = tipviaRepository.save(tipvia);
        tipviaSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipvia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipvias : get all the tipvias.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipvias in body
     */
    @GetMapping("/tipvias")
    @Timed
    public List<Tipvia> getAllTipvias() {
        log.debug("REST request to get all Tipvias");
        return tipviaRepository.findAll();
        }

    /**
     * GET  /tipvias/:id : get the "id" tipvia.
     *
     * @param id the id of the tipvia to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipvia, or with status 404 (Not Found)
     */
    @GetMapping("/tipvias/{id}")
    @Timed
    public ResponseEntity<Tipvia> getTipvia(@PathVariable Long id) {
        log.debug("REST request to get Tipvia : {}", id);
        Tipvia tipvia = tipviaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipvia));
    }

    /**
     * DELETE  /tipvias/:id : delete the "id" tipvia.
     *
     * @param id the id of the tipvia to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipvias/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipvia(@PathVariable Long id) {
        log.debug("REST request to delete Tipvia : {}", id);
        tipviaRepository.delete(id);
        tipviaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipvias?query=:query : search for the tipvia corresponding
     * to the query.
     *
     * @param query the query of the tipvia search
     * @return the result of the search
     */
    @GetMapping("/_search/tipvias")
    @Timed
    public List<Tipvia> searchTipvias(@RequestParam String query) {
        log.debug("REST request to search Tipvias for query {}", query);
        return StreamSupport
            .stream(tipviaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
