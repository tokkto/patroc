package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Oridenu;

import pe.gob.trabajo.repository.OridenuRepository;
import pe.gob.trabajo.repository.search.OridenuSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Oridenu.
 */
@RestController
@RequestMapping("/api")
public class OridenuResource {

    private final Logger log = LoggerFactory.getLogger(OridenuResource.class);

    private static final String ENTITY_NAME = "oridenu";

    private final OridenuRepository oridenuRepository;

    private final OridenuSearchRepository oridenuSearchRepository;

    public OridenuResource(OridenuRepository oridenuRepository, OridenuSearchRepository oridenuSearchRepository) {
        this.oridenuRepository = oridenuRepository;
        this.oridenuSearchRepository = oridenuSearchRepository;
    }

    /**
     * POST  /oridenus : Create a new oridenu.
     *
     * @param oridenu the oridenu to create
     * @return the ResponseEntity with status 201 (Created) and with body the new oridenu, or with status 400 (Bad Request) if the oridenu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/oridenus")
    @Timed
    public ResponseEntity<Oridenu> createOridenu(@Valid @RequestBody Oridenu oridenu) throws URISyntaxException {
        log.debug("REST request to save Oridenu : {}", oridenu);
        if (oridenu.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new oridenu cannot already have an ID")).body(null);
        }
        Oridenu result = oridenuRepository.save(oridenu);
        oridenuSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/oridenus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /oridenus : Updates an existing oridenu.
     *
     * @param oridenu the oridenu to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated oridenu,
     * or with status 400 (Bad Request) if the oridenu is not valid,
     * or with status 500 (Internal Server Error) if the oridenu couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/oridenus")
    @Timed
    public ResponseEntity<Oridenu> updateOridenu(@Valid @RequestBody Oridenu oridenu) throws URISyntaxException {
        log.debug("REST request to update Oridenu : {}", oridenu);
        if (oridenu.getId() == null) {
            return createOridenu(oridenu);
        }
        Oridenu result = oridenuRepository.save(oridenu);
        oridenuSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, oridenu.getId().toString()))
            .body(result);
    }

    /**
     * GET  /oridenus : get all the oridenus.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of oridenus in body
     */
    @GetMapping("/oridenus")
    @Timed
    public List<Oridenu> getAllOridenus() {
        log.debug("REST request to get all Oridenus");
        return oridenuRepository.findAll();
        }

    /**
     * GET  /oridenus/:id : get the "id" oridenu.
     *
     * @param id the id of the oridenu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the oridenu, or with status 404 (Not Found)
     */
    @GetMapping("/oridenus/{id}")
    @Timed
    public ResponseEntity<Oridenu> getOridenu(@PathVariable Long id) {
        log.debug("REST request to get Oridenu : {}", id);
        Oridenu oridenu = oridenuRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(oridenu));
    }

    /**
     * DELETE  /oridenus/:id : delete the "id" oridenu.
     *
     * @param id the id of the oridenu to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/oridenus/{id}")
    @Timed
    public ResponseEntity<Void> deleteOridenu(@PathVariable Long id) {
        log.debug("REST request to delete Oridenu : {}", id);
        oridenuRepository.delete(id);
        oridenuSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/oridenus?query=:query : search for the oridenu corresponding
     * to the query.
     *
     * @param query the query of the oridenu search
     * @return the result of the search
     */
    @GetMapping("/_search/oridenus")
    @Timed
    public List<Oridenu> searchOridenus(@RequestParam String query) {
        log.debug("REST request to search Oridenus for query {}", query);
        return StreamSupport
            .stream(oridenuSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
