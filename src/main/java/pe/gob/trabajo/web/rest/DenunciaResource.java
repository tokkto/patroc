package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Denuncia;

import pe.gob.trabajo.repository.DenunciaRepository;
import pe.gob.trabajo.repository.search.DenunciaSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Denuncia.
 */
@RestController
@RequestMapping("/api")
public class DenunciaResource {

    private final Logger log = LoggerFactory.getLogger(DenunciaResource.class);

    private static final String ENTITY_NAME = "denuncia";

    private final DenunciaRepository denunciaRepository;

    private final DenunciaSearchRepository denunciaSearchRepository;

    public DenunciaResource(DenunciaRepository denunciaRepository, DenunciaSearchRepository denunciaSearchRepository) {
        this.denunciaRepository = denunciaRepository;
        this.denunciaSearchRepository = denunciaSearchRepository;
    }

    /**
     * POST  /denuncias : Create a new denuncia.
     *
     * @param denuncia the denuncia to create
     * @return the ResponseEntity with status 201 (Created) and with body the new denuncia, or with status 400 (Bad Request) if the denuncia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/denuncias")
    @Timed
    public ResponseEntity<Denuncia> createDenuncia(@Valid @RequestBody Denuncia denuncia) throws URISyntaxException {
        log.debug("REST request to save Denuncia : {}", denuncia);
        if (denuncia.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new denuncia cannot already have an ID")).body(null);
        }
        Denuncia result = denunciaRepository.save(denuncia);
        denunciaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/denuncias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /denuncias : Updates an existing denuncia.
     *
     * @param denuncia the denuncia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated denuncia,
     * or with status 400 (Bad Request) if the denuncia is not valid,
     * or with status 500 (Internal Server Error) if the denuncia couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/denuncias")
    @Timed
    public ResponseEntity<Denuncia> updateDenuncia(@Valid @RequestBody Denuncia denuncia) throws URISyntaxException {
        log.debug("REST request to update Denuncia : {}", denuncia);
        if (denuncia.getId() == null) {
            return createDenuncia(denuncia);
        }
        Denuncia result = denunciaRepository.save(denuncia);
        denunciaSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, denuncia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /denuncias : get all the denuncias.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of denuncias in body
     */
    @GetMapping("/denuncias")
    @Timed
    public List<Denuncia> getAllDenuncias() {
        log.debug("REST request to get all Denuncias");
        return denunciaRepository.findAll();
        }

    /**
     * GET  /denuncias/:id : get the "id" denuncia.
     *
     * @param id the id of the denuncia to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the denuncia, or with status 404 (Not Found)
     */
    @GetMapping("/denuncias/{id}")
    @Timed
    public ResponseEntity<Denuncia> getDenuncia(@PathVariable Long id) {
        log.debug("REST request to get Denuncia : {}", id);
        Denuncia denuncia = denunciaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(denuncia));
    }

    /**
     * DELETE  /denuncias/:id : delete the "id" denuncia.
     *
     * @param id the id of the denuncia to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/denuncias/{id}")
    @Timed
    public ResponseEntity<Void> deleteDenuncia(@PathVariable Long id) {
        log.debug("REST request to delete Denuncia : {}", id);
        denunciaRepository.delete(id);
        denunciaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/denuncias?query=:query : search for the denuncia corresponding
     * to the query.
     *
     * @param query the query of the denuncia search
     * @return the result of the search
     */
    @GetMapping("/_search/denuncias")
    @Timed
    public List<Denuncia> searchDenuncias(@RequestParam String query) {
        log.debug("REST request to search Denuncias for query {}", query);
        return StreamSupport
            .stream(denunciaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
