package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Dirdenun;

import pe.gob.trabajo.repository.DirdenunRepository;
import pe.gob.trabajo.repository.search.DirdenunSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Dirdenun.
 */
@RestController
@RequestMapping("/api")
public class DirdenunResource {

    private final Logger log = LoggerFactory.getLogger(DirdenunResource.class);

    private static final String ENTITY_NAME = "dirdenun";

    private final DirdenunRepository dirdenunRepository;

    private final DirdenunSearchRepository dirdenunSearchRepository;

    public DirdenunResource(DirdenunRepository dirdenunRepository, DirdenunSearchRepository dirdenunSearchRepository) {
        this.dirdenunRepository = dirdenunRepository;
        this.dirdenunSearchRepository = dirdenunSearchRepository;
    }

    /**
     * POST  /dirdenuns : Create a new dirdenun.
     *
     * @param dirdenun the dirdenun to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dirdenun, or with status 400 (Bad Request) if the dirdenun has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dirdenuns")
    @Timed
    public ResponseEntity<Dirdenun> createDirdenun(@Valid @RequestBody Dirdenun dirdenun) throws URISyntaxException {
        log.debug("REST request to save Dirdenun : {}", dirdenun);
        if (dirdenun.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dirdenun cannot already have an ID")).body(null);
        }
        Dirdenun result = dirdenunRepository.save(dirdenun);
        dirdenunSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/dirdenuns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dirdenuns : Updates an existing dirdenun.
     *
     * @param dirdenun the dirdenun to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dirdenun,
     * or with status 400 (Bad Request) if the dirdenun is not valid,
     * or with status 500 (Internal Server Error) if the dirdenun couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dirdenuns")
    @Timed
    public ResponseEntity<Dirdenun> updateDirdenun(@Valid @RequestBody Dirdenun dirdenun) throws URISyntaxException {
        log.debug("REST request to update Dirdenun : {}", dirdenun);
        if (dirdenun.getId() == null) {
            return createDirdenun(dirdenun);
        }
        Dirdenun result = dirdenunRepository.save(dirdenun);
        dirdenunSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dirdenun.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dirdenuns : get all the dirdenuns.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dirdenuns in body
     */
    @GetMapping("/dirdenuns")
    @Timed
    public List<Dirdenun> getAllDirdenuns() {
        log.debug("REST request to get all Dirdenuns");
        return dirdenunRepository.findAll();
        }

    /**
     * GET  /dirdenuns/:id : get the "id" dirdenun.
     *
     * @param id the id of the dirdenun to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dirdenun, or with status 404 (Not Found)
     */
    @GetMapping("/dirdenuns/{id}")
    @Timed
    public ResponseEntity<Dirdenun> getDirdenun(@PathVariable Long id) {
        log.debug("REST request to get Dirdenun : {}", id);
        Dirdenun dirdenun = dirdenunRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dirdenun));
    }

    /**
     * DELETE  /dirdenuns/:id : delete the "id" dirdenun.
     *
     * @param id the id of the dirdenun to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dirdenuns/{id}")
    @Timed
    public ResponseEntity<Void> deleteDirdenun(@PathVariable Long id) {
        log.debug("REST request to delete Dirdenun : {}", id);
        dirdenunRepository.delete(id);
        dirdenunSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/dirdenuns?query=:query : search for the dirdenun corresponding
     * to the query.
     *
     * @param query the query of the dirdenun search
     * @return the result of the search
     */
    @GetMapping("/_search/dirdenuns")
    @Timed
    public List<Dirdenun> searchDirdenuns(@RequestParam String query) {
        log.debug("REST request to search Dirdenuns for query {}", query);
        return StreamSupport
            .stream(dirdenunSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
