package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Motidenun;

import pe.gob.trabajo.repository.MotidenunRepository;
import pe.gob.trabajo.repository.search.MotidenunSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Motidenun.
 */
@RestController
@RequestMapping("/api")
public class MotidenunResource {

    private final Logger log = LoggerFactory.getLogger(MotidenunResource.class);

    private static final String ENTITY_NAME = "motidenun";

    private final MotidenunRepository motidenunRepository;

    private final MotidenunSearchRepository motidenunSearchRepository;

    public MotidenunResource(MotidenunRepository motidenunRepository, MotidenunSearchRepository motidenunSearchRepository) {
        this.motidenunRepository = motidenunRepository;
        this.motidenunSearchRepository = motidenunSearchRepository;
    }

    /**
     * POST  /motidenuns : Create a new motidenun.
     *
     * @param motidenun the motidenun to create
     * @return the ResponseEntity with status 201 (Created) and with body the new motidenun, or with status 400 (Bad Request) if the motidenun has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motidenuns")
    @Timed
    public ResponseEntity<Motidenun> createMotidenun(@Valid @RequestBody Motidenun motidenun) throws URISyntaxException {
        log.debug("REST request to save Motidenun : {}", motidenun);
        if (motidenun.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new motidenun cannot already have an ID")).body(null);
        }
        Motidenun result = motidenunRepository.save(motidenun);
        motidenunSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/motidenuns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /motidenuns : Updates an existing motidenun.
     *
     * @param motidenun the motidenun to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated motidenun,
     * or with status 400 (Bad Request) if the motidenun is not valid,
     * or with status 500 (Internal Server Error) if the motidenun couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/motidenuns")
    @Timed
    public ResponseEntity<Motidenun> updateMotidenun(@Valid @RequestBody Motidenun motidenun) throws URISyntaxException {
        log.debug("REST request to update Motidenun : {}", motidenun);
        if (motidenun.getId() == null) {
            return createMotidenun(motidenun);
        }
        Motidenun result = motidenunRepository.save(motidenun);
        motidenunSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, motidenun.getId().toString()))
            .body(result);
    }

    /**
     * GET  /motidenuns : get all the motidenuns.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of motidenuns in body
     */
    @GetMapping("/motidenuns")
    @Timed
    public List<Motidenun> getAllMotidenuns() {
        log.debug("REST request to get all Motidenuns");
        return motidenunRepository.findAll();
        }

    /**
     * GET  /motidenuns/:id : get the "id" motidenun.
     *
     * @param id the id of the motidenun to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the motidenun, or with status 404 (Not Found)
     */
    @GetMapping("/motidenuns/{id}")
    @Timed
    public ResponseEntity<Motidenun> getMotidenun(@PathVariable Long id) {
        log.debug("REST request to get Motidenun : {}", id);
        Motidenun motidenun = motidenunRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(motidenun));
    }

    /**
     * DELETE  /motidenuns/:id : delete the "id" motidenun.
     *
     * @param id the id of the motidenun to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/motidenuns/{id}")
    @Timed
    public ResponseEntity<Void> deleteMotidenun(@PathVariable Long id) {
        log.debug("REST request to delete Motidenun : {}", id);
        motidenunRepository.delete(id);
        motidenunSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/motidenuns?query=:query : search for the motidenun corresponding
     * to the query.
     *
     * @param query the query of the motidenun search
     * @return the result of the search
     */
    @GetMapping("/_search/motidenuns")
    @Timed
    public List<Motidenun> searchMotidenuns(@RequestParam String query) {
        log.debug("REST request to search Motidenuns for query {}", query);
        return StreamSupport
            .stream(motidenunSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
