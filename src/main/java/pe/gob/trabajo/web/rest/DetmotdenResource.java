package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Detmotden;

import pe.gob.trabajo.repository.DetmotdenRepository;
import pe.gob.trabajo.repository.search.DetmotdenSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Detmotden.
 */
@RestController
@RequestMapping("/api")
public class DetmotdenResource {

    private final Logger log = LoggerFactory.getLogger(DetmotdenResource.class);

    private static final String ENTITY_NAME = "detmotden";

    private final DetmotdenRepository detmotdenRepository;

    private final DetmotdenSearchRepository detmotdenSearchRepository;

    public DetmotdenResource(DetmotdenRepository detmotdenRepository, DetmotdenSearchRepository detmotdenSearchRepository) {
        this.detmotdenRepository = detmotdenRepository;
        this.detmotdenSearchRepository = detmotdenSearchRepository;
    }

    /**
     * POST  /detmotdens : Create a new detmotden.
     *
     * @param detmotden the detmotden to create
     * @return the ResponseEntity with status 201 (Created) and with body the new detmotden, or with status 400 (Bad Request) if the detmotden has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/detmotdens")
    @Timed
    public ResponseEntity<Detmotden> createDetmotden(@Valid @RequestBody Detmotden detmotden) throws URISyntaxException {
        log.debug("REST request to save Detmotden : {}", detmotden);
        if (detmotden.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new detmotden cannot already have an ID")).body(null);
        }
        Detmotden result = detmotdenRepository.save(detmotden);
        detmotdenSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/detmotdens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /detmotdens : Updates an existing detmotden.
     *
     * @param detmotden the detmotden to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated detmotden,
     * or with status 400 (Bad Request) if the detmotden is not valid,
     * or with status 500 (Internal Server Error) if the detmotden couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/detmotdens")
    @Timed
    public ResponseEntity<Detmotden> updateDetmotden(@Valid @RequestBody Detmotden detmotden) throws URISyntaxException {
        log.debug("REST request to update Detmotden : {}", detmotden);
        if (detmotden.getId() == null) {
            return createDetmotden(detmotden);
        }
        Detmotden result = detmotdenRepository.save(detmotden);
        detmotdenSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, detmotden.getId().toString()))
            .body(result);
    }

    /**
     * GET  /detmotdens : get all the detmotdens.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of detmotdens in body
     */
    @GetMapping("/detmotdens")
    @Timed
    public List<Detmotden> getAllDetmotdens() {
        log.debug("REST request to get all Detmotdens");
        return detmotdenRepository.findAll();
        }

    /**
     * GET  /detmotdens/:id : get the "id" detmotden.
     *
     * @param id the id of the detmotden to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the detmotden, or with status 404 (Not Found)
     */
    @GetMapping("/detmotdens/{id}")
    @Timed
    public ResponseEntity<Detmotden> getDetmotden(@PathVariable Long id) {
        log.debug("REST request to get Detmotden : {}", id);
        Detmotden detmotden = detmotdenRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(detmotden));
    }

    /**
     * DELETE  /detmotdens/:id : delete the "id" detmotden.
     *
     * @param id the id of the detmotden to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/detmotdens/{id}")
    @Timed
    public ResponseEntity<Void> deleteDetmotden(@PathVariable Long id) {
        log.debug("REST request to delete Detmotden : {}", id);
        detmotdenRepository.delete(id);
        detmotdenSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/detmotdens?query=:query : search for the detmotden corresponding
     * to the query.
     *
     * @param query the query of the detmotden search
     * @return the result of the search
     */
    @GetMapping("/_search/detmotdens")
    @Timed
    public List<Detmotden> searchDetmotdens(@RequestParam String query) {
        log.debug("REST request to search Detmotdens for query {}", query);
        return StreamSupport
            .stream(detmotdenSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
