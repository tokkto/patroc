package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipdiligenc;

import pe.gob.trabajo.repository.TipdiligencRepository;
import pe.gob.trabajo.repository.search.TipdiligencSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipdiligenc.
 */
@RestController
@RequestMapping("/api")
public class TipdiligencResource {

    private final Logger log = LoggerFactory.getLogger(TipdiligencResource.class);

    private static final String ENTITY_NAME = "tipdiligenc";

    private final TipdiligencRepository tipdiligencRepository;

    private final TipdiligencSearchRepository tipdiligencSearchRepository;

    public TipdiligencResource(TipdiligencRepository tipdiligencRepository, TipdiligencSearchRepository tipdiligencSearchRepository) {
        this.tipdiligencRepository = tipdiligencRepository;
        this.tipdiligencSearchRepository = tipdiligencSearchRepository;
    }

    /**
     * POST  /tipdiligencs : Create a new tipdiligenc.
     *
     * @param tipdiligenc the tipdiligenc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipdiligenc, or with status 400 (Bad Request) if the tipdiligenc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipdiligencs")
    @Timed
    public ResponseEntity<Tipdiligenc> createTipdiligenc(@Valid @RequestBody Tipdiligenc tipdiligenc) throws URISyntaxException {
        log.debug("REST request to save Tipdiligenc : {}", tipdiligenc);
        if (tipdiligenc.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipdiligenc cannot already have an ID")).body(null);
        }
        Tipdiligenc result = tipdiligencRepository.save(tipdiligenc);
        tipdiligencSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipdiligencs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipdiligencs : Updates an existing tipdiligenc.
     *
     * @param tipdiligenc the tipdiligenc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipdiligenc,
     * or with status 400 (Bad Request) if the tipdiligenc is not valid,
     * or with status 500 (Internal Server Error) if the tipdiligenc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipdiligencs")
    @Timed
    public ResponseEntity<Tipdiligenc> updateTipdiligenc(@Valid @RequestBody Tipdiligenc tipdiligenc) throws URISyntaxException {
        log.debug("REST request to update Tipdiligenc : {}", tipdiligenc);
        if (tipdiligenc.getId() == null) {
            return createTipdiligenc(tipdiligenc);
        }
        Tipdiligenc result = tipdiligencRepository.save(tipdiligenc);
        tipdiligencSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipdiligenc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipdiligencs : get all the tipdiligencs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipdiligencs in body
     */
    @GetMapping("/tipdiligencs")
    @Timed
    public List<Tipdiligenc> getAllTipdiligencs() {
        log.debug("REST request to get all Tipdiligencs");
        return tipdiligencRepository.findAll();
        }

    /**
     * GET  /tipdiligencs/:id : get the "id" tipdiligenc.
     *
     * @param id the id of the tipdiligenc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipdiligenc, or with status 404 (Not Found)
     */
    @GetMapping("/tipdiligencs/{id}")
    @Timed
    public ResponseEntity<Tipdiligenc> getTipdiligenc(@PathVariable Long id) {
        log.debug("REST request to get Tipdiligenc : {}", id);
        Tipdiligenc tipdiligenc = tipdiligencRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipdiligenc));
    }

    /**
     * DELETE  /tipdiligencs/:id : delete the "id" tipdiligenc.
     *
     * @param id the id of the tipdiligenc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipdiligencs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipdiligenc(@PathVariable Long id) {
        log.debug("REST request to delete Tipdiligenc : {}", id);
        tipdiligencRepository.delete(id);
        tipdiligencSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipdiligencs?query=:query : search for the tipdiligenc corresponding
     * to the query.
     *
     * @param query the query of the tipdiligenc search
     * @return the result of the search
     */
    @GetMapping("/_search/tipdiligencs")
    @Timed
    public List<Tipdiligenc> searchTipdiligencs(@RequestParam String query) {
        log.debug("REST request to search Tipdiligencs for query {}", query);
        return StreamSupport
            .stream(tipdiligencSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
