package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Falsoexp;

import pe.gob.trabajo.repository.FalsoexpRepository;
import pe.gob.trabajo.repository.search.FalsoexpSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Falsoexp.
 */
@RestController
@RequestMapping("/api")
public class FalsoexpResource {

    private final Logger log = LoggerFactory.getLogger(FalsoexpResource.class);

    private static final String ENTITY_NAME = "falsoexp";

    private final FalsoexpRepository falsoexpRepository;

    private final FalsoexpSearchRepository falsoexpSearchRepository;

    public FalsoexpResource(FalsoexpRepository falsoexpRepository, FalsoexpSearchRepository falsoexpSearchRepository) {
        this.falsoexpRepository = falsoexpRepository;
        this.falsoexpSearchRepository = falsoexpSearchRepository;
    }

    /**
     * POST  /falsoexps : Create a new falsoexp.
     *
     * @param falsoexp the falsoexp to create
     * @return the ResponseEntity with status 201 (Created) and with body the new falsoexp, or with status 400 (Bad Request) if the falsoexp has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/falsoexps")
    @Timed
    public ResponseEntity<Falsoexp> createFalsoexp(@Valid @RequestBody Falsoexp falsoexp) throws URISyntaxException {
        log.debug("REST request to save Falsoexp : {}", falsoexp);
        if (falsoexp.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new falsoexp cannot already have an ID")).body(null);
        }
        Falsoexp result = falsoexpRepository.save(falsoexp);
        falsoexpSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/falsoexps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /falsoexps : Updates an existing falsoexp.
     *
     * @param falsoexp the falsoexp to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated falsoexp,
     * or with status 400 (Bad Request) if the falsoexp is not valid,
     * or with status 500 (Internal Server Error) if the falsoexp couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/falsoexps")
    @Timed
    public ResponseEntity<Falsoexp> updateFalsoexp(@Valid @RequestBody Falsoexp falsoexp) throws URISyntaxException {
        log.debug("REST request to update Falsoexp : {}", falsoexp);
        if (falsoexp.getId() == null) {
            return createFalsoexp(falsoexp);
        }
        Falsoexp result = falsoexpRepository.save(falsoexp);
        falsoexpSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, falsoexp.getId().toString()))
            .body(result);
    }

    /**
     * GET  /falsoexps : get all the falsoexps.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of falsoexps in body
     */
    @GetMapping("/falsoexps")
    @Timed
    public List<Falsoexp> getAllFalsoexps() {
        log.debug("REST request to get all Falsoexps");
        return falsoexpRepository.findAll();
        }

    /**
     * GET  /falsoexps/:id : get the "id" falsoexp.
     *
     * @param id the id of the falsoexp to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the falsoexp, or with status 404 (Not Found)
     */
    @GetMapping("/falsoexps/{id}")
    @Timed
    public ResponseEntity<Falsoexp> getFalsoexp(@PathVariable Long id) {
        log.debug("REST request to get Falsoexp : {}", id);
        Falsoexp falsoexp = falsoexpRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(falsoexp));
    }

    /**
     * DELETE  /falsoexps/:id : delete the "id" falsoexp.
     *
     * @param id the id of the falsoexp to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/falsoexps/{id}")
    @Timed
    public ResponseEntity<Void> deleteFalsoexp(@PathVariable Long id) {
        log.debug("REST request to delete Falsoexp : {}", id);
        falsoexpRepository.delete(id);
        falsoexpSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/falsoexps?query=:query : search for the falsoexp corresponding
     * to the query.
     *
     * @param query the query of the falsoexp search
     * @return the result of the search
     */
    @GetMapping("/_search/falsoexps")
    @Timed
    public List<Falsoexp> searchFalsoexps(@RequestParam String query) {
        log.debug("REST request to search Falsoexps for query {}", query);
        return StreamSupport
            .stream(falsoexpSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
