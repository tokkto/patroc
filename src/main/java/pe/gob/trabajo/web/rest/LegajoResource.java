package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Legajo;

import pe.gob.trabajo.repository.LegajoRepository;
import pe.gob.trabajo.repository.search.LegajoSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Legajo.
 */
@RestController
@RequestMapping("/api")
public class LegajoResource {

    private final Logger log = LoggerFactory.getLogger(LegajoResource.class);

    private static final String ENTITY_NAME = "legajo";

    private final LegajoRepository legajoRepository;

    private final LegajoSearchRepository legajoSearchRepository;

    public LegajoResource(LegajoRepository legajoRepository, LegajoSearchRepository legajoSearchRepository) {
        this.legajoRepository = legajoRepository;
        this.legajoSearchRepository = legajoSearchRepository;
    }

    /**
     * POST  /legajos : Create a new legajo.
     *
     * @param legajo the legajo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new legajo, or with status 400 (Bad Request) if the legajo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/legajos")
    @Timed
    public ResponseEntity<Legajo> createLegajo(@Valid @RequestBody Legajo legajo) throws URISyntaxException {
        log.debug("REST request to save Legajo : {}", legajo);
        if (legajo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new legajo cannot already have an ID")).body(null);
        }
        Legajo result = legajoRepository.save(legajo);
        legajoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/legajos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /legajos : Updates an existing legajo.
     *
     * @param legajo the legajo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated legajo,
     * or with status 400 (Bad Request) if the legajo is not valid,
     * or with status 500 (Internal Server Error) if the legajo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/legajos")
    @Timed
    public ResponseEntity<Legajo> updateLegajo(@Valid @RequestBody Legajo legajo) throws URISyntaxException {
        log.debug("REST request to update Legajo : {}", legajo);
        if (legajo.getId() == null) {
            return createLegajo(legajo);
        }
        Legajo result = legajoRepository.save(legajo);
        legajoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, legajo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /legajos : get all the legajos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of legajos in body
     */
    @GetMapping("/legajos")
    @Timed
    public List<Legajo> getAllLegajos() {
        log.debug("REST request to get all Legajos");
        return legajoRepository.findAll();
        }

    /**
     * GET  /legajos/:id : get the "id" legajo.
     *
     * @param id the id of the legajo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the legajo, or with status 404 (Not Found)
     */
    @GetMapping("/legajos/{id}")
    @Timed
    public ResponseEntity<Legajo> getLegajo(@PathVariable Long id) {
        log.debug("REST request to get Legajo : {}", id);
        Legajo legajo = legajoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(legajo));
    }

    /**
     * DELETE  /legajos/:id : delete the "id" legajo.
     *
     * @param id the id of the legajo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/legajos/{id}")
    @Timed
    public ResponseEntity<Void> deleteLegajo(@PathVariable Long id) {
        log.debug("REST request to delete Legajo : {}", id);
        legajoRepository.delete(id);
        legajoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/legajos?query=:query : search for the legajo corresponding
     * to the query.
     *
     * @param query the query of the legajo search
     * @return the result of the search
     */
    @GetMapping("/_search/legajos")
    @Timed
    public List<Legajo> searchLegajos(@RequestParam String query) {
        log.debug("REST request to search Legajos for query {}", query);
        return StreamSupport
            .stream(legajoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
