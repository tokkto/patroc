package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Motfin;

import pe.gob.trabajo.repository.MotfinRepository;
import pe.gob.trabajo.repository.search.MotfinSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Motfin.
 */
@RestController
@RequestMapping("/api")
public class MotfinResource {

    private final Logger log = LoggerFactory.getLogger(MotfinResource.class);

    private static final String ENTITY_NAME = "motfin";

    private final MotfinRepository motfinRepository;

    private final MotfinSearchRepository motfinSearchRepository;

    public MotfinResource(MotfinRepository motfinRepository, MotfinSearchRepository motfinSearchRepository) {
        this.motfinRepository = motfinRepository;
        this.motfinSearchRepository = motfinSearchRepository;
    }

    /**
     * POST  /motfins : Create a new motfin.
     *
     * @param motfin the motfin to create
     * @return the ResponseEntity with status 201 (Created) and with body the new motfin, or with status 400 (Bad Request) if the motfin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motfins")
    @Timed
    public ResponseEntity<Motfin> createMotfin(@Valid @RequestBody Motfin motfin) throws URISyntaxException {
        log.debug("REST request to save Motfin : {}", motfin);
        if (motfin.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new motfin cannot already have an ID")).body(null);
        }
        Motfin result = motfinRepository.save(motfin);
        motfinSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/motfins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /motfins : Updates an existing motfin.
     *
     * @param motfin the motfin to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated motfin,
     * or with status 400 (Bad Request) if the motfin is not valid,
     * or with status 500 (Internal Server Error) if the motfin couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/motfins")
    @Timed
    public ResponseEntity<Motfin> updateMotfin(@Valid @RequestBody Motfin motfin) throws URISyntaxException {
        log.debug("REST request to update Motfin : {}", motfin);
        if (motfin.getId() == null) {
            return createMotfin(motfin);
        }
        Motfin result = motfinRepository.save(motfin);
        motfinSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, motfin.getId().toString()))
            .body(result);
    }

    /**
     * GET  /motfins : get all the motfins.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of motfins in body
     */
    @GetMapping("/motfins")
    @Timed
    public List<Motfin> getAllMotfins() {
        log.debug("REST request to get all Motfins");
        return motfinRepository.findAll();
        }

    /**
     * GET  /motfins/:id : get the "id" motfin.
     *
     * @param id the id of the motfin to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the motfin, or with status 404 (Not Found)
     */
    @GetMapping("/motfins/{id}")
    @Timed
    public ResponseEntity<Motfin> getMotfin(@PathVariable Long id) {
        log.debug("REST request to get Motfin : {}", id);
        Motfin motfin = motfinRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(motfin));
    }

    /**
     * DELETE  /motfins/:id : delete the "id" motfin.
     *
     * @param id the id of the motfin to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/motfins/{id}")
    @Timed
    public ResponseEntity<Void> deleteMotfin(@PathVariable Long id) {
        log.debug("REST request to delete Motfin : {}", id);
        motfinRepository.delete(id);
        motfinSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/motfins?query=:query : search for the motfin corresponding
     * to the query.
     *
     * @param query the query of the motfin search
     * @return the result of the search
     */
    @GetMapping("/_search/motfins")
    @Timed
    public List<Motfin> searchMotfins(@RequestParam String query) {
        log.debug("REST request to search Motfins for query {}", query);
        return StreamSupport
            .stream(motfinSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
