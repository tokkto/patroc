package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Datdenu.
 */
@Entity
@Table(name = "datdenu")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datdenu")
public class Datdenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "n_numtrbafe")
    private Integer nNumtrbafe;

    @NotNull
    @Size(max = 10)
    @Column(name = "v_horaini", length = 10, nullable = false)
    private String vHoraini;

    @NotNull
    @Size(max = 10)
    @Column(name = "v_horafin", length = 10, nullable = false)
    private String vHorafin;

    @NotNull
    @Size(max = 300)
    @Column(name = "v_desmotivo", length = 300, nullable = false)
    private String vDesmotivo;

    @NotNull
    @Size(max = 300)
    @Column(name = "v_desotro", length = 300, nullable = false)
    private String vDesotro;

    @Lob
    @Column(name = "b_archivo")
    private byte[] bArchivo;

    @Column(name = "b_archivo_content_type")
    private String bArchivoContentType;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_archtipo", length = 50, nullable = false)
    private String vArchtipo;

    @Column(name = "v_flggreide")
    private Boolean vFlggreide;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_usuareg", length = 20, nullable = false)
    private String vUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @Column(name = "n_flgactivo")
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Size(max = 20)
    @Column(name = "v_usuaupd", length = 20)
    private String vUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    private Detmotden detmotden;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getnNumtrbafe() {
        return nNumtrbafe;
    }

    public Datdenu nNumtrbafe(Integer nNumtrbafe) {
        this.nNumtrbafe = nNumtrbafe;
        return this;
    }

    public void setnNumtrbafe(Integer nNumtrbafe) {
        this.nNumtrbafe = nNumtrbafe;
    }

    public String getvHoraini() {
        return vHoraini;
    }

    public Datdenu vHoraini(String vHoraini) {
        this.vHoraini = vHoraini;
        return this;
    }

    public void setvHoraini(String vHoraini) {
        this.vHoraini = vHoraini;
    }

    public String getvHorafin() {
        return vHorafin;
    }

    public Datdenu vHorafin(String vHorafin) {
        this.vHorafin = vHorafin;
        return this;
    }

    public void setvHorafin(String vHorafin) {
        this.vHorafin = vHorafin;
    }

    public String getvDesmotivo() {
        return vDesmotivo;
    }

    public Datdenu vDesmotivo(String vDesmotivo) {
        this.vDesmotivo = vDesmotivo;
        return this;
    }

    public void setvDesmotivo(String vDesmotivo) {
        this.vDesmotivo = vDesmotivo;
    }

    public String getvDesotro() {
        return vDesotro;
    }

    public Datdenu vDesotro(String vDesotro) {
        this.vDesotro = vDesotro;
        return this;
    }

    public void setvDesotro(String vDesotro) {
        this.vDesotro = vDesotro;
    }

    public byte[] getbArchivo() {
        return bArchivo;
    }

    public Datdenu bArchivo(byte[] bArchivo) {
        this.bArchivo = bArchivo;
        return this;
    }

    public void setbArchivo(byte[] bArchivo) {
        this.bArchivo = bArchivo;
    }

    public String getbArchivoContentType() {
        return bArchivoContentType;
    }

    public Datdenu bArchivoContentType(String bArchivoContentType) {
        this.bArchivoContentType = bArchivoContentType;
        return this;
    }

    public void setbArchivoContentType(String bArchivoContentType) {
        this.bArchivoContentType = bArchivoContentType;
    }

    public String getvArchtipo() {
        return vArchtipo;
    }

    public Datdenu vArchtipo(String vArchtipo) {
        this.vArchtipo = vArchtipo;
        return this;
    }

    public void setvArchtipo(String vArchtipo) {
        this.vArchtipo = vArchtipo;
    }

    public Boolean isvFlggreide() {
        return vFlggreide;
    }

    public Datdenu vFlggreide(Boolean vFlggreide) {
        this.vFlggreide = vFlggreide;
        return this;
    }

    public void setvFlggreide(Boolean vFlggreide) {
        this.vFlggreide = vFlggreide;
    }

    public String getvUsuareg() {
        return vUsuareg;
    }

    public Datdenu vUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
        return this;
    }

    public void setvUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Datdenu tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Datdenu nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Datdenu nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public String getvUsuaupd() {
        return vUsuaupd;
    }

    public Datdenu vUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
        return this;
    }

    public void setvUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Datdenu tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Datdenu nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Detmotden getDetmotden() {
        return detmotden;
    }

    public Datdenu detmotden(Detmotden detmotden) {
        this.detmotden = detmotden;
        return this;
    }

    public void setDetmotden(Detmotden detmotden) {
        this.detmotden = detmotden;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Datdenu datdenu = (Datdenu) o;
        if (datdenu.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datdenu.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Datdenu{" +
            "id=" + getId() +
            ", nNumtrbafe='" + getnNumtrbafe() + "'" +
            ", vHoraini='" + getvHoraini() + "'" +
            ", vHorafin='" + getvHorafin() + "'" +
            ", vDesmotivo='" + getvDesmotivo() + "'" +
            ", vDesotro='" + getvDesotro() + "'" +
            ", bArchivo='" + getbArchivo() + "'" +
            ", bArchivoContentType='" + bArchivoContentType + "'" +
            ", vArchtipo='" + getvArchtipo() + "'" +
            ", vFlggreide='" + isvFlggreide() + "'" +
            ", vUsuareg='" + getvUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", vUsuaupd='" + getvUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
