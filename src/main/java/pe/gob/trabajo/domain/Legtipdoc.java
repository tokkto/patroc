package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Legtipdoc.
 */
@Entity
@Table(name = "pjmvd_legxtipdoc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjmvd_legxtipdoc")
public class Legtipdoc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codletdoc", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codreg", length = 2, nullable = false)
    private String vCodreg;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codzon", length = 2, nullable = false)
    private String vCodzon;

    @NotNull
    @Column(name = "n_correl", nullable = false)
    private Integer nCorrel;

    @NotNull
    @Size(max = 3)
    @Column(name = "v_codsis", length = 3, nullable = false)
    private String vCodsis;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_asundoc", length = 100, nullable = false)
    private String vAsundoc;

    @Column(name = "d_fecdoc")
    private LocalDate dFecdoc;

    @NotNull
    @Column(name = "n_monpag", nullable = false)
    private Integer nMonpag;

    @Column(name = "d_fecentr")
    private LocalDate dFecentr;

    @Column(name = "d_fecdev")
    private LocalDate dFecdev;

    @Column(name = "d_fecrecjuz")
    private LocalDate dFecrecjuz;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusureg", length = 15, nullable = false)
    private String vCodusureg;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostreg", length = 30, nullable = false)
    private String vHostreg;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusumod", length = 15, nullable = false)
    private String vCodusumod;

    @Column(name = "d_fecmod")
    private LocalDate dFecmod;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostmod", length = 30, nullable = false)
    private String vHostmod;

    @Column(name = "d_feccit")
    private LocalDate dFeccit;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgfund", length = 1, nullable = false)
    private String vFlgfund;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgasis", length = 1, nullable = false)
    private String vFlgasis;

    @NotNull
    @Column(name = "n_numres", nullable = false)
    private Integer nNumres;

    @NotNull
    @Column(name = "n_mondol", nullable = false)
    private Integer nMondol;

    @NotNull
    @Size(max = 6)
    @Column(name = "v_codloc", length = 6, nullable = false)
    private String vCodloc;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgconc", length = 1, nullable = false)
    private String vFlgconc;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_dettdoc", length = 2, nullable = false)
    private String vDettdoc;

    @Column(name = "d_fecdocreq")
    private LocalDate dFecdocreq;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codabogad")
    private Abogado abogado;

    @ManyToOne
    @JoinColumn(name = "n_codlegajo")
    private Legajo legajo;

    @ManyToOne
    @JoinColumn(name = "n_codtdoc")
    private Tipdocpj tipdocpj;

    @ManyToOne
    @JoinColumn(name = "n_codtipres")
    private Tipresoluc tipresoluc;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodreg() {
        return vCodreg;
    }

    public Legtipdoc vCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
        return this;
    }

    public void setvCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getvCodzon() {
        return vCodzon;
    }

    public Legtipdoc vCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
        return this;
    }

    public void setvCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
    }

    public Integer getnCorrel() {
        return nCorrel;
    }

    public Legtipdoc nCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
        return this;
    }

    public void setnCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
    }

    public String getvCodsis() {
        return vCodsis;
    }

    public Legtipdoc vCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
        return this;
    }

    public void setvCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
    }

    public String getvAsundoc() {
        return vAsundoc;
    }

    public Legtipdoc vAsundoc(String vAsundoc) {
        this.vAsundoc = vAsundoc;
        return this;
    }

    public void setvAsundoc(String vAsundoc) {
        this.vAsundoc = vAsundoc;
    }

    public LocalDate getdFecdoc() {
        return dFecdoc;
    }

    public Legtipdoc dFecdoc(LocalDate dFecdoc) {
        this.dFecdoc = dFecdoc;
        return this;
    }

    public void setdFecdoc(LocalDate dFecdoc) {
        this.dFecdoc = dFecdoc;
    }

    public Integer getnMonpag() {
        return nMonpag;
    }

    public Legtipdoc nMonpag(Integer nMonpag) {
        this.nMonpag = nMonpag;
        return this;
    }

    public void setnMonpag(Integer nMonpag) {
        this.nMonpag = nMonpag;
    }

    public LocalDate getdFecentr() {
        return dFecentr;
    }

    public Legtipdoc dFecentr(LocalDate dFecentr) {
        this.dFecentr = dFecentr;
        return this;
    }

    public void setdFecentr(LocalDate dFecentr) {
        this.dFecentr = dFecentr;
    }

    public LocalDate getdFecdev() {
        return dFecdev;
    }

    public Legtipdoc dFecdev(LocalDate dFecdev) {
        this.dFecdev = dFecdev;
        return this;
    }

    public void setdFecdev(LocalDate dFecdev) {
        this.dFecdev = dFecdev;
    }

    public LocalDate getdFecrecjuz() {
        return dFecrecjuz;
    }

    public Legtipdoc dFecrecjuz(LocalDate dFecrecjuz) {
        this.dFecrecjuz = dFecrecjuz;
        return this;
    }

    public void setdFecrecjuz(LocalDate dFecrecjuz) {
        this.dFecrecjuz = dFecrecjuz;
    }

    public String getvCodusureg() {
        return vCodusureg;
    }

    public Legtipdoc vCodusureg(String vCodusureg) {
        this.vCodusureg = vCodusureg;
        return this;
    }

    public void setvCodusureg(String vCodusureg) {
        this.vCodusureg = vCodusureg;
    }

    public String getvHostreg() {
        return vHostreg;
    }

    public Legtipdoc vHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
        return this;
    }

    public void setvHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
    }

    public String getvCodusumod() {
        return vCodusumod;
    }

    public Legtipdoc vCodusumod(String vCodusumod) {
        this.vCodusumod = vCodusumod;
        return this;
    }

    public void setvCodusumod(String vCodusumod) {
        this.vCodusumod = vCodusumod;
    }

    public LocalDate getdFecmod() {
        return dFecmod;
    }

    public Legtipdoc dFecmod(LocalDate dFecmod) {
        this.dFecmod = dFecmod;
        return this;
    }

    public void setdFecmod(LocalDate dFecmod) {
        this.dFecmod = dFecmod;
    }

    public String getvHostmod() {
        return vHostmod;
    }

    public Legtipdoc vHostmod(String vHostmod) {
        this.vHostmod = vHostmod;
        return this;
    }

    public void setvHostmod(String vHostmod) {
        this.vHostmod = vHostmod;
    }

    public LocalDate getdFeccit() {
        return dFeccit;
    }

    public Legtipdoc dFeccit(LocalDate dFeccit) {
        this.dFeccit = dFeccit;
        return this;
    }

    public void setdFeccit(LocalDate dFeccit) {
        this.dFeccit = dFeccit;
    }

    public String getvFlgfund() {
        return vFlgfund;
    }

    public Legtipdoc vFlgfund(String vFlgfund) {
        this.vFlgfund = vFlgfund;
        return this;
    }

    public void setvFlgfund(String vFlgfund) {
        this.vFlgfund = vFlgfund;
    }

    public String getvFlgasis() {
        return vFlgasis;
    }

    public Legtipdoc vFlgasis(String vFlgasis) {
        this.vFlgasis = vFlgasis;
        return this;
    }

    public void setvFlgasis(String vFlgasis) {
        this.vFlgasis = vFlgasis;
    }

    public Integer getnNumres() {
        return nNumres;
    }

    public Legtipdoc nNumres(Integer nNumres) {
        this.nNumres = nNumres;
        return this;
    }

    public void setnNumres(Integer nNumres) {
        this.nNumres = nNumres;
    }

    public Integer getnMondol() {
        return nMondol;
    }

    public Legtipdoc nMondol(Integer nMondol) {
        this.nMondol = nMondol;
        return this;
    }

    public void setnMondol(Integer nMondol) {
        this.nMondol = nMondol;
    }

    public String getvCodloc() {
        return vCodloc;
    }

    public Legtipdoc vCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
        return this;
    }

    public void setvCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
    }

    public String getvFlgconc() {
        return vFlgconc;
    }

    public Legtipdoc vFlgconc(String vFlgconc) {
        this.vFlgconc = vFlgconc;
        return this;
    }

    public void setvFlgconc(String vFlgconc) {
        this.vFlgconc = vFlgconc;
    }

    public String getvDettdoc() {
        return vDettdoc;
    }

    public Legtipdoc vDettdoc(String vDettdoc) {
        this.vDettdoc = vDettdoc;
        return this;
    }

    public void setvDettdoc(String vDettdoc) {
        this.vDettdoc = vDettdoc;
    }

    public LocalDate getdFecdocreq() {
        return dFecdocreq;
    }

    public Legtipdoc dFecdocreq(LocalDate dFecdocreq) {
        this.dFecdocreq = dFecdocreq;
        return this;
    }

    public void setdFecdocreq(LocalDate dFecdocreq) {
        this.dFecdocreq = dFecdocreq;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Legtipdoc nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Legtipdoc tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Legtipdoc nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Legtipdoc nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Legtipdoc nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Legtipdoc tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Legtipdoc nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Abogado getAbogado() {
        return abogado;
    }

    public Legtipdoc abogado(Abogado abogado) {
        this.abogado = abogado;
        return this;
    }

    public void setAbogado(Abogado abogado) {
        this.abogado = abogado;
    }

    public Legajo getLegajo() {
        return legajo;
    }

    public Legtipdoc legajo(Legajo legajo) {
        this.legajo = legajo;
        return this;
    }

    public void setLegajo(Legajo legajo) {
        this.legajo = legajo;
    }

    public Tipdocpj getTipdocpj() {
        return tipdocpj;
    }

    public Legtipdoc tipdocpj(Tipdocpj tipdocpj) {
        this.tipdocpj = tipdocpj;
        return this;
    }

    public void setTipdocpj(Tipdocpj tipdocpj) {
        this.tipdocpj = tipdocpj;
    }

    public Tipresoluc getTipresoluc() {
        return tipresoluc;
    }

    public Legtipdoc tipresoluc(Tipresoluc tipresoluc) {
        this.tipresoluc = tipresoluc;
        return this;
    }

    public void setTipresoluc(Tipresoluc tipresoluc) {
        this.tipresoluc = tipresoluc;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Legtipdoc legtipdoc = (Legtipdoc) o;
        if (legtipdoc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), legtipdoc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Legtipdoc{" +
            "id=" + getId() +
            ", vCodreg='" + getvCodreg() + "'" +
            ", vCodzon='" + getvCodzon() + "'" +
            ", nCorrel='" + getnCorrel() + "'" +
            ", vCodsis='" + getvCodsis() + "'" +
            ", vAsundoc='" + getvAsundoc() + "'" +
            ", dFecdoc='" + getdFecdoc() + "'" +
            ", nMonpag='" + getnMonpag() + "'" +
            ", dFecentr='" + getdFecentr() + "'" +
            ", dFecdev='" + getdFecdev() + "'" +
            ", dFecrecjuz='" + getdFecrecjuz() + "'" +
            ", vCodusureg='" + getvCodusureg() + "'" +
            ", vHostreg='" + getvHostreg() + "'" +
            ", vCodusumod='" + getvCodusumod() + "'" +
            ", dFecmod='" + getdFecmod() + "'" +
            ", vHostmod='" + getvHostmod() + "'" +
            ", dFeccit='" + getdFeccit() + "'" +
            ", vFlgfund='" + getvFlgfund() + "'" +
            ", vFlgasis='" + getvFlgasis() + "'" +
            ", nNumres='" + getnNumres() + "'" +
            ", nMondol='" + getnMondol() + "'" +
            ", vCodloc='" + getvCodloc() + "'" +
            ", vFlgconc='" + getvFlgconc() + "'" +
            ", vDettdoc='" + getvDettdoc() + "'" +
            ", dFecdocreq='" + getdFecdocreq() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
