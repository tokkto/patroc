package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Denunte.
 */
@Entity
@Table(name = "denunte")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "denunte")
public class Denunte implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "n_codusu", nullable = false)
    private Integer nCodusu;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgestado", length = 1, nullable = false)
    private String vFlgestado;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_usuareg", length = 20, nullable = false)
    private String vUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @Column(name = "n_flgactivo")
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Size(max = 20)
    @Column(name = "v_usuaupd", length = 20)
    private String vUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    private Pernatural pernatural;

    @OneToMany(mappedBy = "denunte")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Dirdenun> dirdenuns = new HashSet<>();

    @OneToMany(mappedBy = "denunte")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Denuncia> denuncias = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getnCodusu() {
        return nCodusu;
    }

    public Denunte nCodusu(Integer nCodusu) {
        this.nCodusu = nCodusu;
        return this;
    }

    public void setnCodusu(Integer nCodusu) {
        this.nCodusu = nCodusu;
    }

    public String getvFlgestado() {
        return vFlgestado;
    }

    public Denunte vFlgestado(String vFlgestado) {
        this.vFlgestado = vFlgestado;
        return this;
    }

    public void setvFlgestado(String vFlgestado) {
        this.vFlgestado = vFlgestado;
    }

    public String getvUsuareg() {
        return vUsuareg;
    }

    public Denunte vUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
        return this;
    }

    public void setvUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Denunte tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Denunte nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Denunte nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public String getvUsuaupd() {
        return vUsuaupd;
    }

    public Denunte vUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
        return this;
    }

    public void setvUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Denunte tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Denunte nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Pernatural getPernatural() {
        return pernatural;
    }

    public Denunte pernatural(Pernatural pernatural) {
        this.pernatural = pernatural;
        return this;
    }

    public void setPernatural(Pernatural pernatural) {
        this.pernatural = pernatural;
    }

    public Set<Dirdenun> getDirdenuns() {
        return dirdenuns;
    }

    public Denunte dirdenuns(Set<Dirdenun> dirdenuns) {
        this.dirdenuns = dirdenuns;
        return this;
    }

    public Denunte addDirdenun(Dirdenun dirdenun) {
        this.dirdenuns.add(dirdenun);
        dirdenun.setDenunte(this);
        return this;
    }

    public Denunte removeDirdenun(Dirdenun dirdenun) {
        this.dirdenuns.remove(dirdenun);
        dirdenun.setDenunte(null);
        return this;
    }

    public void setDirdenuns(Set<Dirdenun> dirdenuns) {
        this.dirdenuns = dirdenuns;
    }

    public Set<Denuncia> getDenuncias() {
        return denuncias;
    }

    public Denunte denuncias(Set<Denuncia> denuncias) {
        this.denuncias = denuncias;
        return this;
    }

    public Denunte addDenuncia(Denuncia denuncia) {
        this.denuncias.add(denuncia);
        denuncia.setDenunte(this);
        return this;
    }

    public Denunte removeDenuncia(Denuncia denuncia) {
        this.denuncias.remove(denuncia);
        denuncia.setDenunte(null);
        return this;
    }

    public void setDenuncias(Set<Denuncia> denuncias) {
        this.denuncias = denuncias;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Denunte denunte = (Denunte) o;
        if (denunte.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), denunte.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Denunte{" +
            "id=" + getId() +
            ", nCodusu='" + getnCodusu() + "'" +
            ", vFlgestado='" + getvFlgestado() + "'" +
            ", vUsuareg='" + getvUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", vUsuaupd='" + getvUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
