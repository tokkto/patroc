package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Legajoasig.
 */
@Entity
@Table(name = "pjmvd_legajoasig")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjmvd_legajoasig")
public class Legajoasig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codlegasi", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codreg", length = 2, nullable = false)
    private String vCodreg;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codzon", length = 2, nullable = false)
    private String vCodzon;

    @NotNull
    @Column(name = "n_correl", nullable = false)
    private Integer nCorrel;

    @Column(name = "d_fecasig")
    private LocalDate dFecasig;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostreg", length = 30, nullable = false)
    private String vHostreg;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codabogad")
    private Abogado abogado;

    @ManyToOne
    @JoinColumn(name = "n_codlegajo")
    private Legajo legajo;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodreg() {
        return vCodreg;
    }

    public Legajoasig vCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
        return this;
    }

    public void setvCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getvCodzon() {
        return vCodzon;
    }

    public Legajoasig vCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
        return this;
    }

    public void setvCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
    }

    public Integer getnCorrel() {
        return nCorrel;
    }

    public Legajoasig nCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
        return this;
    }

    public void setnCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
    }

    public LocalDate getdFecasig() {
        return dFecasig;
    }

    public Legajoasig dFecasig(LocalDate dFecasig) {
        this.dFecasig = dFecasig;
        return this;
    }

    public void setdFecasig(LocalDate dFecasig) {
        this.dFecasig = dFecasig;
    }

    public String getvHostreg() {
        return vHostreg;
    }

    public Legajoasig vHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
        return this;
    }

    public void setvHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Legajoasig nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Legajoasig tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Legajoasig nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Legajoasig nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Legajoasig nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Legajoasig tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Legajoasig nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Abogado getAbogado() {
        return abogado;
    }

    public Legajoasig abogado(Abogado abogado) {
        this.abogado = abogado;
        return this;
    }

    public void setAbogado(Abogado abogado) {
        this.abogado = abogado;
    }

    public Legajo getLegajo() {
        return legajo;
    }

    public Legajoasig legajo(Legajo legajo) {
        this.legajo = legajo;
        return this;
    }

    public void setLegajo(Legajo legajo) {
        this.legajo = legajo;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Legajoasig legajoasig = (Legajoasig) o;
        if (legajoasig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), legajoasig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Legajoasig{" +
            "id=" + getId() +
            ", vCodreg='" + getvCodreg() + "'" +
            ", vCodzon='" + getvCodzon() + "'" +
            ", nCorrel='" + getnCorrel() + "'" +
            ", dFecasig='" + getdFecasig() + "'" +
            ", vHostreg='" + getvHostreg() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
