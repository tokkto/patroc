package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Legajo.
 */
@Entity
@Table(name = "pjmvc_legajo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjmvc_legajo")
public class Legajo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codlegajo", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codreg", length = 2, nullable = false)
    private String vCodreg;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codzon", length = 2, nullable = false)
    private String vCodzon;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_numexp", length = 30, nullable = false)
    private String vNumexp;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_nomjuzg", length = 50, nullable = false)
    private String vNomjuzg;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_nomjuez", length = 50, nullable = false)
    private String vNomjuez;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_estleg", length = 1, nullable = false)
    private String vEstleg;

    @Column(name = "d_fecconc")
    private LocalDate dFecconc;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusureg", length = 15, nullable = false)
    private String vCodusureg;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostreg", length = 30, nullable = false)
    private String vHostreg;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusumod", length = 15, nullable = false)
    private String vCodusumod;

    @Column(name = "d_fecmod")
    private LocalDate dFecmod;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostmod", length = 30, nullable = false)
    private String vHostmod;

    @Column(name = "d_fecexp")
    private LocalDate dFecexp;

    @NotNull
    @Size(max = 6)
    @Column(name = "v_numleg", length = 6, nullable = false)
    private String vNumleg;

    @NotNull
    @Column(name = "n_codsuc", nullable = false)
    private Integer nCodsuc;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flginst", length = 1, nullable = false)
    private String vFlginst;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_numexpda", length = 30, nullable = false)
    private String vNumexpda;

    @Column(name = "d_fecexpda")
    private LocalDate dFecexpda;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_nomjuzda", length = 50, nullable = false)
    private String vNomjuzda;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_nomjuezda", length = 50, nullable = false)
    private String vNomjuezda;

    @NotNull
    @Size(max = 6)
    @Column(name = "v_codlegrig", length = 6, nullable = false)
    private String vCodlegrig;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_tiporig", length = 1, nullable = false)
    private String vTiporig;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgarch", length = 1, nullable = false)
    private String vFlgarch;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgcaut", length = 1, nullable = false)
    private String vFlgcaut;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flganul", length = 1, nullable = false)
    private String vFlganul;

    @NotNull
    @Size(max = 6)
    @Column(name = "v_codloc", length = 6, nullable = false)
    private String vCodloc;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codemplea")
    private Empleador empleador;

    @ManyToOne
    @JoinColumn(name = "n_codpase")
    private Pasegl pasegl;

    @ManyToOne
    @JoinColumn(name = "n_codtrab")
    private Trabajador trabajador;

    @OneToMany(mappedBy = "legajo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Legtipdoc> legtipdocs = new HashSet<>();

    @OneToMany(mappedBy = "legajo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Atencionpj> atencionpjs = new HashSet<>();

    @OneToMany(mappedBy = "legajo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Legajoasig> legajoasigs = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "n_codmat")
    private Materia materia;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodreg() {
        return vCodreg;
    }

    public Legajo vCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
        return this;
    }

    public void setvCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getvCodzon() {
        return vCodzon;
    }

    public Legajo vCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
        return this;
    }

    public void setvCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
    }

    public String getvNumexp() {
        return vNumexp;
    }

    public Legajo vNumexp(String vNumexp) {
        this.vNumexp = vNumexp;
        return this;
    }

    public void setvNumexp(String vNumexp) {
        this.vNumexp = vNumexp;
    }

    public String getvNomjuzg() {
        return vNomjuzg;
    }

    public Legajo vNomjuzg(String vNomjuzg) {
        this.vNomjuzg = vNomjuzg;
        return this;
    }

    public void setvNomjuzg(String vNomjuzg) {
        this.vNomjuzg = vNomjuzg;
    }

    public String getvNomjuez() {
        return vNomjuez;
    }

    public Legajo vNomjuez(String vNomjuez) {
        this.vNomjuez = vNomjuez;
        return this;
    }

    public void setvNomjuez(String vNomjuez) {
        this.vNomjuez = vNomjuez;
    }

    public String getvEstleg() {
        return vEstleg;
    }

    public Legajo vEstleg(String vEstleg) {
        this.vEstleg = vEstleg;
        return this;
    }

    public void setvEstleg(String vEstleg) {
        this.vEstleg = vEstleg;
    }

    public LocalDate getdFecconc() {
        return dFecconc;
    }

    public Legajo dFecconc(LocalDate dFecconc) {
        this.dFecconc = dFecconc;
        return this;
    }

    public void setdFecconc(LocalDate dFecconc) {
        this.dFecconc = dFecconc;
    }

    public String getvCodusureg() {
        return vCodusureg;
    }

    public Legajo vCodusureg(String vCodusureg) {
        this.vCodusureg = vCodusureg;
        return this;
    }

    public void setvCodusureg(String vCodusureg) {
        this.vCodusureg = vCodusureg;
    }

    public String getvHostreg() {
        return vHostreg;
    }

    public Legajo vHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
        return this;
    }

    public void setvHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
    }

    public String getvCodusumod() {
        return vCodusumod;
    }

    public Legajo vCodusumod(String vCodusumod) {
        this.vCodusumod = vCodusumod;
        return this;
    }

    public void setvCodusumod(String vCodusumod) {
        this.vCodusumod = vCodusumod;
    }

    public LocalDate getdFecmod() {
        return dFecmod;
    }

    public Legajo dFecmod(LocalDate dFecmod) {
        this.dFecmod = dFecmod;
        return this;
    }

    public void setdFecmod(LocalDate dFecmod) {
        this.dFecmod = dFecmod;
    }

    public String getvHostmod() {
        return vHostmod;
    }

    public Legajo vHostmod(String vHostmod) {
        this.vHostmod = vHostmod;
        return this;
    }

    public void setvHostmod(String vHostmod) {
        this.vHostmod = vHostmod;
    }

    public LocalDate getdFecexp() {
        return dFecexp;
    }

    public Legajo dFecexp(LocalDate dFecexp) {
        this.dFecexp = dFecexp;
        return this;
    }

    public void setdFecexp(LocalDate dFecexp) {
        this.dFecexp = dFecexp;
    }

    public String getvNumleg() {
        return vNumleg;
    }

    public Legajo vNumleg(String vNumleg) {
        this.vNumleg = vNumleg;
        return this;
    }

    public void setvNumleg(String vNumleg) {
        this.vNumleg = vNumleg;
    }

    public Integer getnCodsuc() {
        return nCodsuc;
    }

    public Legajo nCodsuc(Integer nCodsuc) {
        this.nCodsuc = nCodsuc;
        return this;
    }

    public void setnCodsuc(Integer nCodsuc) {
        this.nCodsuc = nCodsuc;
    }

    public String getvFlginst() {
        return vFlginst;
    }

    public Legajo vFlginst(String vFlginst) {
        this.vFlginst = vFlginst;
        return this;
    }

    public void setvFlginst(String vFlginst) {
        this.vFlginst = vFlginst;
    }

    public String getvNumexpda() {
        return vNumexpda;
    }

    public Legajo vNumexpda(String vNumexpda) {
        this.vNumexpda = vNumexpda;
        return this;
    }

    public void setvNumexpda(String vNumexpda) {
        this.vNumexpda = vNumexpda;
    }

    public LocalDate getdFecexpda() {
        return dFecexpda;
    }

    public Legajo dFecexpda(LocalDate dFecexpda) {
        this.dFecexpda = dFecexpda;
        return this;
    }

    public void setdFecexpda(LocalDate dFecexpda) {
        this.dFecexpda = dFecexpda;
    }

    public String getvNomjuzda() {
        return vNomjuzda;
    }

    public Legajo vNomjuzda(String vNomjuzda) {
        this.vNomjuzda = vNomjuzda;
        return this;
    }

    public void setvNomjuzda(String vNomjuzda) {
        this.vNomjuzda = vNomjuzda;
    }

    public String getvNomjuezda() {
        return vNomjuezda;
    }

    public Legajo vNomjuezda(String vNomjuezda) {
        this.vNomjuezda = vNomjuezda;
        return this;
    }

    public void setvNomjuezda(String vNomjuezda) {
        this.vNomjuezda = vNomjuezda;
    }

    public String getvCodlegrig() {
        return vCodlegrig;
    }

    public Legajo vCodlegrig(String vCodlegrig) {
        this.vCodlegrig = vCodlegrig;
        return this;
    }

    public void setvCodlegrig(String vCodlegrig) {
        this.vCodlegrig = vCodlegrig;
    }

    public String getvTiporig() {
        return vTiporig;
    }

    public Legajo vTiporig(String vTiporig) {
        this.vTiporig = vTiporig;
        return this;
    }

    public void setvTiporig(String vTiporig) {
        this.vTiporig = vTiporig;
    }

    public String getvFlgarch() {
        return vFlgarch;
    }

    public Legajo vFlgarch(String vFlgarch) {
        this.vFlgarch = vFlgarch;
        return this;
    }

    public void setvFlgarch(String vFlgarch) {
        this.vFlgarch = vFlgarch;
    }

    public String getvFlgcaut() {
        return vFlgcaut;
    }

    public Legajo vFlgcaut(String vFlgcaut) {
        this.vFlgcaut = vFlgcaut;
        return this;
    }

    public void setvFlgcaut(String vFlgcaut) {
        this.vFlgcaut = vFlgcaut;
    }

    public String getvFlganul() {
        return vFlganul;
    }

    public Legajo vFlganul(String vFlganul) {
        this.vFlganul = vFlganul;
        return this;
    }

    public void setvFlganul(String vFlganul) {
        this.vFlganul = vFlganul;
    }

    public String getvCodloc() {
        return vCodloc;
    }

    public Legajo vCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
        return this;
    }

    public void setvCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Legajo nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Legajo tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Legajo nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Legajo nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Legajo nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Legajo tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Legajo nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Empleador getEmpleador() {
        return empleador;
    }

    public Legajo empleador(Empleador empleador) {
        this.empleador = empleador;
        return this;
    }

    public void setEmpleador(Empleador empleador) {
        this.empleador = empleador;
    }

    public Pasegl getPasegl() {
        return pasegl;
    }

    public Legajo pasegl(Pasegl pasegl) {
        this.pasegl = pasegl;
        return this;
    }

    public void setPasegl(Pasegl pasegl) {
        this.pasegl = pasegl;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public Legajo trabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
        return this;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Set<Legtipdoc> getLegtipdocs() {
        return legtipdocs;
    }

    public Legajo legtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
        return this;
    }

    public Legajo addLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.add(legtipdoc);
        legtipdoc.setLegajo(this);
        return this;
    }

    public Legajo removeLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.remove(legtipdoc);
        legtipdoc.setLegajo(null);
        return this;
    }

    public void setLegtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
    }

    public Set<Atencionpj> getAtencionpjs() {
        return atencionpjs;
    }

    public Legajo atencionpjs(Set<Atencionpj> atencionpjs) {
        this.atencionpjs = atencionpjs;
        return this;
    }

    public Legajo addAtencionpj(Atencionpj atencionpj) {
        this.atencionpjs.add(atencionpj);
        atencionpj.setLegajo(this);
        return this;
    }

    public Legajo removeAtencionpj(Atencionpj atencionpj) {
        this.atencionpjs.remove(atencionpj);
        atencionpj.setLegajo(null);
        return this;
    }

    public void setAtencionpjs(Set<Atencionpj> atencionpjs) {
        this.atencionpjs = atencionpjs;
    }

    public Set<Legajoasig> getLegajoasigs() {
        return legajoasigs;
    }

    public Legajo legajoasigs(Set<Legajoasig> legajoasigs) {
        this.legajoasigs = legajoasigs;
        return this;
    }

    public Legajo addLegajoasig(Legajoasig legajoasig) {
        this.legajoasigs.add(legajoasig);
        legajoasig.setLegajo(this);
        return this;
    }

    public Legajo removeLegajoasig(Legajoasig legajoasig) {
        this.legajoasigs.remove(legajoasig);
        legajoasig.setLegajo(null);
        return this;
    }

    public void setLegajoasigs(Set<Legajoasig> legajoasigs) {
        this.legajoasigs = legajoasigs;
    }

    public Materia getMateria() {
        return materia;
    }

    public Legajo materia(Materia materia) {
        this.materia = materia;
        return this;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Legajo legajo = (Legajo) o;
        if (legajo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), legajo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Legajo{" +
            "id=" + getId() +
            ", vCodreg='" + getvCodreg() + "'" +
            ", vCodzon='" + getvCodzon() + "'" +
            ", vNumexp='" + getvNumexp() + "'" +
            ", vNomjuzg='" + getvNomjuzg() + "'" +
            ", vNomjuez='" + getvNomjuez() + "'" +
            ", vEstleg='" + getvEstleg() + "'" +
            ", dFecconc='" + getdFecconc() + "'" +
            ", vCodusureg='" + getvCodusureg() + "'" +
            ", vHostreg='" + getvHostreg() + "'" +
            ", vCodusumod='" + getvCodusumod() + "'" +
            ", dFecmod='" + getdFecmod() + "'" +
            ", vHostmod='" + getvHostmod() + "'" +
            ", dFecexp='" + getdFecexp() + "'" +
            ", vNumleg='" + getvNumleg() + "'" +
            ", nCodsuc='" + getnCodsuc() + "'" +
            ", vFlginst='" + getvFlginst() + "'" +
            ", vNumexpda='" + getvNumexpda() + "'" +
            ", dFecexpda='" + getdFecexpda() + "'" +
            ", vNomjuzda='" + getvNomjuzda() + "'" +
            ", vNomjuezda='" + getvNomjuezda() + "'" +
            ", vCodlegrig='" + getvCodlegrig() + "'" +
            ", vTiporig='" + getvTiporig() + "'" +
            ", vFlgarch='" + getvFlgarch() + "'" +
            ", vFlgcaut='" + getvFlgcaut() + "'" +
            ", vFlganul='" + getvFlganul() + "'" +
            ", vCodloc='" + getvCodloc() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
