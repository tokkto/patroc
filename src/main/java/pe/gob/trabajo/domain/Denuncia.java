package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Denuncia.
 */
@Entity
@Table(name = "denuncia")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "denuncia")
public class Denuncia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 150)
    @Column(name = "v_coddenu", length = 150, nullable = false)
    private String vCoddenu;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_codsececo", length = 1, nullable = false)
    private String vCodsececo;

    @NotNull
    @Size(max = 5)
    @Column(name = "v_codciu", length = 5, nullable = false)
    private String vCodciu;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_desremipe", length = 200, nullable = false)
    private String vDesremipe;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_coddepart", length = 20, nullable = false)
    private String vCoddepart;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_codprovin", length = 20, nullable = false)
    private String vCodprovin;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_coddistri", length = 20, nullable = false)
    private String vCoddistri;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_desvia", length = 200, nullable = false)
    private String vDesvia;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_deszona", length = 200, nullable = false)
    private String vDeszona;

    @NotNull
    @Size(max = 300)
    @Column(name = "v_dircomp", length = 300, nullable = false)
    private String vDircomp;

    @NotNull
    @Size(max = 700)
    @Column(name = "v_dirdenu", length = 700, nullable = false)
    private String vDirdenu;

    @NotNull
    @Column(name = "t_fecinitra", nullable = false)
    private Instant tFecinitra;

    @Column(name = "n_flaglun")
    private Boolean nFlaglun;

    @Column(name = "n_flagmar")
    private Boolean nFlagmar;

    @Column(name = "n_flagmie")
    private Boolean nFlagmie;

    @Column(name = "n_flagjue")
    private Boolean nFlagjue;

    @Column(name = "n_flagvie")
    private Boolean nFlagvie;

    @Column(name = "n_flagsab")
    private Boolean nFlagsab;

    @Column(name = "n_flagdom")
    private Boolean nFlagdom;

    @NotNull
    @Size(max = 10)
    @Column(name = "t_horainit", length = 10, nullable = false)
    private String tHorainit;

    @NotNull
    @Size(max = 10)
    @Column(name = "t_horafint", length = 10, nullable = false)
    private String tHorafint;

    @Column(name = "v_flgtraba")
    private Boolean vFlgtraba;

    @Column(name = "v_flgrepre")
    private Boolean vFlgrepre;

    @NotNull
    @Column(name = "t_feccese", nullable = false)
    private Instant tFeccese;

    @NotNull
    @Size(max = 400)
    @Column(name = "v_desrepre", length = 400, nullable = false)
    private String vDesrepre;

    @NotNull
    @Size(max = 400)
    @Column(name = "v_obscalifi", length = 400, nullable = false)
    private String vObscalifi;

    @NotNull
    @Size(max = 400)
    @Column(name = "v_obsfin", length = 400, nullable = false)
    private String vObsfin;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgestado", length = 1, nullable = false)
    private String vFlgestado;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_usuareg", length = 20, nullable = false)
    private String vUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @Column(name = "n_flgactivo")
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Size(max = 20)
    @Column(name = "v_usuaupd", length = 20)
    private String vUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    private Perjuridica perjuridica;

    @ManyToOne
    private Denunte denunte;

    @ManyToOne
    private Tipzona tipzona;

    @ManyToOne
    private Tipvia tipvia;

    @ManyToOne
    private Motfin motfin;

    @ManyToOne
    private Infosoli infosoli;

    @ManyToOne
    private Calidenu calidenu;

    @ManyToOne
    private Oridenu oridenu;

    @OneToOne
    @JoinColumn(unique = true)
    private Datdenu datdenu;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCoddenu() {
        return vCoddenu;
    }

    public Denuncia vCoddenu(String vCoddenu) {
        this.vCoddenu = vCoddenu;
        return this;
    }

    public void setvCoddenu(String vCoddenu) {
        this.vCoddenu = vCoddenu;
    }

    public String getvCodsececo() {
        return vCodsececo;
    }

    public Denuncia vCodsececo(String vCodsececo) {
        this.vCodsececo = vCodsececo;
        return this;
    }

    public void setvCodsececo(String vCodsececo) {
        this.vCodsececo = vCodsececo;
    }

    public String getvCodciu() {
        return vCodciu;
    }

    public Denuncia vCodciu(String vCodciu) {
        this.vCodciu = vCodciu;
        return this;
    }

    public void setvCodciu(String vCodciu) {
        this.vCodciu = vCodciu;
    }

    public String getvDesremipe() {
        return vDesremipe;
    }

    public Denuncia vDesremipe(String vDesremipe) {
        this.vDesremipe = vDesremipe;
        return this;
    }

    public void setvDesremipe(String vDesremipe) {
        this.vDesremipe = vDesremipe;
    }

    public String getvCoddepart() {
        return vCoddepart;
    }

    public Denuncia vCoddepart(String vCoddepart) {
        this.vCoddepart = vCoddepart;
        return this;
    }

    public void setvCoddepart(String vCoddepart) {
        this.vCoddepart = vCoddepart;
    }

    public String getvCodprovin() {
        return vCodprovin;
    }

    public Denuncia vCodprovin(String vCodprovin) {
        this.vCodprovin = vCodprovin;
        return this;
    }

    public void setvCodprovin(String vCodprovin) {
        this.vCodprovin = vCodprovin;
    }

    public String getvCoddistri() {
        return vCoddistri;
    }

    public Denuncia vCoddistri(String vCoddistri) {
        this.vCoddistri = vCoddistri;
        return this;
    }

    public void setvCoddistri(String vCoddistri) {
        this.vCoddistri = vCoddistri;
    }

    public String getvDesvia() {
        return vDesvia;
    }

    public Denuncia vDesvia(String vDesvia) {
        this.vDesvia = vDesvia;
        return this;
    }

    public void setvDesvia(String vDesvia) {
        this.vDesvia = vDesvia;
    }

    public String getvDeszona() {
        return vDeszona;
    }

    public Denuncia vDeszona(String vDeszona) {
        this.vDeszona = vDeszona;
        return this;
    }

    public void setvDeszona(String vDeszona) {
        this.vDeszona = vDeszona;
    }

    public String getvDircomp() {
        return vDircomp;
    }

    public Denuncia vDircomp(String vDircomp) {
        this.vDircomp = vDircomp;
        return this;
    }

    public void setvDircomp(String vDircomp) {
        this.vDircomp = vDircomp;
    }

    public String getvDirdenu() {
        return vDirdenu;
    }

    public Denuncia vDirdenu(String vDirdenu) {
        this.vDirdenu = vDirdenu;
        return this;
    }

    public void setvDirdenu(String vDirdenu) {
        this.vDirdenu = vDirdenu;
    }

    public Instant gettFecinitra() {
        return tFecinitra;
    }

    public Denuncia tFecinitra(Instant tFecinitra) {
        this.tFecinitra = tFecinitra;
        return this;
    }

    public void settFecinitra(Instant tFecinitra) {
        this.tFecinitra = tFecinitra;
    }

    public Boolean isnFlaglun() {
        return nFlaglun;
    }

    public Denuncia nFlaglun(Boolean nFlaglun) {
        this.nFlaglun = nFlaglun;
        return this;
    }

    public void setnFlaglun(Boolean nFlaglun) {
        this.nFlaglun = nFlaglun;
    }

    public Boolean isnFlagmar() {
        return nFlagmar;
    }

    public Denuncia nFlagmar(Boolean nFlagmar) {
        this.nFlagmar = nFlagmar;
        return this;
    }

    public void setnFlagmar(Boolean nFlagmar) {
        this.nFlagmar = nFlagmar;
    }

    public Boolean isnFlagmie() {
        return nFlagmie;
    }

    public Denuncia nFlagmie(Boolean nFlagmie) {
        this.nFlagmie = nFlagmie;
        return this;
    }

    public void setnFlagmie(Boolean nFlagmie) {
        this.nFlagmie = nFlagmie;
    }

    public Boolean isnFlagjue() {
        return nFlagjue;
    }

    public Denuncia nFlagjue(Boolean nFlagjue) {
        this.nFlagjue = nFlagjue;
        return this;
    }

    public void setnFlagjue(Boolean nFlagjue) {
        this.nFlagjue = nFlagjue;
    }

    public Boolean isnFlagvie() {
        return nFlagvie;
    }

    public Denuncia nFlagvie(Boolean nFlagvie) {
        this.nFlagvie = nFlagvie;
        return this;
    }

    public void setnFlagvie(Boolean nFlagvie) {
        this.nFlagvie = nFlagvie;
    }

    public Boolean isnFlagsab() {
        return nFlagsab;
    }

    public Denuncia nFlagsab(Boolean nFlagsab) {
        this.nFlagsab = nFlagsab;
        return this;
    }

    public void setnFlagsab(Boolean nFlagsab) {
        this.nFlagsab = nFlagsab;
    }

    public Boolean isnFlagdom() {
        return nFlagdom;
    }

    public Denuncia nFlagdom(Boolean nFlagdom) {
        this.nFlagdom = nFlagdom;
        return this;
    }

    public void setnFlagdom(Boolean nFlagdom) {
        this.nFlagdom = nFlagdom;
    }

    public String gettHorainit() {
        return tHorainit;
    }

    public Denuncia tHorainit(String tHorainit) {
        this.tHorainit = tHorainit;
        return this;
    }

    public void settHorainit(String tHorainit) {
        this.tHorainit = tHorainit;
    }

    public String gettHorafint() {
        return tHorafint;
    }

    public Denuncia tHorafint(String tHorafint) {
        this.tHorafint = tHorafint;
        return this;
    }

    public void settHorafint(String tHorafint) {
        this.tHorafint = tHorafint;
    }

    public Boolean isvFlgtraba() {
        return vFlgtraba;
    }

    public Denuncia vFlgtraba(Boolean vFlgtraba) {
        this.vFlgtraba = vFlgtraba;
        return this;
    }

    public void setvFlgtraba(Boolean vFlgtraba) {
        this.vFlgtraba = vFlgtraba;
    }

    public Boolean isvFlgrepre() {
        return vFlgrepre;
    }

    public Denuncia vFlgrepre(Boolean vFlgrepre) {
        this.vFlgrepre = vFlgrepre;
        return this;
    }

    public void setvFlgrepre(Boolean vFlgrepre) {
        this.vFlgrepre = vFlgrepre;
    }

    public Instant gettFeccese() {
        return tFeccese;
    }

    public Denuncia tFeccese(Instant tFeccese) {
        this.tFeccese = tFeccese;
        return this;
    }

    public void settFeccese(Instant tFeccese) {
        this.tFeccese = tFeccese;
    }

    public String getvDesrepre() {
        return vDesrepre;
    }

    public Denuncia vDesrepre(String vDesrepre) {
        this.vDesrepre = vDesrepre;
        return this;
    }

    public void setvDesrepre(String vDesrepre) {
        this.vDesrepre = vDesrepre;
    }

    public String getvObscalifi() {
        return vObscalifi;
    }

    public Denuncia vObscalifi(String vObscalifi) {
        this.vObscalifi = vObscalifi;
        return this;
    }

    public void setvObscalifi(String vObscalifi) {
        this.vObscalifi = vObscalifi;
    }

    public String getvObsfin() {
        return vObsfin;
    }

    public Denuncia vObsfin(String vObsfin) {
        this.vObsfin = vObsfin;
        return this;
    }

    public void setvObsfin(String vObsfin) {
        this.vObsfin = vObsfin;
    }

    public String getvFlgestado() {
        return vFlgestado;
    }

    public Denuncia vFlgestado(String vFlgestado) {
        this.vFlgestado = vFlgestado;
        return this;
    }

    public void setvFlgestado(String vFlgestado) {
        this.vFlgestado = vFlgestado;
    }

    public String getvUsuareg() {
        return vUsuareg;
    }

    public Denuncia vUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
        return this;
    }

    public void setvUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Denuncia tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Denuncia nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Denuncia nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public String getvUsuaupd() {
        return vUsuaupd;
    }

    public Denuncia vUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
        return this;
    }

    public void setvUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Denuncia tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Denuncia nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Perjuridica getPerjuridica() {
        return perjuridica;
    }

    public Denuncia perjuridica(Perjuridica perjuridica) {
        this.perjuridica = perjuridica;
        return this;
    }

    public void setPerjuridica(Perjuridica perjuridica) {
        this.perjuridica = perjuridica;
    }

    public Denunte getDenunte() {
        return denunte;
    }

    public Denuncia denunte(Denunte denunte) {
        this.denunte = denunte;
        return this;
    }

    public void setDenunte(Denunte denunte) {
        this.denunte = denunte;
    }

    public Tipzona getTipzona() {
        return tipzona;
    }

    public Denuncia tipzona(Tipzona tipzona) {
        this.tipzona = tipzona;
        return this;
    }

    public void setTipzona(Tipzona tipzona) {
        this.tipzona = tipzona;
    }

    public Tipvia getTipvia() {
        return tipvia;
    }

    public Denuncia tipvia(Tipvia tipvia) {
        this.tipvia = tipvia;
        return this;
    }

    public void setTipvia(Tipvia tipvia) {
        this.tipvia = tipvia;
    }

    public Motfin getMotfin() {
        return motfin;
    }

    public Denuncia motfin(Motfin motfin) {
        this.motfin = motfin;
        return this;
    }

    public void setMotfin(Motfin motfin) {
        this.motfin = motfin;
    }

    public Infosoli getInfosoli() {
        return infosoli;
    }

    public Denuncia infosoli(Infosoli infosoli) {
        this.infosoli = infosoli;
        return this;
    }

    public void setInfosoli(Infosoli infosoli) {
        this.infosoli = infosoli;
    }

    public Calidenu getCalidenu() {
        return calidenu;
    }

    public Denuncia calidenu(Calidenu calidenu) {
        this.calidenu = calidenu;
        return this;
    }

    public void setCalidenu(Calidenu calidenu) {
        this.calidenu = calidenu;
    }

    public Oridenu getOridenu() {
        return oridenu;
    }

    public Denuncia oridenu(Oridenu oridenu) {
        this.oridenu = oridenu;
        return this;
    }

    public void setOridenu(Oridenu oridenu) {
        this.oridenu = oridenu;
    }

    public Datdenu getDatdenu() {
        return datdenu;
    }

    public Denuncia datdenu(Datdenu datdenu) {
        this.datdenu = datdenu;
        return this;
    }

    public void setDatdenu(Datdenu datdenu) {
        this.datdenu = datdenu;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Denuncia denuncia = (Denuncia) o;
        if (denuncia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), denuncia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Denuncia{" +
            "id=" + getId() +
            ", vCoddenu='" + getvCoddenu() + "'" +
            ", vCodsececo='" + getvCodsececo() + "'" +
            ", vCodciu='" + getvCodciu() + "'" +
            ", vDesremipe='" + getvDesremipe() + "'" +
            ", vCoddepart='" + getvCoddepart() + "'" +
            ", vCodprovin='" + getvCodprovin() + "'" +
            ", vCoddistri='" + getvCoddistri() + "'" +
            ", vDesvia='" + getvDesvia() + "'" +
            ", vDeszona='" + getvDeszona() + "'" +
            ", vDircomp='" + getvDircomp() + "'" +
            ", vDirdenu='" + getvDirdenu() + "'" +
            ", tFecinitra='" + gettFecinitra() + "'" +
            ", nFlaglun='" + isnFlaglun() + "'" +
            ", nFlagmar='" + isnFlagmar() + "'" +
            ", nFlagmie='" + isnFlagmie() + "'" +
            ", nFlagjue='" + isnFlagjue() + "'" +
            ", nFlagvie='" + isnFlagvie() + "'" +
            ", nFlagsab='" + isnFlagsab() + "'" +
            ", nFlagdom='" + isnFlagdom() + "'" +
            ", tHorainit='" + gettHorainit() + "'" +
            ", tHorafint='" + gettHorafint() + "'" +
            ", vFlgtraba='" + isvFlgtraba() + "'" +
            ", vFlgrepre='" + isvFlgrepre() + "'" +
            ", tFeccese='" + gettFeccese() + "'" +
            ", vDesrepre='" + getvDesrepre() + "'" +
            ", vObscalifi='" + getvObscalifi() + "'" +
            ", vObsfin='" + getvObsfin() + "'" +
            ", vFlgestado='" + getvFlgestado() + "'" +
            ", vUsuareg='" + getvUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", vUsuaupd='" + getvUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
