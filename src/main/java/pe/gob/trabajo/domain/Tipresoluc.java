package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tipresoluc.
 */
@Entity
@Table(name = "pjtbc_tipresoluc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjtbc_tipresoluc")
public class Tipresoluc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codtipres", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_destipres", length = 50, nullable = false)
    private String vDestipres;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "tipresoluc")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Tipdiligenc> tipdiligencs = new HashSet<>();

    @OneToMany(mappedBy = "tipresoluc")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Legtipdoc> legtipdocs = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvDestipres() {
        return vDestipres;
    }

    public Tipresoluc vDestipres(String vDestipres) {
        this.vDestipres = vDestipres;
        return this;
    }

    public void setvDestipres(String vDestipres) {
        this.vDestipres = vDestipres;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Tipresoluc nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Tipresoluc tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Tipresoluc nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Tipresoluc nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Tipresoluc nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Tipresoluc tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Tipresoluc nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Tipdiligenc> getTipdiligencs() {
        return tipdiligencs;
    }

    public Tipresoluc tipdiligencs(Set<Tipdiligenc> tipdiligencs) {
        this.tipdiligencs = tipdiligencs;
        return this;
    }

    public Tipresoluc addTipdiligenc(Tipdiligenc tipdiligenc) {
        this.tipdiligencs.add(tipdiligenc);
        tipdiligenc.setTipresoluc(this);
        return this;
    }

    public Tipresoluc removeTipdiligenc(Tipdiligenc tipdiligenc) {
        this.tipdiligencs.remove(tipdiligenc);
        tipdiligenc.setTipresoluc(null);
        return this;
    }

    public void setTipdiligencs(Set<Tipdiligenc> tipdiligencs) {
        this.tipdiligencs = tipdiligencs;
    }

    public Set<Legtipdoc> getLegtipdocs() {
        return legtipdocs;
    }

    public Tipresoluc legtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
        return this;
    }

    public Tipresoluc addLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.add(legtipdoc);
        legtipdoc.setTipresoluc(this);
        return this;
    }

    public Tipresoluc removeLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.remove(legtipdoc);
        legtipdoc.setTipresoluc(null);
        return this;
    }

    public void setLegtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tipresoluc tipresoluc = (Tipresoluc) o;
        if (tipresoluc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipresoluc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tipresoluc{" +
            "id=" + getId() +
            ", vDestipres='" + getvDestipres() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
