package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Atencionpj.
 */
@Entity
@Table(name = "pjtbd_atencionpj")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjtbd_atencionpj")
public class Atencionpj implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codatenpj", nullable = false)
    
    private Long id;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codreg", length = 2, nullable = false)
    private String vCodreg;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codzon", length = 2, nullable = false)
    private String vCodzon;

    @NotNull
    @Column(name = "n_correl", nullable = false)
    private Integer nCorrel;

    @Column(name = "d_fecaten")
    private LocalDate dFecaten;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_obsaten", length = 200, nullable = false)
    private String vObsaten;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgtel", length = 1, nullable = false)
    private String vFlgtel;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgadm", length = 1, nullable = false)
    private String vFlgadm;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgtelr", length = 1, nullable = false)
    private String vFlgtelr;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgaten", length = 1, nullable = false)
    private String vFlgaten;

    @NotNull
    @Size(max = 6)
    @Column(name = "v_codloc", length = 6, nullable = false)
    private String vCodloc;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codlegajo")
    private Legajo legajo;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodreg() {
        return vCodreg;
    }

    public Atencionpj vCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
        return this;
    }

    public void setvCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getvCodzon() {
        return vCodzon;
    }

    public Atencionpj vCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
        return this;
    }

    public void setvCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
    }

    public Integer getnCorrel() {
        return nCorrel;
    }

    public Atencionpj nCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
        return this;
    }

    public void setnCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
    }

    public LocalDate getdFecaten() {
        return dFecaten;
    }

    public Atencionpj dFecaten(LocalDate dFecaten) {
        this.dFecaten = dFecaten;
        return this;
    }

    public void setdFecaten(LocalDate dFecaten) {
        this.dFecaten = dFecaten;
    }

    public String getvObsaten() {
        return vObsaten;
    }

    public Atencionpj vObsaten(String vObsaten) {
        this.vObsaten = vObsaten;
        return this;
    }

    public void setvObsaten(String vObsaten) {
        this.vObsaten = vObsaten;
    }

    public String getvFlgtel() {
        return vFlgtel;
    }

    public Atencionpj vFlgtel(String vFlgtel) {
        this.vFlgtel = vFlgtel;
        return this;
    }

    public void setvFlgtel(String vFlgtel) {
        this.vFlgtel = vFlgtel;
    }

    public String getvFlgadm() {
        return vFlgadm;
    }

    public Atencionpj vFlgadm(String vFlgadm) {
        this.vFlgadm = vFlgadm;
        return this;
    }

    public void setvFlgadm(String vFlgadm) {
        this.vFlgadm = vFlgadm;
    }

    public String getvFlgtelr() {
        return vFlgtelr;
    }

    public Atencionpj vFlgtelr(String vFlgtelr) {
        this.vFlgtelr = vFlgtelr;
        return this;
    }

    public void setvFlgtelr(String vFlgtelr) {
        this.vFlgtelr = vFlgtelr;
    }

    public String getvFlgaten() {
        return vFlgaten;
    }

    public Atencionpj vFlgaten(String vFlgaten) {
        this.vFlgaten = vFlgaten;
        return this;
    }

    public void setvFlgaten(String vFlgaten) {
        this.vFlgaten = vFlgaten;
    }

    public String getvCodloc() {
        return vCodloc;
    }

    public Atencionpj vCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
        return this;
    }

    public void setvCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Atencionpj nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Atencionpj tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Atencionpj nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Atencionpj nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Atencionpj nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Atencionpj tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Atencionpj nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Legajo getLegajo() {
        return legajo;
    }

    public Atencionpj legajo(Legajo legajo) {
        this.legajo = legajo;
        return this;
    }

    public void setLegajo(Legajo legajo) {
        this.legajo = legajo;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Atencionpj atencionpj = (Atencionpj) o;
        if (atencionpj.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), atencionpj.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Atencionpj{" +
            "id=" + getId() +
            ", vCodreg='" + getvCodreg() + "'" +
            ", vCodzon='" + getvCodzon() + "'" +
            ", nCorrel='" + getnCorrel() + "'" +
            ", dFecaten='" + getdFecaten() + "'" +
            ", vObsaten='" + getvObsaten() + "'" +
            ", vFlgtel='" + getvFlgtel() + "'" +
            ", vFlgadm='" + getvFlgadm() + "'" +
            ", vFlgtelr='" + getvFlgtelr() + "'" +
            ", vFlgaten='" + getvFlgaten() + "'" +
            ", vCodloc='" + getvCodloc() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
