package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Infosoli;
import pe.gob.trabajo.repository.InfosoliRepository;
import pe.gob.trabajo.repository.search.InfosoliSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InfosoliResource REST controller.
 *
 * @see InfosoliResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class InfosoliResourceIntTest {

    private static final String DEFAULT_V_INFOSOLI = "AAAAAAAAAA";
    private static final String UPDATED_V_INFOSOLI = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECSOLI = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECSOLI = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_V_RESPUESTA = "AAAAAAAAAA";
    private static final String UPDATED_V_RESPUESTA = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECRESP = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECRESP = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private InfosoliRepository infosoliRepository;

    @Autowired
    private InfosoliSearchRepository infosoliSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInfosoliMockMvc;

    private Infosoli infosoli;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InfosoliResource infosoliResource = new InfosoliResource(infosoliRepository, infosoliSearchRepository);
        this.restInfosoliMockMvc = MockMvcBuilders.standaloneSetup(infosoliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Infosoli createEntity(EntityManager em) {
        Infosoli infosoli = new Infosoli()
            .vInfosoli(DEFAULT_V_INFOSOLI)
            .tFecsoli(DEFAULT_T_FECSOLI)
            .vRespuesta(DEFAULT_V_RESPUESTA)
            .tFecresp(DEFAULT_T_FECRESP)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return infosoli;
    }

    @Before
    public void initTest() {
        infosoliSearchRepository.deleteAll();
        infosoli = createEntity(em);
    }

    @Test
    @Transactional
    public void createInfosoli() throws Exception {
        int databaseSizeBeforeCreate = infosoliRepository.findAll().size();

        // Create the Infosoli
        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isCreated());

        // Validate the Infosoli in the database
        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeCreate + 1);
        Infosoli testInfosoli = infosoliList.get(infosoliList.size() - 1);
        assertThat(testInfosoli.getvInfosoli()).isEqualTo(DEFAULT_V_INFOSOLI);
        assertThat(testInfosoli.gettFecsoli()).isEqualTo(DEFAULT_T_FECSOLI);
        assertThat(testInfosoli.getvRespuesta()).isEqualTo(DEFAULT_V_RESPUESTA);
        assertThat(testInfosoli.gettFecresp()).isEqualTo(DEFAULT_T_FECRESP);
        assertThat(testInfosoli.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testInfosoli.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testInfosoli.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testInfosoli.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testInfosoli.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testInfosoli.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testInfosoli.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Infosoli in Elasticsearch
        Infosoli infosoliEs = infosoliSearchRepository.findOne(testInfosoli.getId());
        assertThat(infosoliEs).isEqualToComparingFieldByField(testInfosoli);
    }

    @Test
    @Transactional
    public void createInfosoliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = infosoliRepository.findAll().size();

        // Create the Infosoli with an existing ID
        infosoli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isBadRequest());

        // Validate the Infosoli in the database
        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvInfosoliIsRequired() throws Exception {
        int databaseSizeBeforeTest = infosoliRepository.findAll().size();
        // set the field null
        infosoli.setvInfosoli(null);

        // Create the Infosoli, which fails.

        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isBadRequest());

        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecsoliIsRequired() throws Exception {
        int databaseSizeBeforeTest = infosoliRepository.findAll().size();
        // set the field null
        infosoli.settFecsoli(null);

        // Create the Infosoli, which fails.

        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isBadRequest());

        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = infosoliRepository.findAll().size();
        // set the field null
        infosoli.setvUsuareg(null);

        // Create the Infosoli, which fails.

        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isBadRequest());

        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = infosoliRepository.findAll().size();
        // set the field null
        infosoli.settFecreg(null);

        // Create the Infosoli, which fails.

        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isBadRequest());

        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = infosoliRepository.findAll().size();
        // set the field null
        infosoli.setnSedereg(null);

        // Create the Infosoli, which fails.

        restInfosoliMockMvc.perform(post("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isBadRequest());

        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInfosolis() throws Exception {
        // Initialize the database
        infosoliRepository.saveAndFlush(infosoli);

        // Get all the infosoliList
        restInfosoliMockMvc.perform(get("/api/infosolis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(infosoli.getId().intValue())))
            .andExpect(jsonPath("$.[*].vInfosoli").value(hasItem(DEFAULT_V_INFOSOLI.toString())))
            .andExpect(jsonPath("$.[*].tFecsoli").value(hasItem(DEFAULT_T_FECSOLI.toString())))
            .andExpect(jsonPath("$.[*].vRespuesta").value(hasItem(DEFAULT_V_RESPUESTA.toString())))
            .andExpect(jsonPath("$.[*].tFecresp").value(hasItem(DEFAULT_T_FECRESP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getInfosoli() throws Exception {
        // Initialize the database
        infosoliRepository.saveAndFlush(infosoli);

        // Get the infosoli
        restInfosoliMockMvc.perform(get("/api/infosolis/{id}", infosoli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(infosoli.getId().intValue()))
            .andExpect(jsonPath("$.vInfosoli").value(DEFAULT_V_INFOSOLI.toString()))
            .andExpect(jsonPath("$.tFecsoli").value(DEFAULT_T_FECSOLI.toString()))
            .andExpect(jsonPath("$.vRespuesta").value(DEFAULT_V_RESPUESTA.toString()))
            .andExpect(jsonPath("$.tFecresp").value(DEFAULT_T_FECRESP.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingInfosoli() throws Exception {
        // Get the infosoli
        restInfosoliMockMvc.perform(get("/api/infosolis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInfosoli() throws Exception {
        // Initialize the database
        infosoliRepository.saveAndFlush(infosoli);
        infosoliSearchRepository.save(infosoli);
        int databaseSizeBeforeUpdate = infosoliRepository.findAll().size();

        // Update the infosoli
        Infosoli updatedInfosoli = infosoliRepository.findOne(infosoli.getId());
        updatedInfosoli
            .vInfosoli(UPDATED_V_INFOSOLI)
            .tFecsoli(UPDATED_T_FECSOLI)
            .vRespuesta(UPDATED_V_RESPUESTA)
            .tFecresp(UPDATED_T_FECRESP)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restInfosoliMockMvc.perform(put("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInfosoli)))
            .andExpect(status().isOk());

        // Validate the Infosoli in the database
        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeUpdate);
        Infosoli testInfosoli = infosoliList.get(infosoliList.size() - 1);
        assertThat(testInfosoli.getvInfosoli()).isEqualTo(UPDATED_V_INFOSOLI);
        assertThat(testInfosoli.gettFecsoli()).isEqualTo(UPDATED_T_FECSOLI);
        assertThat(testInfosoli.getvRespuesta()).isEqualTo(UPDATED_V_RESPUESTA);
        assertThat(testInfosoli.gettFecresp()).isEqualTo(UPDATED_T_FECRESP);
        assertThat(testInfosoli.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testInfosoli.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testInfosoli.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testInfosoli.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testInfosoli.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testInfosoli.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testInfosoli.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Infosoli in Elasticsearch
        Infosoli infosoliEs = infosoliSearchRepository.findOne(testInfosoli.getId());
        assertThat(infosoliEs).isEqualToComparingFieldByField(testInfosoli);
    }

    @Test
    @Transactional
    public void updateNonExistingInfosoli() throws Exception {
        int databaseSizeBeforeUpdate = infosoliRepository.findAll().size();

        // Create the Infosoli

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInfosoliMockMvc.perform(put("/api/infosolis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(infosoli)))
            .andExpect(status().isCreated());

        // Validate the Infosoli in the database
        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInfosoli() throws Exception {
        // Initialize the database
        infosoliRepository.saveAndFlush(infosoli);
        infosoliSearchRepository.save(infosoli);
        int databaseSizeBeforeDelete = infosoliRepository.findAll().size();

        // Get the infosoli
        restInfosoliMockMvc.perform(delete("/api/infosolis/{id}", infosoli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean infosoliExistsInEs = infosoliSearchRepository.exists(infosoli.getId());
        assertThat(infosoliExistsInEs).isFalse();

        // Validate the database is empty
        List<Infosoli> infosoliList = infosoliRepository.findAll();
        assertThat(infosoliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchInfosoli() throws Exception {
        // Initialize the database
        infosoliRepository.saveAndFlush(infosoli);
        infosoliSearchRepository.save(infosoli);

        // Search the infosoli
        restInfosoliMockMvc.perform(get("/api/_search/infosolis?query=id:" + infosoli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(infosoli.getId().intValue())))
            .andExpect(jsonPath("$.[*].vInfosoli").value(hasItem(DEFAULT_V_INFOSOLI.toString())))
            .andExpect(jsonPath("$.[*].tFecsoli").value(hasItem(DEFAULT_T_FECSOLI.toString())))
            .andExpect(jsonPath("$.[*].vRespuesta").value(hasItem(DEFAULT_V_RESPUESTA.toString())))
            .andExpect(jsonPath("$.[*].tFecresp").value(hasItem(DEFAULT_T_FECRESP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Infosoli.class);
        Infosoli infosoli1 = new Infosoli();
        infosoli1.setId(1L);
        Infosoli infosoli2 = new Infosoli();
        infosoli2.setId(infosoli1.getId());
        assertThat(infosoli1).isEqualTo(infosoli2);
        infosoli2.setId(2L);
        assertThat(infosoli1).isNotEqualTo(infosoli2);
        infosoli1.setId(null);
        assertThat(infosoli1).isNotEqualTo(infosoli2);
    }
}
