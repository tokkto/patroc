package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Tipvia;
import pe.gob.trabajo.repository.TipviaRepository;
import pe.gob.trabajo.repository.search.TipviaSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipviaResource REST controller.
 *
 * @see TipviaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class TipviaResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_DESCCORTA = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCCORTA = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipviaRepository tipviaRepository;

    @Autowired
    private TipviaSearchRepository tipviaSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipviaMockMvc;

    private Tipvia tipvia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipviaResource tipviaResource = new TipviaResource(tipviaRepository, tipviaSearchRepository);
        this.restTipviaMockMvc = MockMvcBuilders.standaloneSetup(tipviaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipvia createEntity(EntityManager em) {
        Tipvia tipvia = new Tipvia()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vDesccorta(DEFAULT_V_DESCCORTA)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipvia;
    }

    @Before
    public void initTest() {
        tipviaSearchRepository.deleteAll();
        tipvia = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipvia() throws Exception {
        int databaseSizeBeforeCreate = tipviaRepository.findAll().size();

        // Create the Tipvia
        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isCreated());

        // Validate the Tipvia in the database
        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeCreate + 1);
        Tipvia testTipvia = tipviaList.get(tipviaList.size() - 1);
        assertThat(testTipvia.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipvia.getvDesccorta()).isEqualTo(DEFAULT_V_DESCCORTA);
        assertThat(testTipvia.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testTipvia.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipvia.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipvia.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipvia.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testTipvia.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipvia.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipvia in Elasticsearch
        Tipvia tipviaEs = tipviaSearchRepository.findOne(testTipvia.getId());
        assertThat(tipviaEs).isEqualToComparingFieldByField(testTipvia);
    }

    @Test
    @Transactional
    public void createTipviaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipviaRepository.findAll().size();

        // Create the Tipvia with an existing ID
        tipvia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isBadRequest());

        // Validate the Tipvia in the database
        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipviaRepository.findAll().size();
        // set the field null
        tipvia.setvDescrip(null);

        // Create the Tipvia, which fails.

        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isBadRequest());

        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesccortaIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipviaRepository.findAll().size();
        // set the field null
        tipvia.setvDesccorta(null);

        // Create the Tipvia, which fails.

        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isBadRequest());

        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipviaRepository.findAll().size();
        // set the field null
        tipvia.setvUsuareg(null);

        // Create the Tipvia, which fails.

        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isBadRequest());

        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipviaRepository.findAll().size();
        // set the field null
        tipvia.settFecreg(null);

        // Create the Tipvia, which fails.

        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isBadRequest());

        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipviaRepository.findAll().size();
        // set the field null
        tipvia.setnSedereg(null);

        // Create the Tipvia, which fails.

        restTipviaMockMvc.perform(post("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isBadRequest());

        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipvias() throws Exception {
        // Initialize the database
        tipviaRepository.saveAndFlush(tipvia);

        // Get all the tipviaList
        restTipviaMockMvc.perform(get("/api/tipvias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipvia.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vDesccorta").value(hasItem(DEFAULT_V_DESCCORTA.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipvia() throws Exception {
        // Initialize the database
        tipviaRepository.saveAndFlush(tipvia);

        // Get the tipvia
        restTipviaMockMvc.perform(get("/api/tipvias/{id}", tipvia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipvia.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vDesccorta").value(DEFAULT_V_DESCCORTA.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipvia() throws Exception {
        // Get the tipvia
        restTipviaMockMvc.perform(get("/api/tipvias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipvia() throws Exception {
        // Initialize the database
        tipviaRepository.saveAndFlush(tipvia);
        tipviaSearchRepository.save(tipvia);
        int databaseSizeBeforeUpdate = tipviaRepository.findAll().size();

        // Update the tipvia
        Tipvia updatedTipvia = tipviaRepository.findOne(tipvia.getId());
        updatedTipvia
            .vDescrip(UPDATED_V_DESCRIP)
            .vDesccorta(UPDATED_V_DESCCORTA)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipviaMockMvc.perform(put("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipvia)))
            .andExpect(status().isOk());

        // Validate the Tipvia in the database
        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeUpdate);
        Tipvia testTipvia = tipviaList.get(tipviaList.size() - 1);
        assertThat(testTipvia.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipvia.getvDesccorta()).isEqualTo(UPDATED_V_DESCCORTA);
        assertThat(testTipvia.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testTipvia.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipvia.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipvia.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipvia.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testTipvia.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipvia.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipvia in Elasticsearch
        Tipvia tipviaEs = tipviaSearchRepository.findOne(testTipvia.getId());
        assertThat(tipviaEs).isEqualToComparingFieldByField(testTipvia);
    }

    @Test
    @Transactional
    public void updateNonExistingTipvia() throws Exception {
        int databaseSizeBeforeUpdate = tipviaRepository.findAll().size();

        // Create the Tipvia

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipviaMockMvc.perform(put("/api/tipvias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipvia)))
            .andExpect(status().isCreated());

        // Validate the Tipvia in the database
        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipvia() throws Exception {
        // Initialize the database
        tipviaRepository.saveAndFlush(tipvia);
        tipviaSearchRepository.save(tipvia);
        int databaseSizeBeforeDelete = tipviaRepository.findAll().size();

        // Get the tipvia
        restTipviaMockMvc.perform(delete("/api/tipvias/{id}", tipvia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipviaExistsInEs = tipviaSearchRepository.exists(tipvia.getId());
        assertThat(tipviaExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipvia> tipviaList = tipviaRepository.findAll();
        assertThat(tipviaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipvia() throws Exception {
        // Initialize the database
        tipviaRepository.saveAndFlush(tipvia);
        tipviaSearchRepository.save(tipvia);

        // Search the tipvia
        restTipviaMockMvc.perform(get("/api/_search/tipvias?query=id:" + tipvia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipvia.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vDesccorta").value(hasItem(DEFAULT_V_DESCCORTA.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipvia.class);
        Tipvia tipvia1 = new Tipvia();
        tipvia1.setId(1L);
        Tipvia tipvia2 = new Tipvia();
        tipvia2.setId(tipvia1.getId());
        assertThat(tipvia1).isEqualTo(tipvia2);
        tipvia2.setId(2L);
        assertThat(tipvia1).isNotEqualTo(tipvia2);
        tipvia1.setId(null);
        assertThat(tipvia1).isNotEqualTo(tipvia2);
    }
}
