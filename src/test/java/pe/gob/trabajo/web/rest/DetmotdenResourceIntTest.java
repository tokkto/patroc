package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Detmotden;
import pe.gob.trabajo.repository.DetmotdenRepository;
import pe.gob.trabajo.repository.search.DetmotdenSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DetmotdenResource REST controller.
 *
 * @see DetmotdenResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class DetmotdenResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DetmotdenRepository detmotdenRepository;

    @Autowired
    private DetmotdenSearchRepository detmotdenSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDetmotdenMockMvc;

    private Detmotden detmotden;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DetmotdenResource detmotdenResource = new DetmotdenResource(detmotdenRepository, detmotdenSearchRepository);
        this.restDetmotdenMockMvc = MockMvcBuilders.standaloneSetup(detmotdenResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Detmotden createEntity(EntityManager em) {
        Detmotden detmotden = new Detmotden()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return detmotden;
    }

    @Before
    public void initTest() {
        detmotdenSearchRepository.deleteAll();
        detmotden = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetmotden() throws Exception {
        int databaseSizeBeforeCreate = detmotdenRepository.findAll().size();

        // Create the Detmotden
        restDetmotdenMockMvc.perform(post("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isCreated());

        // Validate the Detmotden in the database
        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeCreate + 1);
        Detmotden testDetmotden = detmotdenList.get(detmotdenList.size() - 1);
        assertThat(testDetmotden.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testDetmotden.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testDetmotden.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDetmotden.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDetmotden.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDetmotden.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testDetmotden.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDetmotden.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Detmotden in Elasticsearch
        Detmotden detmotdenEs = detmotdenSearchRepository.findOne(testDetmotden.getId());
        assertThat(detmotdenEs).isEqualToComparingFieldByField(testDetmotden);
    }

    @Test
    @Transactional
    public void createDetmotdenWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detmotdenRepository.findAll().size();

        // Create the Detmotden with an existing ID
        detmotden.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetmotdenMockMvc.perform(post("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isBadRequest());

        // Validate the Detmotden in the database
        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = detmotdenRepository.findAll().size();
        // set the field null
        detmotden.setvDescrip(null);

        // Create the Detmotden, which fails.

        restDetmotdenMockMvc.perform(post("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isBadRequest());

        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = detmotdenRepository.findAll().size();
        // set the field null
        detmotden.setvUsuareg(null);

        // Create the Detmotden, which fails.

        restDetmotdenMockMvc.perform(post("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isBadRequest());

        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = detmotdenRepository.findAll().size();
        // set the field null
        detmotden.settFecreg(null);

        // Create the Detmotden, which fails.

        restDetmotdenMockMvc.perform(post("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isBadRequest());

        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = detmotdenRepository.findAll().size();
        // set the field null
        detmotden.setnSedereg(null);

        // Create the Detmotden, which fails.

        restDetmotdenMockMvc.perform(post("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isBadRequest());

        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDetmotdens() throws Exception {
        // Initialize the database
        detmotdenRepository.saveAndFlush(detmotden);

        // Get all the detmotdenList
        restDetmotdenMockMvc.perform(get("/api/detmotdens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detmotden.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDetmotden() throws Exception {
        // Initialize the database
        detmotdenRepository.saveAndFlush(detmotden);

        // Get the detmotden
        restDetmotdenMockMvc.perform(get("/api/detmotdens/{id}", detmotden.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(detmotden.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDetmotden() throws Exception {
        // Get the detmotden
        restDetmotdenMockMvc.perform(get("/api/detmotdens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetmotden() throws Exception {
        // Initialize the database
        detmotdenRepository.saveAndFlush(detmotden);
        detmotdenSearchRepository.save(detmotden);
        int databaseSizeBeforeUpdate = detmotdenRepository.findAll().size();

        // Update the detmotden
        Detmotden updatedDetmotden = detmotdenRepository.findOne(detmotden.getId());
        updatedDetmotden
            .vDescrip(UPDATED_V_DESCRIP)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDetmotdenMockMvc.perform(put("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDetmotden)))
            .andExpect(status().isOk());

        // Validate the Detmotden in the database
        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeUpdate);
        Detmotden testDetmotden = detmotdenList.get(detmotdenList.size() - 1);
        assertThat(testDetmotden.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testDetmotden.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testDetmotden.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDetmotden.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDetmotden.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDetmotden.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testDetmotden.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDetmotden.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Detmotden in Elasticsearch
        Detmotden detmotdenEs = detmotdenSearchRepository.findOne(testDetmotden.getId());
        assertThat(detmotdenEs).isEqualToComparingFieldByField(testDetmotden);
    }

    @Test
    @Transactional
    public void updateNonExistingDetmotden() throws Exception {
        int databaseSizeBeforeUpdate = detmotdenRepository.findAll().size();

        // Create the Detmotden

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDetmotdenMockMvc.perform(put("/api/detmotdens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detmotden)))
            .andExpect(status().isCreated());

        // Validate the Detmotden in the database
        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDetmotden() throws Exception {
        // Initialize the database
        detmotdenRepository.saveAndFlush(detmotden);
        detmotdenSearchRepository.save(detmotden);
        int databaseSizeBeforeDelete = detmotdenRepository.findAll().size();

        // Get the detmotden
        restDetmotdenMockMvc.perform(delete("/api/detmotdens/{id}", detmotden.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean detmotdenExistsInEs = detmotdenSearchRepository.exists(detmotden.getId());
        assertThat(detmotdenExistsInEs).isFalse();

        // Validate the database is empty
        List<Detmotden> detmotdenList = detmotdenRepository.findAll();
        assertThat(detmotdenList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDetmotden() throws Exception {
        // Initialize the database
        detmotdenRepository.saveAndFlush(detmotden);
        detmotdenSearchRepository.save(detmotden);

        // Search the detmotden
        restDetmotdenMockMvc.perform(get("/api/_search/detmotdens?query=id:" + detmotden.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detmotden.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Detmotden.class);
        Detmotden detmotden1 = new Detmotden();
        detmotden1.setId(1L);
        Detmotden detmotden2 = new Detmotden();
        detmotden2.setId(detmotden1.getId());
        assertThat(detmotden1).isEqualTo(detmotden2);
        detmotden2.setId(2L);
        assertThat(detmotden1).isNotEqualTo(detmotden2);
        detmotden1.setId(null);
        assertThat(detmotden1).isNotEqualTo(detmotden2);
    }
}
