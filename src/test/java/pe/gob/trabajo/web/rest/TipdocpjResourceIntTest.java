package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Tipdocpj;
import pe.gob.trabajo.repository.TipdocpjRepository;
import pe.gob.trabajo.repository.search.TipdocpjSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipdocpjResource REST controller.
 *
 * @see TipdocpjResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class TipdocpjResourceIntTest {

    private static final String DEFAULT_V_CODSIS = "AAA";
    private static final String UPDATED_V_CODSIS = "BBB";

    private static final String DEFAULT_V_DESTDOC = "AAAAAAAAAA";
    private static final String UPDATED_V_DESTDOC = "BBBBBBBBBB";

    private static final String DEFAULT_V_FLGTIP = "A";
    private static final String UPDATED_V_FLGTIP = "B";

    private static final String DEFAULT_V_FLGRES = "A";
    private static final String UPDATED_V_FLGRES = "B";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipdocpjRepository tipdocpjRepository;

    @Autowired
    private TipdocpjSearchRepository tipdocpjSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipdocpjMockMvc;

    private Tipdocpj tipdocpj;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipdocpjResource tipdocpjResource = new TipdocpjResource(tipdocpjRepository, tipdocpjSearchRepository);
        this.restTipdocpjMockMvc = MockMvcBuilders.standaloneSetup(tipdocpjResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipdocpj createEntity(EntityManager em) {
        Tipdocpj tipdocpj = new Tipdocpj()
            .vCodsis(DEFAULT_V_CODSIS)
            .vDestdoc(DEFAULT_V_DESTDOC)
            .vFlgtip(DEFAULT_V_FLGTIP)
            .vFlgres(DEFAULT_V_FLGRES)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipdocpj;
    }

    @Before
    public void initTest() {
        tipdocpjSearchRepository.deleteAll();
        tipdocpj = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipdocpj() throws Exception {
        int databaseSizeBeforeCreate = tipdocpjRepository.findAll().size();

        // Create the Tipdocpj
        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isCreated());

        // Validate the Tipdocpj in the database
        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeCreate + 1);
        Tipdocpj testTipdocpj = tipdocpjList.get(tipdocpjList.size() - 1);
        assertThat(testTipdocpj.getvCodsis()).isEqualTo(DEFAULT_V_CODSIS);
        assertThat(testTipdocpj.getvDestdoc()).isEqualTo(DEFAULT_V_DESTDOC);
        assertThat(testTipdocpj.getvFlgtip()).isEqualTo(DEFAULT_V_FLGTIP);
        assertThat(testTipdocpj.getvFlgres()).isEqualTo(DEFAULT_V_FLGRES);
        assertThat(testTipdocpj.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipdocpj.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipdocpj.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipdocpj.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipdocpj.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipdocpj.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipdocpj.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipdocpj in Elasticsearch
        Tipdocpj tipdocpjEs = tipdocpjSearchRepository.findOne(testTipdocpj.getId());
        assertThat(tipdocpjEs).isEqualToComparingFieldByField(testTipdocpj);
    }

    @Test
    @Transactional
    public void createTipdocpjWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipdocpjRepository.findAll().size();

        // Create the Tipdocpj with an existing ID
        tipdocpj.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        // Validate the Tipdocpj in the database
        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodsisIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setvCodsis(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDestdocIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setvDestdoc(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgtipIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setvFlgtip(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgresIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setvFlgres(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setnUsuareg(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.settFecreg(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setnFlgactivo(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocpjRepository.findAll().size();
        // set the field null
        tipdocpj.setnSedereg(null);

        // Create the Tipdocpj, which fails.

        restTipdocpjMockMvc.perform(post("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isBadRequest());

        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipdocpjs() throws Exception {
        // Initialize the database
        tipdocpjRepository.saveAndFlush(tipdocpj);

        // Get all the tipdocpjList
        restTipdocpjMockMvc.perform(get("/api/tipdocpjs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipdocpj.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].vDestdoc").value(hasItem(DEFAULT_V_DESTDOC.toString())))
            .andExpect(jsonPath("$.[*].vFlgtip").value(hasItem(DEFAULT_V_FLGTIP.toString())))
            .andExpect(jsonPath("$.[*].vFlgres").value(hasItem(DEFAULT_V_FLGRES.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipdocpj() throws Exception {
        // Initialize the database
        tipdocpjRepository.saveAndFlush(tipdocpj);

        // Get the tipdocpj
        restTipdocpjMockMvc.perform(get("/api/tipdocpjs/{id}", tipdocpj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipdocpj.getId().intValue()))
            .andExpect(jsonPath("$.vCodsis").value(DEFAULT_V_CODSIS.toString()))
            .andExpect(jsonPath("$.vDestdoc").value(DEFAULT_V_DESTDOC.toString()))
            .andExpect(jsonPath("$.vFlgtip").value(DEFAULT_V_FLGTIP.toString()))
            .andExpect(jsonPath("$.vFlgres").value(DEFAULT_V_FLGRES.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipdocpj() throws Exception {
        // Get the tipdocpj
        restTipdocpjMockMvc.perform(get("/api/tipdocpjs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipdocpj() throws Exception {
        // Initialize the database
        tipdocpjRepository.saveAndFlush(tipdocpj);
        tipdocpjSearchRepository.save(tipdocpj);
        int databaseSizeBeforeUpdate = tipdocpjRepository.findAll().size();

        // Update the tipdocpj
        Tipdocpj updatedTipdocpj = tipdocpjRepository.findOne(tipdocpj.getId());
        updatedTipdocpj
            .vCodsis(UPDATED_V_CODSIS)
            .vDestdoc(UPDATED_V_DESTDOC)
            .vFlgtip(UPDATED_V_FLGTIP)
            .vFlgres(UPDATED_V_FLGRES)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipdocpjMockMvc.perform(put("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipdocpj)))
            .andExpect(status().isOk());

        // Validate the Tipdocpj in the database
        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeUpdate);
        Tipdocpj testTipdocpj = tipdocpjList.get(tipdocpjList.size() - 1);
        assertThat(testTipdocpj.getvCodsis()).isEqualTo(UPDATED_V_CODSIS);
        assertThat(testTipdocpj.getvDestdoc()).isEqualTo(UPDATED_V_DESTDOC);
        assertThat(testTipdocpj.getvFlgtip()).isEqualTo(UPDATED_V_FLGTIP);
        assertThat(testTipdocpj.getvFlgres()).isEqualTo(UPDATED_V_FLGRES);
        assertThat(testTipdocpj.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipdocpj.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipdocpj.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipdocpj.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipdocpj.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipdocpj.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipdocpj.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipdocpj in Elasticsearch
        Tipdocpj tipdocpjEs = tipdocpjSearchRepository.findOne(testTipdocpj.getId());
        assertThat(tipdocpjEs).isEqualToComparingFieldByField(testTipdocpj);
    }

    @Test
    @Transactional
    public void updateNonExistingTipdocpj() throws Exception {
        int databaseSizeBeforeUpdate = tipdocpjRepository.findAll().size();

        // Create the Tipdocpj

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipdocpjMockMvc.perform(put("/api/tipdocpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocpj)))
            .andExpect(status().isCreated());

        // Validate the Tipdocpj in the database
        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipdocpj() throws Exception {
        // Initialize the database
        tipdocpjRepository.saveAndFlush(tipdocpj);
        tipdocpjSearchRepository.save(tipdocpj);
        int databaseSizeBeforeDelete = tipdocpjRepository.findAll().size();

        // Get the tipdocpj
        restTipdocpjMockMvc.perform(delete("/api/tipdocpjs/{id}", tipdocpj.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipdocpjExistsInEs = tipdocpjSearchRepository.exists(tipdocpj.getId());
        assertThat(tipdocpjExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipdocpj> tipdocpjList = tipdocpjRepository.findAll();
        assertThat(tipdocpjList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipdocpj() throws Exception {
        // Initialize the database
        tipdocpjRepository.saveAndFlush(tipdocpj);
        tipdocpjSearchRepository.save(tipdocpj);

        // Search the tipdocpj
        restTipdocpjMockMvc.perform(get("/api/_search/tipdocpjs?query=id:" + tipdocpj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipdocpj.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].vDestdoc").value(hasItem(DEFAULT_V_DESTDOC.toString())))
            .andExpect(jsonPath("$.[*].vFlgtip").value(hasItem(DEFAULT_V_FLGTIP.toString())))
            .andExpect(jsonPath("$.[*].vFlgres").value(hasItem(DEFAULT_V_FLGRES.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipdocpj.class);
        Tipdocpj tipdocpj1 = new Tipdocpj();
        tipdocpj1.setId(1L);
        Tipdocpj tipdocpj2 = new Tipdocpj();
        tipdocpj2.setId(tipdocpj1.getId());
        assertThat(tipdocpj1).isEqualTo(tipdocpj2);
        tipdocpj2.setId(2L);
        assertThat(tipdocpj1).isNotEqualTo(tipdocpj2);
        tipdocpj1.setId(null);
        assertThat(tipdocpj1).isNotEqualTo(tipdocpj2);
    }
}
