package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Dirdenun;
import pe.gob.trabajo.repository.DirdenunRepository;
import pe.gob.trabajo.repository.search.DirdenunSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DirdenunResource REST controller.
 *
 * @see DirdenunResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class DirdenunResourceIntTest {

    private static final String DEFAULT_V_CODDEPART = "AAAAAAAAAA";
    private static final String UPDATED_V_CODDEPART = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODPROVIN = "AAAAAAAAAA";
    private static final String UPDATED_V_CODPROVIN = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODDISTRI = "AAAAAAAAAA";
    private static final String UPDATED_V_CODDISTRI = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_CODTZONA = 1;
    private static final Integer UPDATED_N_CODTZONA = 2;

    private static final String DEFAULT_V_DESZONA = "AAAAAAAAAA";
    private static final String UPDATED_V_DESZONA = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_CODTIPVIA = 1;
    private static final Integer UPDATED_N_CODTIPVIA = 2;

    private static final String DEFAULT_V_DESVIA = "AAAAAAAAAA";
    private static final String UPDATED_V_DESVIA = "BBBBBBBBBB";

    private static final String DEFAULT_V_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_V_DIRECCION = "BBBBBBBBBB";

    private static final String DEFAULT_V_DIRCOMPLE = "AAAAAAAAAA";
    private static final String UPDATED_V_DIRCOMPLE = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DirdenunRepository dirdenunRepository;

    @Autowired
    private DirdenunSearchRepository dirdenunSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDirdenunMockMvc;

    private Dirdenun dirdenun;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DirdenunResource dirdenunResource = new DirdenunResource(dirdenunRepository, dirdenunSearchRepository);
        this.restDirdenunMockMvc = MockMvcBuilders.standaloneSetup(dirdenunResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dirdenun createEntity(EntityManager em) {
        Dirdenun dirdenun = new Dirdenun()
            .vCoddepart(DEFAULT_V_CODDEPART)
            .vCodprovin(DEFAULT_V_CODPROVIN)
            .vCoddistri(DEFAULT_V_CODDISTRI)
            .nCodtzona(DEFAULT_N_CODTZONA)
            .vDeszona(DEFAULT_V_DESZONA)
            .nCodtipvia(DEFAULT_N_CODTIPVIA)
            .vDesvia(DEFAULT_V_DESVIA)
            .vDireccion(DEFAULT_V_DIRECCION)
            .vDircomple(DEFAULT_V_DIRCOMPLE)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return dirdenun;
    }

    @Before
    public void initTest() {
        dirdenunSearchRepository.deleteAll();
        dirdenun = createEntity(em);
    }

    @Test
    @Transactional
    public void createDirdenun() throws Exception {
        int databaseSizeBeforeCreate = dirdenunRepository.findAll().size();

        // Create the Dirdenun
        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isCreated());

        // Validate the Dirdenun in the database
        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeCreate + 1);
        Dirdenun testDirdenun = dirdenunList.get(dirdenunList.size() - 1);
        assertThat(testDirdenun.getvCoddepart()).isEqualTo(DEFAULT_V_CODDEPART);
        assertThat(testDirdenun.getvCodprovin()).isEqualTo(DEFAULT_V_CODPROVIN);
        assertThat(testDirdenun.getvCoddistri()).isEqualTo(DEFAULT_V_CODDISTRI);
        assertThat(testDirdenun.getnCodtzona()).isEqualTo(DEFAULT_N_CODTZONA);
        assertThat(testDirdenun.getvDeszona()).isEqualTo(DEFAULT_V_DESZONA);
        assertThat(testDirdenun.getnCodtipvia()).isEqualTo(DEFAULT_N_CODTIPVIA);
        assertThat(testDirdenun.getvDesvia()).isEqualTo(DEFAULT_V_DESVIA);
        assertThat(testDirdenun.getvDireccion()).isEqualTo(DEFAULT_V_DIRECCION);
        assertThat(testDirdenun.getvDircomple()).isEqualTo(DEFAULT_V_DIRCOMPLE);
        assertThat(testDirdenun.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testDirdenun.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDirdenun.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDirdenun.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDirdenun.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testDirdenun.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDirdenun.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Dirdenun in Elasticsearch
        Dirdenun dirdenunEs = dirdenunSearchRepository.findOne(testDirdenun.getId());
        assertThat(dirdenunEs).isEqualToComparingFieldByField(testDirdenun);
    }

    @Test
    @Transactional
    public void createDirdenunWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dirdenunRepository.findAll().size();

        // Create the Dirdenun with an existing ID
        dirdenun.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        // Validate the Dirdenun in the database
        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCoddepartIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvCoddepart(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodprovinIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvCodprovin(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCoddistriIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvCoddistri(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCodtzonaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setnCodtzona(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDeszonaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvDeszona(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCodtipviaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setnCodtipvia(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesviaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvDesvia(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDireccionIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvDireccion(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDircompleIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvDircomple(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setvUsuareg(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.settFecreg(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = dirdenunRepository.findAll().size();
        // set the field null
        dirdenun.setnSedereg(null);

        // Create the Dirdenun, which fails.

        restDirdenunMockMvc.perform(post("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isBadRequest());

        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDirdenuns() throws Exception {
        // Initialize the database
        dirdenunRepository.saveAndFlush(dirdenun);

        // Get all the dirdenunList
        restDirdenunMockMvc.perform(get("/api/dirdenuns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dirdenun.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCoddepart").value(hasItem(DEFAULT_V_CODDEPART.toString())))
            .andExpect(jsonPath("$.[*].vCodprovin").value(hasItem(DEFAULT_V_CODPROVIN.toString())))
            .andExpect(jsonPath("$.[*].vCoddistri").value(hasItem(DEFAULT_V_CODDISTRI.toString())))
            .andExpect(jsonPath("$.[*].nCodtzona").value(hasItem(DEFAULT_N_CODTZONA)))
            .andExpect(jsonPath("$.[*].vDeszona").value(hasItem(DEFAULT_V_DESZONA.toString())))
            .andExpect(jsonPath("$.[*].nCodtipvia").value(hasItem(DEFAULT_N_CODTIPVIA)))
            .andExpect(jsonPath("$.[*].vDesvia").value(hasItem(DEFAULT_V_DESVIA.toString())))
            .andExpect(jsonPath("$.[*].vDireccion").value(hasItem(DEFAULT_V_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].vDircomple").value(hasItem(DEFAULT_V_DIRCOMPLE.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDirdenun() throws Exception {
        // Initialize the database
        dirdenunRepository.saveAndFlush(dirdenun);

        // Get the dirdenun
        restDirdenunMockMvc.perform(get("/api/dirdenuns/{id}", dirdenun.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dirdenun.getId().intValue()))
            .andExpect(jsonPath("$.vCoddepart").value(DEFAULT_V_CODDEPART.toString()))
            .andExpect(jsonPath("$.vCodprovin").value(DEFAULT_V_CODPROVIN.toString()))
            .andExpect(jsonPath("$.vCoddistri").value(DEFAULT_V_CODDISTRI.toString()))
            .andExpect(jsonPath("$.nCodtzona").value(DEFAULT_N_CODTZONA))
            .andExpect(jsonPath("$.vDeszona").value(DEFAULT_V_DESZONA.toString()))
            .andExpect(jsonPath("$.nCodtipvia").value(DEFAULT_N_CODTIPVIA))
            .andExpect(jsonPath("$.vDesvia").value(DEFAULT_V_DESVIA.toString()))
            .andExpect(jsonPath("$.vDireccion").value(DEFAULT_V_DIRECCION.toString()))
            .andExpect(jsonPath("$.vDircomple").value(DEFAULT_V_DIRCOMPLE.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDirdenun() throws Exception {
        // Get the dirdenun
        restDirdenunMockMvc.perform(get("/api/dirdenuns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDirdenun() throws Exception {
        // Initialize the database
        dirdenunRepository.saveAndFlush(dirdenun);
        dirdenunSearchRepository.save(dirdenun);
        int databaseSizeBeforeUpdate = dirdenunRepository.findAll().size();

        // Update the dirdenun
        Dirdenun updatedDirdenun = dirdenunRepository.findOne(dirdenun.getId());
        updatedDirdenun
            .vCoddepart(UPDATED_V_CODDEPART)
            .vCodprovin(UPDATED_V_CODPROVIN)
            .vCoddistri(UPDATED_V_CODDISTRI)
            .nCodtzona(UPDATED_N_CODTZONA)
            .vDeszona(UPDATED_V_DESZONA)
            .nCodtipvia(UPDATED_N_CODTIPVIA)
            .vDesvia(UPDATED_V_DESVIA)
            .vDireccion(UPDATED_V_DIRECCION)
            .vDircomple(UPDATED_V_DIRCOMPLE)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDirdenunMockMvc.perform(put("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDirdenun)))
            .andExpect(status().isOk());

        // Validate the Dirdenun in the database
        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeUpdate);
        Dirdenun testDirdenun = dirdenunList.get(dirdenunList.size() - 1);
        assertThat(testDirdenun.getvCoddepart()).isEqualTo(UPDATED_V_CODDEPART);
        assertThat(testDirdenun.getvCodprovin()).isEqualTo(UPDATED_V_CODPROVIN);
        assertThat(testDirdenun.getvCoddistri()).isEqualTo(UPDATED_V_CODDISTRI);
        assertThat(testDirdenun.getnCodtzona()).isEqualTo(UPDATED_N_CODTZONA);
        assertThat(testDirdenun.getvDeszona()).isEqualTo(UPDATED_V_DESZONA);
        assertThat(testDirdenun.getnCodtipvia()).isEqualTo(UPDATED_N_CODTIPVIA);
        assertThat(testDirdenun.getvDesvia()).isEqualTo(UPDATED_V_DESVIA);
        assertThat(testDirdenun.getvDireccion()).isEqualTo(UPDATED_V_DIRECCION);
        assertThat(testDirdenun.getvDircomple()).isEqualTo(UPDATED_V_DIRCOMPLE);
        assertThat(testDirdenun.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testDirdenun.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDirdenun.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDirdenun.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDirdenun.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testDirdenun.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDirdenun.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Dirdenun in Elasticsearch
        Dirdenun dirdenunEs = dirdenunSearchRepository.findOne(testDirdenun.getId());
        assertThat(dirdenunEs).isEqualToComparingFieldByField(testDirdenun);
    }

    @Test
    @Transactional
    public void updateNonExistingDirdenun() throws Exception {
        int databaseSizeBeforeUpdate = dirdenunRepository.findAll().size();

        // Create the Dirdenun

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDirdenunMockMvc.perform(put("/api/dirdenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dirdenun)))
            .andExpect(status().isCreated());

        // Validate the Dirdenun in the database
        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDirdenun() throws Exception {
        // Initialize the database
        dirdenunRepository.saveAndFlush(dirdenun);
        dirdenunSearchRepository.save(dirdenun);
        int databaseSizeBeforeDelete = dirdenunRepository.findAll().size();

        // Get the dirdenun
        restDirdenunMockMvc.perform(delete("/api/dirdenuns/{id}", dirdenun.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dirdenunExistsInEs = dirdenunSearchRepository.exists(dirdenun.getId());
        assertThat(dirdenunExistsInEs).isFalse();

        // Validate the database is empty
        List<Dirdenun> dirdenunList = dirdenunRepository.findAll();
        assertThat(dirdenunList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDirdenun() throws Exception {
        // Initialize the database
        dirdenunRepository.saveAndFlush(dirdenun);
        dirdenunSearchRepository.save(dirdenun);

        // Search the dirdenun
        restDirdenunMockMvc.perform(get("/api/_search/dirdenuns?query=id:" + dirdenun.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dirdenun.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCoddepart").value(hasItem(DEFAULT_V_CODDEPART.toString())))
            .andExpect(jsonPath("$.[*].vCodprovin").value(hasItem(DEFAULT_V_CODPROVIN.toString())))
            .andExpect(jsonPath("$.[*].vCoddistri").value(hasItem(DEFAULT_V_CODDISTRI.toString())))
            .andExpect(jsonPath("$.[*].nCodtzona").value(hasItem(DEFAULT_N_CODTZONA)))
            .andExpect(jsonPath("$.[*].vDeszona").value(hasItem(DEFAULT_V_DESZONA.toString())))
            .andExpect(jsonPath("$.[*].nCodtipvia").value(hasItem(DEFAULT_N_CODTIPVIA)))
            .andExpect(jsonPath("$.[*].vDesvia").value(hasItem(DEFAULT_V_DESVIA.toString())))
            .andExpect(jsonPath("$.[*].vDireccion").value(hasItem(DEFAULT_V_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].vDircomple").value(hasItem(DEFAULT_V_DIRCOMPLE.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dirdenun.class);
        Dirdenun dirdenun1 = new Dirdenun();
        dirdenun1.setId(1L);
        Dirdenun dirdenun2 = new Dirdenun();
        dirdenun2.setId(dirdenun1.getId());
        assertThat(dirdenun1).isEqualTo(dirdenun2);
        dirdenun2.setId(2L);
        assertThat(dirdenun1).isNotEqualTo(dirdenun2);
        dirdenun1.setId(null);
        assertThat(dirdenun1).isNotEqualTo(dirdenun2);
    }
}
