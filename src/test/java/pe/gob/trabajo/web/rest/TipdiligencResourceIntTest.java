package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Tipdiligenc;
import pe.gob.trabajo.repository.TipdiligencRepository;
import pe.gob.trabajo.repository.search.TipdiligencSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipdiligencResource REST controller.
 *
 * @see TipdiligencResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class TipdiligencResourceIntTest {

    private static final String DEFAULT_V_CODSIS = "AAA";
    private static final String UPDATED_V_CODSIS = "BBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipdiligencRepository tipdiligencRepository;

    @Autowired
    private TipdiligencSearchRepository tipdiligencSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipdiligencMockMvc;

    private Tipdiligenc tipdiligenc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipdiligencResource tipdiligencResource = new TipdiligencResource(tipdiligencRepository, tipdiligencSearchRepository);
        this.restTipdiligencMockMvc = MockMvcBuilders.standaloneSetup(tipdiligencResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipdiligenc createEntity(EntityManager em) {
        Tipdiligenc tipdiligenc = new Tipdiligenc()
            .vCodsis(DEFAULT_V_CODSIS)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipdiligenc;
    }

    @Before
    public void initTest() {
        tipdiligencSearchRepository.deleteAll();
        tipdiligenc = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipdiligenc() throws Exception {
        int databaseSizeBeforeCreate = tipdiligencRepository.findAll().size();

        // Create the Tipdiligenc
        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isCreated());

        // Validate the Tipdiligenc in the database
        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeCreate + 1);
        Tipdiligenc testTipdiligenc = tipdiligencList.get(tipdiligencList.size() - 1);
        assertThat(testTipdiligenc.getvCodsis()).isEqualTo(DEFAULT_V_CODSIS);
        assertThat(testTipdiligenc.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipdiligenc.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipdiligenc.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipdiligenc.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipdiligenc.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipdiligenc.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipdiligenc.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipdiligenc in Elasticsearch
        Tipdiligenc tipdiligencEs = tipdiligencSearchRepository.findOne(testTipdiligenc.getId());
        assertThat(tipdiligencEs).isEqualToComparingFieldByField(testTipdiligenc);
    }

    @Test
    @Transactional
    public void createTipdiligencWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipdiligencRepository.findAll().size();

        // Create the Tipdiligenc with an existing ID
        tipdiligenc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isBadRequest());

        // Validate the Tipdiligenc in the database
        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodsisIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdiligencRepository.findAll().size();
        // set the field null
        tipdiligenc.setvCodsis(null);

        // Create the Tipdiligenc, which fails.

        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isBadRequest());

        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdiligencRepository.findAll().size();
        // set the field null
        tipdiligenc.setnUsuareg(null);

        // Create the Tipdiligenc, which fails.

        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isBadRequest());

        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdiligencRepository.findAll().size();
        // set the field null
        tipdiligenc.settFecreg(null);

        // Create the Tipdiligenc, which fails.

        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isBadRequest());

        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdiligencRepository.findAll().size();
        // set the field null
        tipdiligenc.setnFlgactivo(null);

        // Create the Tipdiligenc, which fails.

        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isBadRequest());

        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdiligencRepository.findAll().size();
        // set the field null
        tipdiligenc.setnSedereg(null);

        // Create the Tipdiligenc, which fails.

        restTipdiligencMockMvc.perform(post("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isBadRequest());

        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipdiligencs() throws Exception {
        // Initialize the database
        tipdiligencRepository.saveAndFlush(tipdiligenc);

        // Get all the tipdiligencList
        restTipdiligencMockMvc.perform(get("/api/tipdiligencs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipdiligenc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipdiligenc() throws Exception {
        // Initialize the database
        tipdiligencRepository.saveAndFlush(tipdiligenc);

        // Get the tipdiligenc
        restTipdiligencMockMvc.perform(get("/api/tipdiligencs/{id}", tipdiligenc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipdiligenc.getId().intValue()))
            .andExpect(jsonPath("$.vCodsis").value(DEFAULT_V_CODSIS.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipdiligenc() throws Exception {
        // Get the tipdiligenc
        restTipdiligencMockMvc.perform(get("/api/tipdiligencs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipdiligenc() throws Exception {
        // Initialize the database
        tipdiligencRepository.saveAndFlush(tipdiligenc);
        tipdiligencSearchRepository.save(tipdiligenc);
        int databaseSizeBeforeUpdate = tipdiligencRepository.findAll().size();

        // Update the tipdiligenc
        Tipdiligenc updatedTipdiligenc = tipdiligencRepository.findOne(tipdiligenc.getId());
        updatedTipdiligenc
            .vCodsis(UPDATED_V_CODSIS)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipdiligencMockMvc.perform(put("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipdiligenc)))
            .andExpect(status().isOk());

        // Validate the Tipdiligenc in the database
        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeUpdate);
        Tipdiligenc testTipdiligenc = tipdiligencList.get(tipdiligencList.size() - 1);
        assertThat(testTipdiligenc.getvCodsis()).isEqualTo(UPDATED_V_CODSIS);
        assertThat(testTipdiligenc.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipdiligenc.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipdiligenc.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipdiligenc.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipdiligenc.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipdiligenc.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipdiligenc.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipdiligenc in Elasticsearch
        Tipdiligenc tipdiligencEs = tipdiligencSearchRepository.findOne(testTipdiligenc.getId());
        assertThat(tipdiligencEs).isEqualToComparingFieldByField(testTipdiligenc);
    }

    @Test
    @Transactional
    public void updateNonExistingTipdiligenc() throws Exception {
        int databaseSizeBeforeUpdate = tipdiligencRepository.findAll().size();

        // Create the Tipdiligenc

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipdiligencMockMvc.perform(put("/api/tipdiligencs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdiligenc)))
            .andExpect(status().isCreated());

        // Validate the Tipdiligenc in the database
        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipdiligenc() throws Exception {
        // Initialize the database
        tipdiligencRepository.saveAndFlush(tipdiligenc);
        tipdiligencSearchRepository.save(tipdiligenc);
        int databaseSizeBeforeDelete = tipdiligencRepository.findAll().size();

        // Get the tipdiligenc
        restTipdiligencMockMvc.perform(delete("/api/tipdiligencs/{id}", tipdiligenc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipdiligencExistsInEs = tipdiligencSearchRepository.exists(tipdiligenc.getId());
        assertThat(tipdiligencExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipdiligenc> tipdiligencList = tipdiligencRepository.findAll();
        assertThat(tipdiligencList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipdiligenc() throws Exception {
        // Initialize the database
        tipdiligencRepository.saveAndFlush(tipdiligenc);
        tipdiligencSearchRepository.save(tipdiligenc);

        // Search the tipdiligenc
        restTipdiligencMockMvc.perform(get("/api/_search/tipdiligencs?query=id:" + tipdiligenc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipdiligenc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipdiligenc.class);
        Tipdiligenc tipdiligenc1 = new Tipdiligenc();
        tipdiligenc1.setId(1L);
        Tipdiligenc tipdiligenc2 = new Tipdiligenc();
        tipdiligenc2.setId(tipdiligenc1.getId());
        assertThat(tipdiligenc1).isEqualTo(tipdiligenc2);
        tipdiligenc2.setId(2L);
        assertThat(tipdiligenc1).isNotEqualTo(tipdiligenc2);
        tipdiligenc1.setId(null);
        assertThat(tipdiligenc1).isNotEqualTo(tipdiligenc2);
    }
}
