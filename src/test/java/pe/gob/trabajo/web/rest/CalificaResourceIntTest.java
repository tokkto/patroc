package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Califica;
import pe.gob.trabajo.repository.CalificaRepository;
import pe.gob.trabajo.repository.search.CalificaSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CalificaResource REST controller.
 *
 * @see CalificaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class CalificaResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private CalificaRepository calificaRepository;

    @Autowired
    private CalificaSearchRepository calificaSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCalificaMockMvc;

    private Califica califica;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CalificaResource calificaResource = new CalificaResource(calificaRepository, calificaSearchRepository);
        this.restCalificaMockMvc = MockMvcBuilders.standaloneSetup(calificaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Califica createEntity(EntityManager em) {
        Califica califica = new Califica()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return califica;
    }

    @Before
    public void initTest() {
        calificaSearchRepository.deleteAll();
        califica = createEntity(em);
    }

    @Test
    @Transactional
    public void createCalifica() throws Exception {
        int databaseSizeBeforeCreate = calificaRepository.findAll().size();

        // Create the Califica
        restCalificaMockMvc.perform(post("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isCreated());

        // Validate the Califica in the database
        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeCreate + 1);
        Califica testCalifica = calificaList.get(calificaList.size() - 1);
        assertThat(testCalifica.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testCalifica.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testCalifica.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testCalifica.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testCalifica.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testCalifica.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testCalifica.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testCalifica.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Califica in Elasticsearch
        Califica calificaEs = calificaSearchRepository.findOne(testCalifica.getId());
        assertThat(calificaEs).isEqualToComparingFieldByField(testCalifica);
    }

    @Test
    @Transactional
    public void createCalificaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = calificaRepository.findAll().size();

        // Create the Califica with an existing ID
        califica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCalificaMockMvc.perform(post("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isBadRequest());

        // Validate the Califica in the database
        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = calificaRepository.findAll().size();
        // set the field null
        califica.setvDescrip(null);

        // Create the Califica, which fails.

        restCalificaMockMvc.perform(post("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isBadRequest());

        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = calificaRepository.findAll().size();
        // set the field null
        califica.setvUsuareg(null);

        // Create the Califica, which fails.

        restCalificaMockMvc.perform(post("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isBadRequest());

        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = calificaRepository.findAll().size();
        // set the field null
        califica.settFecreg(null);

        // Create the Califica, which fails.

        restCalificaMockMvc.perform(post("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isBadRequest());

        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = calificaRepository.findAll().size();
        // set the field null
        califica.setnSedereg(null);

        // Create the Califica, which fails.

        restCalificaMockMvc.perform(post("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isBadRequest());

        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCalificas() throws Exception {
        // Initialize the database
        calificaRepository.saveAndFlush(califica);

        // Get all the calificaList
        restCalificaMockMvc.perform(get("/api/calificas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(califica.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getCalifica() throws Exception {
        // Initialize the database
        calificaRepository.saveAndFlush(califica);

        // Get the califica
        restCalificaMockMvc.perform(get("/api/calificas/{id}", califica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(califica.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingCalifica() throws Exception {
        // Get the califica
        restCalificaMockMvc.perform(get("/api/calificas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCalifica() throws Exception {
        // Initialize the database
        calificaRepository.saveAndFlush(califica);
        calificaSearchRepository.save(califica);
        int databaseSizeBeforeUpdate = calificaRepository.findAll().size();

        // Update the califica
        Califica updatedCalifica = calificaRepository.findOne(califica.getId());
        updatedCalifica
            .vDescrip(UPDATED_V_DESCRIP)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restCalificaMockMvc.perform(put("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCalifica)))
            .andExpect(status().isOk());

        // Validate the Califica in the database
        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeUpdate);
        Califica testCalifica = calificaList.get(calificaList.size() - 1);
        assertThat(testCalifica.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testCalifica.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testCalifica.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testCalifica.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testCalifica.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testCalifica.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testCalifica.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testCalifica.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Califica in Elasticsearch
        Califica calificaEs = calificaSearchRepository.findOne(testCalifica.getId());
        assertThat(calificaEs).isEqualToComparingFieldByField(testCalifica);
    }

    @Test
    @Transactional
    public void updateNonExistingCalifica() throws Exception {
        int databaseSizeBeforeUpdate = calificaRepository.findAll().size();

        // Create the Califica

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCalificaMockMvc.perform(put("/api/calificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(califica)))
            .andExpect(status().isCreated());

        // Validate the Califica in the database
        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCalifica() throws Exception {
        // Initialize the database
        calificaRepository.saveAndFlush(califica);
        calificaSearchRepository.save(califica);
        int databaseSizeBeforeDelete = calificaRepository.findAll().size();

        // Get the califica
        restCalificaMockMvc.perform(delete("/api/calificas/{id}", califica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean calificaExistsInEs = calificaSearchRepository.exists(califica.getId());
        assertThat(calificaExistsInEs).isFalse();

        // Validate the database is empty
        List<Califica> calificaList = calificaRepository.findAll();
        assertThat(calificaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCalifica() throws Exception {
        // Initialize the database
        calificaRepository.saveAndFlush(califica);
        calificaSearchRepository.save(califica);

        // Search the califica
        restCalificaMockMvc.perform(get("/api/_search/calificas?query=id:" + califica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(califica.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Califica.class);
        Califica califica1 = new Califica();
        califica1.setId(1L);
        Califica califica2 = new Califica();
        califica2.setId(califica1.getId());
        assertThat(califica1).isEqualTo(califica2);
        califica2.setId(2L);
        assertThat(califica1).isNotEqualTo(califica2);
        califica1.setId(null);
        assertThat(califica1).isNotEqualTo(califica2);
    }
}
