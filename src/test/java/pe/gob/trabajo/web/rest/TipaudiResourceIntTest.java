package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Tipaudi;
import pe.gob.trabajo.repository.TipaudiRepository;
import pe.gob.trabajo.repository.search.TipaudiSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipaudiResource REST controller.
 *
 * @see TipaudiResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class TipaudiResourceIntTest {

    private static final String DEFAULT_V_CODSIS = "AAA";
    private static final String UPDATED_V_CODSIS = "BBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipaudiRepository tipaudiRepository;

    @Autowired
    private TipaudiSearchRepository tipaudiSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipaudiMockMvc;

    private Tipaudi tipaudi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipaudiResource tipaudiResource = new TipaudiResource(tipaudiRepository, tipaudiSearchRepository);
        this.restTipaudiMockMvc = MockMvcBuilders.standaloneSetup(tipaudiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipaudi createEntity(EntityManager em) {
        Tipaudi tipaudi = new Tipaudi()
            .vCodsis(DEFAULT_V_CODSIS)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipaudi;
    }

    @Before
    public void initTest() {
        tipaudiSearchRepository.deleteAll();
        tipaudi = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipaudi() throws Exception {
        int databaseSizeBeforeCreate = tipaudiRepository.findAll().size();

        // Create the Tipaudi
        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isCreated());

        // Validate the Tipaudi in the database
        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeCreate + 1);
        Tipaudi testTipaudi = tipaudiList.get(tipaudiList.size() - 1);
        assertThat(testTipaudi.getvCodsis()).isEqualTo(DEFAULT_V_CODSIS);
        assertThat(testTipaudi.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipaudi.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipaudi.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipaudi.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipaudi.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipaudi.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipaudi.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipaudi in Elasticsearch
        Tipaudi tipaudiEs = tipaudiSearchRepository.findOne(testTipaudi.getId());
        assertThat(tipaudiEs).isEqualToComparingFieldByField(testTipaudi);
    }

    @Test
    @Transactional
    public void createTipaudiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipaudiRepository.findAll().size();

        // Create the Tipaudi with an existing ID
        tipaudi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isBadRequest());

        // Validate the Tipaudi in the database
        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodsisIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipaudiRepository.findAll().size();
        // set the field null
        tipaudi.setvCodsis(null);

        // Create the Tipaudi, which fails.

        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isBadRequest());

        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipaudiRepository.findAll().size();
        // set the field null
        tipaudi.setnUsuareg(null);

        // Create the Tipaudi, which fails.

        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isBadRequest());

        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipaudiRepository.findAll().size();
        // set the field null
        tipaudi.settFecreg(null);

        // Create the Tipaudi, which fails.

        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isBadRequest());

        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipaudiRepository.findAll().size();
        // set the field null
        tipaudi.setnFlgactivo(null);

        // Create the Tipaudi, which fails.

        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isBadRequest());

        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipaudiRepository.findAll().size();
        // set the field null
        tipaudi.setnSedereg(null);

        // Create the Tipaudi, which fails.

        restTipaudiMockMvc.perform(post("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isBadRequest());

        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipaudis() throws Exception {
        // Initialize the database
        tipaudiRepository.saveAndFlush(tipaudi);

        // Get all the tipaudiList
        restTipaudiMockMvc.perform(get("/api/tipaudis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipaudi.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipaudi() throws Exception {
        // Initialize the database
        tipaudiRepository.saveAndFlush(tipaudi);

        // Get the tipaudi
        restTipaudiMockMvc.perform(get("/api/tipaudis/{id}", tipaudi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipaudi.getId().intValue()))
            .andExpect(jsonPath("$.vCodsis").value(DEFAULT_V_CODSIS.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipaudi() throws Exception {
        // Get the tipaudi
        restTipaudiMockMvc.perform(get("/api/tipaudis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipaudi() throws Exception {
        // Initialize the database
        tipaudiRepository.saveAndFlush(tipaudi);
        tipaudiSearchRepository.save(tipaudi);
        int databaseSizeBeforeUpdate = tipaudiRepository.findAll().size();

        // Update the tipaudi
        Tipaudi updatedTipaudi = tipaudiRepository.findOne(tipaudi.getId());
        updatedTipaudi
            .vCodsis(UPDATED_V_CODSIS)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipaudiMockMvc.perform(put("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipaudi)))
            .andExpect(status().isOk());

        // Validate the Tipaudi in the database
        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeUpdate);
        Tipaudi testTipaudi = tipaudiList.get(tipaudiList.size() - 1);
        assertThat(testTipaudi.getvCodsis()).isEqualTo(UPDATED_V_CODSIS);
        assertThat(testTipaudi.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipaudi.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipaudi.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipaudi.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipaudi.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipaudi.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipaudi.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipaudi in Elasticsearch
        Tipaudi tipaudiEs = tipaudiSearchRepository.findOne(testTipaudi.getId());
        assertThat(tipaudiEs).isEqualToComparingFieldByField(testTipaudi);
    }

    @Test
    @Transactional
    public void updateNonExistingTipaudi() throws Exception {
        int databaseSizeBeforeUpdate = tipaudiRepository.findAll().size();

        // Create the Tipaudi

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipaudiMockMvc.perform(put("/api/tipaudis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipaudi)))
            .andExpect(status().isCreated());

        // Validate the Tipaudi in the database
        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipaudi() throws Exception {
        // Initialize the database
        tipaudiRepository.saveAndFlush(tipaudi);
        tipaudiSearchRepository.save(tipaudi);
        int databaseSizeBeforeDelete = tipaudiRepository.findAll().size();

        // Get the tipaudi
        restTipaudiMockMvc.perform(delete("/api/tipaudis/{id}", tipaudi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipaudiExistsInEs = tipaudiSearchRepository.exists(tipaudi.getId());
        assertThat(tipaudiExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipaudi> tipaudiList = tipaudiRepository.findAll();
        assertThat(tipaudiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipaudi() throws Exception {
        // Initialize the database
        tipaudiRepository.saveAndFlush(tipaudi);
        tipaudiSearchRepository.save(tipaudi);

        // Search the tipaudi
        restTipaudiMockMvc.perform(get("/api/_search/tipaudis?query=id:" + tipaudi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipaudi.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipaudi.class);
        Tipaudi tipaudi1 = new Tipaudi();
        tipaudi1.setId(1L);
        Tipaudi tipaudi2 = new Tipaudi();
        tipaudi2.setId(tipaudi1.getId());
        assertThat(tipaudi1).isEqualTo(tipaudi2);
        tipaudi2.setId(2L);
        assertThat(tipaudi1).isNotEqualTo(tipaudi2);
        tipaudi1.setId(null);
        assertThat(tipaudi1).isNotEqualTo(tipaudi2);
    }
}
