package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Denunte;
import pe.gob.trabajo.repository.DenunteRepository;
import pe.gob.trabajo.repository.search.DenunteSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DenunteResource REST controller.
 *
 * @see DenunteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class DenunteResourceIntTest {

    private static final Integer DEFAULT_N_CODUSU = 1;
    private static final Integer UPDATED_N_CODUSU = 2;

    private static final String DEFAULT_V_FLGESTADO = "A";
    private static final String UPDATED_V_FLGESTADO = "B";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DenunteRepository denunteRepository;

    @Autowired
    private DenunteSearchRepository denunteSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDenunteMockMvc;

    private Denunte denunte;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DenunteResource denunteResource = new DenunteResource(denunteRepository, denunteSearchRepository);
        this.restDenunteMockMvc = MockMvcBuilders.standaloneSetup(denunteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Denunte createEntity(EntityManager em) {
        Denunte denunte = new Denunte()
            .nCodusu(DEFAULT_N_CODUSU)
            .vFlgestado(DEFAULT_V_FLGESTADO)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return denunte;
    }

    @Before
    public void initTest() {
        denunteSearchRepository.deleteAll();
        denunte = createEntity(em);
    }

    @Test
    @Transactional
    public void createDenunte() throws Exception {
        int databaseSizeBeforeCreate = denunteRepository.findAll().size();

        // Create the Denunte
        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isCreated());

        // Validate the Denunte in the database
        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeCreate + 1);
        Denunte testDenunte = denunteList.get(denunteList.size() - 1);
        assertThat(testDenunte.getnCodusu()).isEqualTo(DEFAULT_N_CODUSU);
        assertThat(testDenunte.getvFlgestado()).isEqualTo(DEFAULT_V_FLGESTADO);
        assertThat(testDenunte.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testDenunte.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDenunte.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDenunte.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDenunte.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testDenunte.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDenunte.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Denunte in Elasticsearch
        Denunte denunteEs = denunteSearchRepository.findOne(testDenunte.getId());
        assertThat(denunteEs).isEqualToComparingFieldByField(testDenunte);
    }

    @Test
    @Transactional
    public void createDenunteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = denunteRepository.findAll().size();

        // Create the Denunte with an existing ID
        denunte.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isBadRequest());

        // Validate the Denunte in the database
        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknCodusuIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunteRepository.findAll().size();
        // set the field null
        denunte.setnCodusu(null);

        // Create the Denunte, which fails.

        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isBadRequest());

        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgestadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunteRepository.findAll().size();
        // set the field null
        denunte.setvFlgestado(null);

        // Create the Denunte, which fails.

        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isBadRequest());

        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunteRepository.findAll().size();
        // set the field null
        denunte.setvUsuareg(null);

        // Create the Denunte, which fails.

        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isBadRequest());

        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunteRepository.findAll().size();
        // set the field null
        denunte.settFecreg(null);

        // Create the Denunte, which fails.

        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isBadRequest());

        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunteRepository.findAll().size();
        // set the field null
        denunte.setnSedereg(null);

        // Create the Denunte, which fails.

        restDenunteMockMvc.perform(post("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isBadRequest());

        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDenuntes() throws Exception {
        // Initialize the database
        denunteRepository.saveAndFlush(denunte);

        // Get all the denunteList
        restDenunteMockMvc.perform(get("/api/denuntes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(denunte.getId().intValue())))
            .andExpect(jsonPath("$.[*].nCodusu").value(hasItem(DEFAULT_N_CODUSU)))
            .andExpect(jsonPath("$.[*].vFlgestado").value(hasItem(DEFAULT_V_FLGESTADO.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDenunte() throws Exception {
        // Initialize the database
        denunteRepository.saveAndFlush(denunte);

        // Get the denunte
        restDenunteMockMvc.perform(get("/api/denuntes/{id}", denunte.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(denunte.getId().intValue()))
            .andExpect(jsonPath("$.nCodusu").value(DEFAULT_N_CODUSU))
            .andExpect(jsonPath("$.vFlgestado").value(DEFAULT_V_FLGESTADO.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDenunte() throws Exception {
        // Get the denunte
        restDenunteMockMvc.perform(get("/api/denuntes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDenunte() throws Exception {
        // Initialize the database
        denunteRepository.saveAndFlush(denunte);
        denunteSearchRepository.save(denunte);
        int databaseSizeBeforeUpdate = denunteRepository.findAll().size();

        // Update the denunte
        Denunte updatedDenunte = denunteRepository.findOne(denunte.getId());
        updatedDenunte
            .nCodusu(UPDATED_N_CODUSU)
            .vFlgestado(UPDATED_V_FLGESTADO)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDenunteMockMvc.perform(put("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDenunte)))
            .andExpect(status().isOk());

        // Validate the Denunte in the database
        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeUpdate);
        Denunte testDenunte = denunteList.get(denunteList.size() - 1);
        assertThat(testDenunte.getnCodusu()).isEqualTo(UPDATED_N_CODUSU);
        assertThat(testDenunte.getvFlgestado()).isEqualTo(UPDATED_V_FLGESTADO);
        assertThat(testDenunte.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testDenunte.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDenunte.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDenunte.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDenunte.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testDenunte.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDenunte.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Denunte in Elasticsearch
        Denunte denunteEs = denunteSearchRepository.findOne(testDenunte.getId());
        assertThat(denunteEs).isEqualToComparingFieldByField(testDenunte);
    }

    @Test
    @Transactional
    public void updateNonExistingDenunte() throws Exception {
        int databaseSizeBeforeUpdate = denunteRepository.findAll().size();

        // Create the Denunte

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDenunteMockMvc.perform(put("/api/denuntes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denunte)))
            .andExpect(status().isCreated());

        // Validate the Denunte in the database
        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDenunte() throws Exception {
        // Initialize the database
        denunteRepository.saveAndFlush(denunte);
        denunteSearchRepository.save(denunte);
        int databaseSizeBeforeDelete = denunteRepository.findAll().size();

        // Get the denunte
        restDenunteMockMvc.perform(delete("/api/denuntes/{id}", denunte.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean denunteExistsInEs = denunteSearchRepository.exists(denunte.getId());
        assertThat(denunteExistsInEs).isFalse();

        // Validate the database is empty
        List<Denunte> denunteList = denunteRepository.findAll();
        assertThat(denunteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDenunte() throws Exception {
        // Initialize the database
        denunteRepository.saveAndFlush(denunte);
        denunteSearchRepository.save(denunte);

        // Search the denunte
        restDenunteMockMvc.perform(get("/api/_search/denuntes?query=id:" + denunte.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(denunte.getId().intValue())))
            .andExpect(jsonPath("$.[*].nCodusu").value(hasItem(DEFAULT_N_CODUSU)))
            .andExpect(jsonPath("$.[*].vFlgestado").value(hasItem(DEFAULT_V_FLGESTADO.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Denunte.class);
        Denunte denunte1 = new Denunte();
        denunte1.setId(1L);
        Denunte denunte2 = new Denunte();
        denunte2.setId(denunte1.getId());
        assertThat(denunte1).isEqualTo(denunte2);
        denunte2.setId(2L);
        assertThat(denunte1).isNotEqualTo(denunte2);
        denunte1.setId(null);
        assertThat(denunte1).isNotEqualTo(denunte2);
    }
}
