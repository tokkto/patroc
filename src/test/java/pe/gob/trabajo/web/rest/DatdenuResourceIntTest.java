package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Datdenu;
import pe.gob.trabajo.repository.DatdenuRepository;
import pe.gob.trabajo.repository.search.DatdenuSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DatdenuResource REST controller.
 *
 * @see DatdenuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class DatdenuResourceIntTest {

    private static final Integer DEFAULT_N_NUMTRBAFE = 1;
    private static final Integer UPDATED_N_NUMTRBAFE = 2;

    private static final String DEFAULT_V_HORAINI = "AAAAAAAAAA";
    private static final String UPDATED_V_HORAINI = "BBBBBBBBBB";

    private static final String DEFAULT_V_HORAFIN = "AAAAAAAAAA";
    private static final String UPDATED_V_HORAFIN = "BBBBBBBBBB";

    private static final String DEFAULT_V_DESMOTIVO = "AAAAAAAAAA";
    private static final String UPDATED_V_DESMOTIVO = "BBBBBBBBBB";

    private static final String DEFAULT_V_DESOTRO = "AAAAAAAAAA";
    private static final String UPDATED_V_DESOTRO = "BBBBBBBBBB";

    private static final byte[] DEFAULT_B_ARCHIVO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_B_ARCHIVO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_B_ARCHIVO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_B_ARCHIVO_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_V_ARCHTIPO = "AAAAAAAAAA";
    private static final String UPDATED_V_ARCHTIPO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_V_FLGGREIDE = false;
    private static final Boolean UPDATED_V_FLGGREIDE = true;

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DatdenuRepository datdenuRepository;

    @Autowired
    private DatdenuSearchRepository datdenuSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDatdenuMockMvc;

    private Datdenu datdenu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DatdenuResource datdenuResource = new DatdenuResource(datdenuRepository, datdenuSearchRepository);
        this.restDatdenuMockMvc = MockMvcBuilders.standaloneSetup(datdenuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Datdenu createEntity(EntityManager em) {
        Datdenu datdenu = new Datdenu()
            .nNumtrbafe(DEFAULT_N_NUMTRBAFE)
            .vHoraini(DEFAULT_V_HORAINI)
            .vHorafin(DEFAULT_V_HORAFIN)
            .vDesmotivo(DEFAULT_V_DESMOTIVO)
            .vDesotro(DEFAULT_V_DESOTRO)
            .bArchivo(DEFAULT_B_ARCHIVO)
            .bArchivoContentType(DEFAULT_B_ARCHIVO_CONTENT_TYPE)
            .vArchtipo(DEFAULT_V_ARCHTIPO)
            .vFlggreide(DEFAULT_V_FLGGREIDE)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return datdenu;
    }

    @Before
    public void initTest() {
        datdenuSearchRepository.deleteAll();
        datdenu = createEntity(em);
    }

    @Test
    @Transactional
    public void createDatdenu() throws Exception {
        int databaseSizeBeforeCreate = datdenuRepository.findAll().size();

        // Create the Datdenu
        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isCreated());

        // Validate the Datdenu in the database
        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeCreate + 1);
        Datdenu testDatdenu = datdenuList.get(datdenuList.size() - 1);
        assertThat(testDatdenu.getnNumtrbafe()).isEqualTo(DEFAULT_N_NUMTRBAFE);
        assertThat(testDatdenu.getvHoraini()).isEqualTo(DEFAULT_V_HORAINI);
        assertThat(testDatdenu.getvHorafin()).isEqualTo(DEFAULT_V_HORAFIN);
        assertThat(testDatdenu.getvDesmotivo()).isEqualTo(DEFAULT_V_DESMOTIVO);
        assertThat(testDatdenu.getvDesotro()).isEqualTo(DEFAULT_V_DESOTRO);
        assertThat(testDatdenu.getbArchivo()).isEqualTo(DEFAULT_B_ARCHIVO);
        assertThat(testDatdenu.getbArchivoContentType()).isEqualTo(DEFAULT_B_ARCHIVO_CONTENT_TYPE);
        assertThat(testDatdenu.getvArchtipo()).isEqualTo(DEFAULT_V_ARCHTIPO);
        assertThat(testDatdenu.isvFlggreide()).isEqualTo(DEFAULT_V_FLGGREIDE);
        assertThat(testDatdenu.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testDatdenu.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDatdenu.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDatdenu.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDatdenu.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testDatdenu.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDatdenu.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Datdenu in Elasticsearch
        Datdenu datdenuEs = datdenuSearchRepository.findOne(testDatdenu.getId());
        assertThat(datdenuEs).isEqualToComparingFieldByField(testDatdenu);
    }

    @Test
    @Transactional
    public void createDatdenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = datdenuRepository.findAll().size();

        // Create the Datdenu with an existing ID
        datdenu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        // Validate the Datdenu in the database
        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvHorainiIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setvHoraini(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHorafinIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setvHorafin(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesmotivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setvDesmotivo(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesotroIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setvDesotro(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvArchtipoIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setvArchtipo(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setvUsuareg(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.settFecreg(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = datdenuRepository.findAll().size();
        // set the field null
        datdenu.setnSedereg(null);

        // Create the Datdenu, which fails.

        restDatdenuMockMvc.perform(post("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isBadRequest());

        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDatdenus() throws Exception {
        // Initialize the database
        datdenuRepository.saveAndFlush(datdenu);

        // Get all the datdenuList
        restDatdenuMockMvc.perform(get("/api/datdenus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(datdenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].nNumtrbafe").value(hasItem(DEFAULT_N_NUMTRBAFE)))
            .andExpect(jsonPath("$.[*].vHoraini").value(hasItem(DEFAULT_V_HORAINI.toString())))
            .andExpect(jsonPath("$.[*].vHorafin").value(hasItem(DEFAULT_V_HORAFIN.toString())))
            .andExpect(jsonPath("$.[*].vDesmotivo").value(hasItem(DEFAULT_V_DESMOTIVO.toString())))
            .andExpect(jsonPath("$.[*].vDesotro").value(hasItem(DEFAULT_V_DESOTRO.toString())))
            .andExpect(jsonPath("$.[*].bArchivoContentType").value(hasItem(DEFAULT_B_ARCHIVO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].bArchivo").value(hasItem(Base64Utils.encodeToString(DEFAULT_B_ARCHIVO))))
            .andExpect(jsonPath("$.[*].vArchtipo").value(hasItem(DEFAULT_V_ARCHTIPO.toString())))
            .andExpect(jsonPath("$.[*].vFlggreide").value(hasItem(DEFAULT_V_FLGGREIDE.booleanValue())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDatdenu() throws Exception {
        // Initialize the database
        datdenuRepository.saveAndFlush(datdenu);

        // Get the datdenu
        restDatdenuMockMvc.perform(get("/api/datdenus/{id}", datdenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(datdenu.getId().intValue()))
            .andExpect(jsonPath("$.nNumtrbafe").value(DEFAULT_N_NUMTRBAFE))
            .andExpect(jsonPath("$.vHoraini").value(DEFAULT_V_HORAINI.toString()))
            .andExpect(jsonPath("$.vHorafin").value(DEFAULT_V_HORAFIN.toString()))
            .andExpect(jsonPath("$.vDesmotivo").value(DEFAULT_V_DESMOTIVO.toString()))
            .andExpect(jsonPath("$.vDesotro").value(DEFAULT_V_DESOTRO.toString()))
            .andExpect(jsonPath("$.bArchivoContentType").value(DEFAULT_B_ARCHIVO_CONTENT_TYPE))
            .andExpect(jsonPath("$.bArchivo").value(Base64Utils.encodeToString(DEFAULT_B_ARCHIVO)))
            .andExpect(jsonPath("$.vArchtipo").value(DEFAULT_V_ARCHTIPO.toString()))
            .andExpect(jsonPath("$.vFlggreide").value(DEFAULT_V_FLGGREIDE.booleanValue()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDatdenu() throws Exception {
        // Get the datdenu
        restDatdenuMockMvc.perform(get("/api/datdenus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDatdenu() throws Exception {
        // Initialize the database
        datdenuRepository.saveAndFlush(datdenu);
        datdenuSearchRepository.save(datdenu);
        int databaseSizeBeforeUpdate = datdenuRepository.findAll().size();

        // Update the datdenu
        Datdenu updatedDatdenu = datdenuRepository.findOne(datdenu.getId());
        updatedDatdenu
            .nNumtrbafe(UPDATED_N_NUMTRBAFE)
            .vHoraini(UPDATED_V_HORAINI)
            .vHorafin(UPDATED_V_HORAFIN)
            .vDesmotivo(UPDATED_V_DESMOTIVO)
            .vDesotro(UPDATED_V_DESOTRO)
            .bArchivo(UPDATED_B_ARCHIVO)
            .bArchivoContentType(UPDATED_B_ARCHIVO_CONTENT_TYPE)
            .vArchtipo(UPDATED_V_ARCHTIPO)
            .vFlggreide(UPDATED_V_FLGGREIDE)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDatdenuMockMvc.perform(put("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDatdenu)))
            .andExpect(status().isOk());

        // Validate the Datdenu in the database
        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeUpdate);
        Datdenu testDatdenu = datdenuList.get(datdenuList.size() - 1);
        assertThat(testDatdenu.getnNumtrbafe()).isEqualTo(UPDATED_N_NUMTRBAFE);
        assertThat(testDatdenu.getvHoraini()).isEqualTo(UPDATED_V_HORAINI);
        assertThat(testDatdenu.getvHorafin()).isEqualTo(UPDATED_V_HORAFIN);
        assertThat(testDatdenu.getvDesmotivo()).isEqualTo(UPDATED_V_DESMOTIVO);
        assertThat(testDatdenu.getvDesotro()).isEqualTo(UPDATED_V_DESOTRO);
        assertThat(testDatdenu.getbArchivo()).isEqualTo(UPDATED_B_ARCHIVO);
        assertThat(testDatdenu.getbArchivoContentType()).isEqualTo(UPDATED_B_ARCHIVO_CONTENT_TYPE);
        assertThat(testDatdenu.getvArchtipo()).isEqualTo(UPDATED_V_ARCHTIPO);
        assertThat(testDatdenu.isvFlggreide()).isEqualTo(UPDATED_V_FLGGREIDE);
        assertThat(testDatdenu.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testDatdenu.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDatdenu.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDatdenu.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDatdenu.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testDatdenu.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDatdenu.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Datdenu in Elasticsearch
        Datdenu datdenuEs = datdenuSearchRepository.findOne(testDatdenu.getId());
        assertThat(datdenuEs).isEqualToComparingFieldByField(testDatdenu);
    }

    @Test
    @Transactional
    public void updateNonExistingDatdenu() throws Exception {
        int databaseSizeBeforeUpdate = datdenuRepository.findAll().size();

        // Create the Datdenu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDatdenuMockMvc.perform(put("/api/datdenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datdenu)))
            .andExpect(status().isCreated());

        // Validate the Datdenu in the database
        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDatdenu() throws Exception {
        // Initialize the database
        datdenuRepository.saveAndFlush(datdenu);
        datdenuSearchRepository.save(datdenu);
        int databaseSizeBeforeDelete = datdenuRepository.findAll().size();

        // Get the datdenu
        restDatdenuMockMvc.perform(delete("/api/datdenus/{id}", datdenu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean datdenuExistsInEs = datdenuSearchRepository.exists(datdenu.getId());
        assertThat(datdenuExistsInEs).isFalse();

        // Validate the database is empty
        List<Datdenu> datdenuList = datdenuRepository.findAll();
        assertThat(datdenuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDatdenu() throws Exception {
        // Initialize the database
        datdenuRepository.saveAndFlush(datdenu);
        datdenuSearchRepository.save(datdenu);

        // Search the datdenu
        restDatdenuMockMvc.perform(get("/api/_search/datdenus?query=id:" + datdenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(datdenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].nNumtrbafe").value(hasItem(DEFAULT_N_NUMTRBAFE)))
            .andExpect(jsonPath("$.[*].vHoraini").value(hasItem(DEFAULT_V_HORAINI.toString())))
            .andExpect(jsonPath("$.[*].vHorafin").value(hasItem(DEFAULT_V_HORAFIN.toString())))
            .andExpect(jsonPath("$.[*].vDesmotivo").value(hasItem(DEFAULT_V_DESMOTIVO.toString())))
            .andExpect(jsonPath("$.[*].vDesotro").value(hasItem(DEFAULT_V_DESOTRO.toString())))
            .andExpect(jsonPath("$.[*].bArchivoContentType").value(hasItem(DEFAULT_B_ARCHIVO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].bArchivo").value(hasItem(Base64Utils.encodeToString(DEFAULT_B_ARCHIVO))))
            .andExpect(jsonPath("$.[*].vArchtipo").value(hasItem(DEFAULT_V_ARCHTIPO.toString())))
            .andExpect(jsonPath("$.[*].vFlggreide").value(hasItem(DEFAULT_V_FLGGREIDE.booleanValue())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Datdenu.class);
        Datdenu datdenu1 = new Datdenu();
        datdenu1.setId(1L);
        Datdenu datdenu2 = new Datdenu();
        datdenu2.setId(datdenu1.getId());
        assertThat(datdenu1).isEqualTo(datdenu2);
        datdenu2.setId(2L);
        assertThat(datdenu1).isNotEqualTo(datdenu2);
        datdenu1.setId(null);
        assertThat(datdenu1).isNotEqualTo(datdenu2);
    }
}
