package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Motidenun;
import pe.gob.trabajo.repository.MotidenunRepository;
import pe.gob.trabajo.repository.search.MotidenunSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MotidenunResource REST controller.
 *
 * @see MotidenunResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class MotidenunResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private MotidenunRepository motidenunRepository;

    @Autowired
    private MotidenunSearchRepository motidenunSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMotidenunMockMvc;

    private Motidenun motidenun;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotidenunResource motidenunResource = new MotidenunResource(motidenunRepository, motidenunSearchRepository);
        this.restMotidenunMockMvc = MockMvcBuilders.standaloneSetup(motidenunResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Motidenun createEntity(EntityManager em) {
        Motidenun motidenun = new Motidenun()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return motidenun;
    }

    @Before
    public void initTest() {
        motidenunSearchRepository.deleteAll();
        motidenun = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotidenun() throws Exception {
        int databaseSizeBeforeCreate = motidenunRepository.findAll().size();

        // Create the Motidenun
        restMotidenunMockMvc.perform(post("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isCreated());

        // Validate the Motidenun in the database
        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeCreate + 1);
        Motidenun testMotidenun = motidenunList.get(motidenunList.size() - 1);
        assertThat(testMotidenun.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testMotidenun.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testMotidenun.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testMotidenun.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testMotidenun.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testMotidenun.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testMotidenun.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testMotidenun.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Motidenun in Elasticsearch
        Motidenun motidenunEs = motidenunSearchRepository.findOne(testMotidenun.getId());
        assertThat(motidenunEs).isEqualToComparingFieldByField(testMotidenun);
    }

    @Test
    @Transactional
    public void createMotidenunWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motidenunRepository.findAll().size();

        // Create the Motidenun with an existing ID
        motidenun.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotidenunMockMvc.perform(post("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isBadRequest());

        // Validate the Motidenun in the database
        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = motidenunRepository.findAll().size();
        // set the field null
        motidenun.setvDescrip(null);

        // Create the Motidenun, which fails.

        restMotidenunMockMvc.perform(post("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isBadRequest());

        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = motidenunRepository.findAll().size();
        // set the field null
        motidenun.setvUsuareg(null);

        // Create the Motidenun, which fails.

        restMotidenunMockMvc.perform(post("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isBadRequest());

        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = motidenunRepository.findAll().size();
        // set the field null
        motidenun.settFecreg(null);

        // Create the Motidenun, which fails.

        restMotidenunMockMvc.perform(post("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isBadRequest());

        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = motidenunRepository.findAll().size();
        // set the field null
        motidenun.setnSedereg(null);

        // Create the Motidenun, which fails.

        restMotidenunMockMvc.perform(post("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isBadRequest());

        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMotidenuns() throws Exception {
        // Initialize the database
        motidenunRepository.saveAndFlush(motidenun);

        // Get all the motidenunList
        restMotidenunMockMvc.perform(get("/api/motidenuns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motidenun.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getMotidenun() throws Exception {
        // Initialize the database
        motidenunRepository.saveAndFlush(motidenun);

        // Get the motidenun
        restMotidenunMockMvc.perform(get("/api/motidenuns/{id}", motidenun.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motidenun.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingMotidenun() throws Exception {
        // Get the motidenun
        restMotidenunMockMvc.perform(get("/api/motidenuns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotidenun() throws Exception {
        // Initialize the database
        motidenunRepository.saveAndFlush(motidenun);
        motidenunSearchRepository.save(motidenun);
        int databaseSizeBeforeUpdate = motidenunRepository.findAll().size();

        // Update the motidenun
        Motidenun updatedMotidenun = motidenunRepository.findOne(motidenun.getId());
        updatedMotidenun
            .vDescrip(UPDATED_V_DESCRIP)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restMotidenunMockMvc.perform(put("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMotidenun)))
            .andExpect(status().isOk());

        // Validate the Motidenun in the database
        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeUpdate);
        Motidenun testMotidenun = motidenunList.get(motidenunList.size() - 1);
        assertThat(testMotidenun.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testMotidenun.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testMotidenun.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testMotidenun.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testMotidenun.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testMotidenun.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testMotidenun.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testMotidenun.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Motidenun in Elasticsearch
        Motidenun motidenunEs = motidenunSearchRepository.findOne(testMotidenun.getId());
        assertThat(motidenunEs).isEqualToComparingFieldByField(testMotidenun);
    }

    @Test
    @Transactional
    public void updateNonExistingMotidenun() throws Exception {
        int databaseSizeBeforeUpdate = motidenunRepository.findAll().size();

        // Create the Motidenun

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMotidenunMockMvc.perform(put("/api/motidenuns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motidenun)))
            .andExpect(status().isCreated());

        // Validate the Motidenun in the database
        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMotidenun() throws Exception {
        // Initialize the database
        motidenunRepository.saveAndFlush(motidenun);
        motidenunSearchRepository.save(motidenun);
        int databaseSizeBeforeDelete = motidenunRepository.findAll().size();

        // Get the motidenun
        restMotidenunMockMvc.perform(delete("/api/motidenuns/{id}", motidenun.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean motidenunExistsInEs = motidenunSearchRepository.exists(motidenun.getId());
        assertThat(motidenunExistsInEs).isFalse();

        // Validate the database is empty
        List<Motidenun> motidenunList = motidenunRepository.findAll();
        assertThat(motidenunList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMotidenun() throws Exception {
        // Initialize the database
        motidenunRepository.saveAndFlush(motidenun);
        motidenunSearchRepository.save(motidenun);

        // Search the motidenun
        restMotidenunMockMvc.perform(get("/api/_search/motidenuns?query=id:" + motidenun.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motidenun.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Motidenun.class);
        Motidenun motidenun1 = new Motidenun();
        motidenun1.setId(1L);
        Motidenun motidenun2 = new Motidenun();
        motidenun2.setId(motidenun1.getId());
        assertThat(motidenun1).isEqualTo(motidenun2);
        motidenun2.setId(2L);
        assertThat(motidenun1).isNotEqualTo(motidenun2);
        motidenun1.setId(null);
        assertThat(motidenun1).isNotEqualTo(motidenun2);
    }
}
