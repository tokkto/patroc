package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Atencionpj;
import pe.gob.trabajo.repository.AtencionpjRepository;
import pe.gob.trabajo.repository.search.AtencionpjSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AtencionpjResource REST controller.
 *
 * @see AtencionpjResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class AtencionpjResourceIntTest {

    private static final String DEFAULT_V_CODREG = "AA";
    private static final String UPDATED_V_CODREG = "BB";

    private static final String DEFAULT_V_CODZON = "AA";
    private static final String UPDATED_V_CODZON = "BB";

    private static final Integer DEFAULT_N_CORREL = 1;
    private static final Integer UPDATED_N_CORREL = 2;

    private static final LocalDate DEFAULT_D_FECATEN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECATEN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_OBSATEN = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSATEN = "BBBBBBBBBB";

    private static final String DEFAULT_V_FLGTEL = "A";
    private static final String UPDATED_V_FLGTEL = "B";

    private static final String DEFAULT_V_FLGADM = "A";
    private static final String UPDATED_V_FLGADM = "B";

    private static final String DEFAULT_V_FLGTELR = "A";
    private static final String UPDATED_V_FLGTELR = "B";

    private static final String DEFAULT_V_FLGATEN = "A";
    private static final String UPDATED_V_FLGATEN = "B";

    private static final String DEFAULT_V_CODLOC = "AAAAAA";
    private static final String UPDATED_V_CODLOC = "BBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private AtencionpjRepository atencionpjRepository;

    @Autowired
    private AtencionpjSearchRepository atencionpjSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAtencionpjMockMvc;

    private Atencionpj atencionpj;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AtencionpjResource atencionpjResource = new AtencionpjResource(atencionpjRepository, atencionpjSearchRepository);
        this.restAtencionpjMockMvc = MockMvcBuilders.standaloneSetup(atencionpjResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atencionpj createEntity(EntityManager em) {
        Atencionpj atencionpj = new Atencionpj()
            .vCodreg(DEFAULT_V_CODREG)
            .vCodzon(DEFAULT_V_CODZON)
            .nCorrel(DEFAULT_N_CORREL)
            .dFecaten(DEFAULT_D_FECATEN)
            .vObsaten(DEFAULT_V_OBSATEN)
            .vFlgtel(DEFAULT_V_FLGTEL)
            .vFlgadm(DEFAULT_V_FLGADM)
            .vFlgtelr(DEFAULT_V_FLGTELR)
            .vFlgaten(DEFAULT_V_FLGATEN)
            .vCodloc(DEFAULT_V_CODLOC)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return atencionpj;
    }

    @Before
    public void initTest() {
        atencionpjSearchRepository.deleteAll();
        atencionpj = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtencionpj() throws Exception {
        int databaseSizeBeforeCreate = atencionpjRepository.findAll().size();

        // Create the Atencionpj
        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isCreated());

        // Validate the Atencionpj in the database
        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeCreate + 1);
        Atencionpj testAtencionpj = atencionpjList.get(atencionpjList.size() - 1);
        assertThat(testAtencionpj.getvCodreg()).isEqualTo(DEFAULT_V_CODREG);
        assertThat(testAtencionpj.getvCodzon()).isEqualTo(DEFAULT_V_CODZON);
        assertThat(testAtencionpj.getnCorrel()).isEqualTo(DEFAULT_N_CORREL);
        assertThat(testAtencionpj.getdFecaten()).isEqualTo(DEFAULT_D_FECATEN);
        assertThat(testAtencionpj.getvObsaten()).isEqualTo(DEFAULT_V_OBSATEN);
        assertThat(testAtencionpj.getvFlgtel()).isEqualTo(DEFAULT_V_FLGTEL);
        assertThat(testAtencionpj.getvFlgadm()).isEqualTo(DEFAULT_V_FLGADM);
        assertThat(testAtencionpj.getvFlgtelr()).isEqualTo(DEFAULT_V_FLGTELR);
        assertThat(testAtencionpj.getvFlgaten()).isEqualTo(DEFAULT_V_FLGATEN);
        assertThat(testAtencionpj.getvCodloc()).isEqualTo(DEFAULT_V_CODLOC);
        assertThat(testAtencionpj.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testAtencionpj.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testAtencionpj.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testAtencionpj.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testAtencionpj.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testAtencionpj.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testAtencionpj.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Atencionpj in Elasticsearch
        Atencionpj atencionpjEs = atencionpjSearchRepository.findOne(testAtencionpj.getId());
        assertThat(atencionpjEs).isEqualToComparingFieldByField(testAtencionpj);
    }

    @Test
    @Transactional
    public void createAtencionpjWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atencionpjRepository.findAll().size();

        // Create the Atencionpj with an existing ID
        atencionpj.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        // Validate the Atencionpj in the database
        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodregIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvCodreg(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodzonIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvCodzon(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCorrelIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setnCorrel(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvObsatenIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvObsaten(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgtelIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvFlgtel(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgadmIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvFlgadm(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgtelrIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvFlgtelr(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgatenIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvFlgaten(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodlocIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setvCodloc(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setnUsuareg(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.settFecreg(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setnFlgactivo(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = atencionpjRepository.findAll().size();
        // set the field null
        atencionpj.setnSedereg(null);

        // Create the Atencionpj, which fails.

        restAtencionpjMockMvc.perform(post("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isBadRequest());

        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAtencionpjs() throws Exception {
        // Initialize the database
        atencionpjRepository.saveAndFlush(atencionpj);

        // Get all the atencionpjList
        restAtencionpjMockMvc.perform(get("/api/atencionpjs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atencionpj.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].dFecaten").value(hasItem(DEFAULT_D_FECATEN.toString())))
            .andExpect(jsonPath("$.[*].vObsaten").value(hasItem(DEFAULT_V_OBSATEN.toString())))
            .andExpect(jsonPath("$.[*].vFlgtel").value(hasItem(DEFAULT_V_FLGTEL.toString())))
            .andExpect(jsonPath("$.[*].vFlgadm").value(hasItem(DEFAULT_V_FLGADM.toString())))
            .andExpect(jsonPath("$.[*].vFlgtelr").value(hasItem(DEFAULT_V_FLGTELR.toString())))
            .andExpect(jsonPath("$.[*].vFlgaten").value(hasItem(DEFAULT_V_FLGATEN.toString())))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getAtencionpj() throws Exception {
        // Initialize the database
        atencionpjRepository.saveAndFlush(atencionpj);

        // Get the atencionpj
        restAtencionpjMockMvc.perform(get("/api/atencionpjs/{id}", atencionpj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(atencionpj.getId().intValue()))
            .andExpect(jsonPath("$.vCodreg").value(DEFAULT_V_CODREG.toString()))
            .andExpect(jsonPath("$.vCodzon").value(DEFAULT_V_CODZON.toString()))
            .andExpect(jsonPath("$.nCorrel").value(DEFAULT_N_CORREL))
            .andExpect(jsonPath("$.dFecaten").value(DEFAULT_D_FECATEN.toString()))
            .andExpect(jsonPath("$.vObsaten").value(DEFAULT_V_OBSATEN.toString()))
            .andExpect(jsonPath("$.vFlgtel").value(DEFAULT_V_FLGTEL.toString()))
            .andExpect(jsonPath("$.vFlgadm").value(DEFAULT_V_FLGADM.toString()))
            .andExpect(jsonPath("$.vFlgtelr").value(DEFAULT_V_FLGTELR.toString()))
            .andExpect(jsonPath("$.vFlgaten").value(DEFAULT_V_FLGATEN.toString()))
            .andExpect(jsonPath("$.vCodloc").value(DEFAULT_V_CODLOC.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingAtencionpj() throws Exception {
        // Get the atencionpj
        restAtencionpjMockMvc.perform(get("/api/atencionpjs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtencionpj() throws Exception {
        // Initialize the database
        atencionpjRepository.saveAndFlush(atencionpj);
        atencionpjSearchRepository.save(atencionpj);
        int databaseSizeBeforeUpdate = atencionpjRepository.findAll().size();

        // Update the atencionpj
        Atencionpj updatedAtencionpj = atencionpjRepository.findOne(atencionpj.getId());
        updatedAtencionpj
            .vCodreg(UPDATED_V_CODREG)
            .vCodzon(UPDATED_V_CODZON)
            .nCorrel(UPDATED_N_CORREL)
            .dFecaten(UPDATED_D_FECATEN)
            .vObsaten(UPDATED_V_OBSATEN)
            .vFlgtel(UPDATED_V_FLGTEL)
            .vFlgadm(UPDATED_V_FLGADM)
            .vFlgtelr(UPDATED_V_FLGTELR)
            .vFlgaten(UPDATED_V_FLGATEN)
            .vCodloc(UPDATED_V_CODLOC)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restAtencionpjMockMvc.perform(put("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAtencionpj)))
            .andExpect(status().isOk());

        // Validate the Atencionpj in the database
        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeUpdate);
        Atencionpj testAtencionpj = atencionpjList.get(atencionpjList.size() - 1);
        assertThat(testAtencionpj.getvCodreg()).isEqualTo(UPDATED_V_CODREG);
        assertThat(testAtencionpj.getvCodzon()).isEqualTo(UPDATED_V_CODZON);
        assertThat(testAtencionpj.getnCorrel()).isEqualTo(UPDATED_N_CORREL);
        assertThat(testAtencionpj.getdFecaten()).isEqualTo(UPDATED_D_FECATEN);
        assertThat(testAtencionpj.getvObsaten()).isEqualTo(UPDATED_V_OBSATEN);
        assertThat(testAtencionpj.getvFlgtel()).isEqualTo(UPDATED_V_FLGTEL);
        assertThat(testAtencionpj.getvFlgadm()).isEqualTo(UPDATED_V_FLGADM);
        assertThat(testAtencionpj.getvFlgtelr()).isEqualTo(UPDATED_V_FLGTELR);
        assertThat(testAtencionpj.getvFlgaten()).isEqualTo(UPDATED_V_FLGATEN);
        assertThat(testAtencionpj.getvCodloc()).isEqualTo(UPDATED_V_CODLOC);
        assertThat(testAtencionpj.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testAtencionpj.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testAtencionpj.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testAtencionpj.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testAtencionpj.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testAtencionpj.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testAtencionpj.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Atencionpj in Elasticsearch
        Atencionpj atencionpjEs = atencionpjSearchRepository.findOne(testAtencionpj.getId());
        assertThat(atencionpjEs).isEqualToComparingFieldByField(testAtencionpj);
    }

    @Test
    @Transactional
    public void updateNonExistingAtencionpj() throws Exception {
        int databaseSizeBeforeUpdate = atencionpjRepository.findAll().size();

        // Create the Atencionpj

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAtencionpjMockMvc.perform(put("/api/atencionpjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atencionpj)))
            .andExpect(status().isCreated());

        // Validate the Atencionpj in the database
        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAtencionpj() throws Exception {
        // Initialize the database
        atencionpjRepository.saveAndFlush(atencionpj);
        atencionpjSearchRepository.save(atencionpj);
        int databaseSizeBeforeDelete = atencionpjRepository.findAll().size();

        // Get the atencionpj
        restAtencionpjMockMvc.perform(delete("/api/atencionpjs/{id}", atencionpj.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean atencionpjExistsInEs = atencionpjSearchRepository.exists(atencionpj.getId());
        assertThat(atencionpjExistsInEs).isFalse();

        // Validate the database is empty
        List<Atencionpj> atencionpjList = atencionpjRepository.findAll();
        assertThat(atencionpjList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAtencionpj() throws Exception {
        // Initialize the database
        atencionpjRepository.saveAndFlush(atencionpj);
        atencionpjSearchRepository.save(atencionpj);

        // Search the atencionpj
        restAtencionpjMockMvc.perform(get("/api/_search/atencionpjs?query=id:" + atencionpj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atencionpj.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].dFecaten").value(hasItem(DEFAULT_D_FECATEN.toString())))
            .andExpect(jsonPath("$.[*].vObsaten").value(hasItem(DEFAULT_V_OBSATEN.toString())))
            .andExpect(jsonPath("$.[*].vFlgtel").value(hasItem(DEFAULT_V_FLGTEL.toString())))
            .andExpect(jsonPath("$.[*].vFlgadm").value(hasItem(DEFAULT_V_FLGADM.toString())))
            .andExpect(jsonPath("$.[*].vFlgtelr").value(hasItem(DEFAULT_V_FLGTELR.toString())))
            .andExpect(jsonPath("$.[*].vFlgaten").value(hasItem(DEFAULT_V_FLGATEN.toString())))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Atencionpj.class);
        Atencionpj atencionpj1 = new Atencionpj();
        atencionpj1.setId(1L);
        Atencionpj atencionpj2 = new Atencionpj();
        atencionpj2.setId(atencionpj1.getId());
        assertThat(atencionpj1).isEqualTo(atencionpj2);
        atencionpj2.setId(2L);
        assertThat(atencionpj1).isNotEqualTo(atencionpj2);
        atencionpj1.setId(null);
        assertThat(atencionpj1).isNotEqualTo(atencionpj2);
    }
}
