package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Oridenu;
import pe.gob.trabajo.repository.OridenuRepository;
import pe.gob.trabajo.repository.search.OridenuSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OridenuResource REST controller.
 *
 * @see OridenuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class OridenuResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_ESTADO = "A";
    private static final String UPDATED_V_ESTADO = "B";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private OridenuRepository oridenuRepository;

    @Autowired
    private OridenuSearchRepository oridenuSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOridenuMockMvc;

    private Oridenu oridenu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OridenuResource oridenuResource = new OridenuResource(oridenuRepository, oridenuSearchRepository);
        this.restOridenuMockMvc = MockMvcBuilders.standaloneSetup(oridenuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Oridenu createEntity(EntityManager em) {
        Oridenu oridenu = new Oridenu()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vEstado(DEFAULT_V_ESTADO)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return oridenu;
    }

    @Before
    public void initTest() {
        oridenuSearchRepository.deleteAll();
        oridenu = createEntity(em);
    }

    @Test
    @Transactional
    public void createOridenu() throws Exception {
        int databaseSizeBeforeCreate = oridenuRepository.findAll().size();

        // Create the Oridenu
        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isCreated());

        // Validate the Oridenu in the database
        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeCreate + 1);
        Oridenu testOridenu = oridenuList.get(oridenuList.size() - 1);
        assertThat(testOridenu.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testOridenu.getvEstado()).isEqualTo(DEFAULT_V_ESTADO);
        assertThat(testOridenu.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testOridenu.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testOridenu.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testOridenu.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testOridenu.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testOridenu.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testOridenu.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Oridenu in Elasticsearch
        Oridenu oridenuEs = oridenuSearchRepository.findOne(testOridenu.getId());
        assertThat(oridenuEs).isEqualToComparingFieldByField(testOridenu);
    }

    @Test
    @Transactional
    public void createOridenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = oridenuRepository.findAll().size();

        // Create the Oridenu with an existing ID
        oridenu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isBadRequest());

        // Validate the Oridenu in the database
        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = oridenuRepository.findAll().size();
        // set the field null
        oridenu.setvDescrip(null);

        // Create the Oridenu, which fails.

        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isBadRequest());

        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvEstadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = oridenuRepository.findAll().size();
        // set the field null
        oridenu.setvEstado(null);

        // Create the Oridenu, which fails.

        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isBadRequest());

        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = oridenuRepository.findAll().size();
        // set the field null
        oridenu.setvUsuareg(null);

        // Create the Oridenu, which fails.

        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isBadRequest());

        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = oridenuRepository.findAll().size();
        // set the field null
        oridenu.settFecreg(null);

        // Create the Oridenu, which fails.

        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isBadRequest());

        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = oridenuRepository.findAll().size();
        // set the field null
        oridenu.setnSedereg(null);

        // Create the Oridenu, which fails.

        restOridenuMockMvc.perform(post("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isBadRequest());

        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOridenus() throws Exception {
        // Initialize the database
        oridenuRepository.saveAndFlush(oridenu);

        // Get all the oridenuList
        restOridenuMockMvc.perform(get("/api/oridenus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(oridenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vEstado").value(hasItem(DEFAULT_V_ESTADO.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getOridenu() throws Exception {
        // Initialize the database
        oridenuRepository.saveAndFlush(oridenu);

        // Get the oridenu
        restOridenuMockMvc.perform(get("/api/oridenus/{id}", oridenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(oridenu.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vEstado").value(DEFAULT_V_ESTADO.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingOridenu() throws Exception {
        // Get the oridenu
        restOridenuMockMvc.perform(get("/api/oridenus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOridenu() throws Exception {
        // Initialize the database
        oridenuRepository.saveAndFlush(oridenu);
        oridenuSearchRepository.save(oridenu);
        int databaseSizeBeforeUpdate = oridenuRepository.findAll().size();

        // Update the oridenu
        Oridenu updatedOridenu = oridenuRepository.findOne(oridenu.getId());
        updatedOridenu
            .vDescrip(UPDATED_V_DESCRIP)
            .vEstado(UPDATED_V_ESTADO)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restOridenuMockMvc.perform(put("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOridenu)))
            .andExpect(status().isOk());

        // Validate the Oridenu in the database
        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeUpdate);
        Oridenu testOridenu = oridenuList.get(oridenuList.size() - 1);
        assertThat(testOridenu.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testOridenu.getvEstado()).isEqualTo(UPDATED_V_ESTADO);
        assertThat(testOridenu.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testOridenu.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testOridenu.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testOridenu.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testOridenu.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testOridenu.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testOridenu.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Oridenu in Elasticsearch
        Oridenu oridenuEs = oridenuSearchRepository.findOne(testOridenu.getId());
        assertThat(oridenuEs).isEqualToComparingFieldByField(testOridenu);
    }

    @Test
    @Transactional
    public void updateNonExistingOridenu() throws Exception {
        int databaseSizeBeforeUpdate = oridenuRepository.findAll().size();

        // Create the Oridenu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOridenuMockMvc.perform(put("/api/oridenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oridenu)))
            .andExpect(status().isCreated());

        // Validate the Oridenu in the database
        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteOridenu() throws Exception {
        // Initialize the database
        oridenuRepository.saveAndFlush(oridenu);
        oridenuSearchRepository.save(oridenu);
        int databaseSizeBeforeDelete = oridenuRepository.findAll().size();

        // Get the oridenu
        restOridenuMockMvc.perform(delete("/api/oridenus/{id}", oridenu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean oridenuExistsInEs = oridenuSearchRepository.exists(oridenu.getId());
        assertThat(oridenuExistsInEs).isFalse();

        // Validate the database is empty
        List<Oridenu> oridenuList = oridenuRepository.findAll();
        assertThat(oridenuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchOridenu() throws Exception {
        // Initialize the database
        oridenuRepository.saveAndFlush(oridenu);
        oridenuSearchRepository.save(oridenu);

        // Search the oridenu
        restOridenuMockMvc.perform(get("/api/_search/oridenus?query=id:" + oridenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(oridenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vEstado").value(hasItem(DEFAULT_V_ESTADO.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Oridenu.class);
        Oridenu oridenu1 = new Oridenu();
        oridenu1.setId(1L);
        Oridenu oridenu2 = new Oridenu();
        oridenu2.setId(oridenu1.getId());
        assertThat(oridenu1).isEqualTo(oridenu2);
        oridenu2.setId(2L);
        assertThat(oridenu1).isNotEqualTo(oridenu2);
        oridenu1.setId(null);
        assertThat(oridenu1).isNotEqualTo(oridenu2);
    }
}
