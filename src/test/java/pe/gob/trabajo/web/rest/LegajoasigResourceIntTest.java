package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Legajoasig;
import pe.gob.trabajo.repository.LegajoasigRepository;
import pe.gob.trabajo.repository.search.LegajoasigSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LegajoasigResource REST controller.
 *
 * @see LegajoasigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class LegajoasigResourceIntTest {

    private static final String DEFAULT_V_CODREG = "AA";
    private static final String UPDATED_V_CODREG = "BB";

    private static final String DEFAULT_V_CODZON = "AA";
    private static final String UPDATED_V_CODZON = "BB";

    private static final Integer DEFAULT_N_CORREL = 1;
    private static final Integer UPDATED_N_CORREL = 2;

    private static final LocalDate DEFAULT_D_FECASIG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECASIG = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_HOSTREG = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTREG = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private LegajoasigRepository legajoasigRepository;

    @Autowired
    private LegajoasigSearchRepository legajoasigSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLegajoasigMockMvc;

    private Legajoasig legajoasig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LegajoasigResource legajoasigResource = new LegajoasigResource(legajoasigRepository, legajoasigSearchRepository);
        this.restLegajoasigMockMvc = MockMvcBuilders.standaloneSetup(legajoasigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Legajoasig createEntity(EntityManager em) {
        Legajoasig legajoasig = new Legajoasig()
            .vCodreg(DEFAULT_V_CODREG)
            .vCodzon(DEFAULT_V_CODZON)
            .nCorrel(DEFAULT_N_CORREL)
            .dFecasig(DEFAULT_D_FECASIG)
            .vHostreg(DEFAULT_V_HOSTREG)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return legajoasig;
    }

    @Before
    public void initTest() {
        legajoasigSearchRepository.deleteAll();
        legajoasig = createEntity(em);
    }

    @Test
    @Transactional
    public void createLegajoasig() throws Exception {
        int databaseSizeBeforeCreate = legajoasigRepository.findAll().size();

        // Create the Legajoasig
        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isCreated());

        // Validate the Legajoasig in the database
        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeCreate + 1);
        Legajoasig testLegajoasig = legajoasigList.get(legajoasigList.size() - 1);
        assertThat(testLegajoasig.getvCodreg()).isEqualTo(DEFAULT_V_CODREG);
        assertThat(testLegajoasig.getvCodzon()).isEqualTo(DEFAULT_V_CODZON);
        assertThat(testLegajoasig.getnCorrel()).isEqualTo(DEFAULT_N_CORREL);
        assertThat(testLegajoasig.getdFecasig()).isEqualTo(DEFAULT_D_FECASIG);
        assertThat(testLegajoasig.getvHostreg()).isEqualTo(DEFAULT_V_HOSTREG);
        assertThat(testLegajoasig.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testLegajoasig.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testLegajoasig.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testLegajoasig.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testLegajoasig.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testLegajoasig.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testLegajoasig.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Legajoasig in Elasticsearch
        Legajoasig legajoasigEs = legajoasigSearchRepository.findOne(testLegajoasig.getId());
        assertThat(legajoasigEs).isEqualToComparingFieldByField(testLegajoasig);
    }

    @Test
    @Transactional
    public void createLegajoasigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = legajoasigRepository.findAll().size();

        // Create the Legajoasig with an existing ID
        legajoasig.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        // Validate the Legajoasig in the database
        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setvCodreg(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodzonIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setvCodzon(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCorrelIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setnCorrel(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setvHostreg(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setnUsuareg(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.settFecreg(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setnFlgactivo(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoasigRepository.findAll().size();
        // set the field null
        legajoasig.setnSedereg(null);

        // Create the Legajoasig, which fails.

        restLegajoasigMockMvc.perform(post("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isBadRequest());

        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLegajoasigs() throws Exception {
        // Initialize the database
        legajoasigRepository.saveAndFlush(legajoasig);

        // Get all the legajoasigList
        restLegajoasigMockMvc.perform(get("/api/legajoasigs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(legajoasig.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].dFecasig").value(hasItem(DEFAULT_D_FECASIG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getLegajoasig() throws Exception {
        // Initialize the database
        legajoasigRepository.saveAndFlush(legajoasig);

        // Get the legajoasig
        restLegajoasigMockMvc.perform(get("/api/legajoasigs/{id}", legajoasig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(legajoasig.getId().intValue()))
            .andExpect(jsonPath("$.vCodreg").value(DEFAULT_V_CODREG.toString()))
            .andExpect(jsonPath("$.vCodzon").value(DEFAULT_V_CODZON.toString()))
            .andExpect(jsonPath("$.nCorrel").value(DEFAULT_N_CORREL))
            .andExpect(jsonPath("$.dFecasig").value(DEFAULT_D_FECASIG.toString()))
            .andExpect(jsonPath("$.vHostreg").value(DEFAULT_V_HOSTREG.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingLegajoasig() throws Exception {
        // Get the legajoasig
        restLegajoasigMockMvc.perform(get("/api/legajoasigs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLegajoasig() throws Exception {
        // Initialize the database
        legajoasigRepository.saveAndFlush(legajoasig);
        legajoasigSearchRepository.save(legajoasig);
        int databaseSizeBeforeUpdate = legajoasigRepository.findAll().size();

        // Update the legajoasig
        Legajoasig updatedLegajoasig = legajoasigRepository.findOne(legajoasig.getId());
        updatedLegajoasig
            .vCodreg(UPDATED_V_CODREG)
            .vCodzon(UPDATED_V_CODZON)
            .nCorrel(UPDATED_N_CORREL)
            .dFecasig(UPDATED_D_FECASIG)
            .vHostreg(UPDATED_V_HOSTREG)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restLegajoasigMockMvc.perform(put("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLegajoasig)))
            .andExpect(status().isOk());

        // Validate the Legajoasig in the database
        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeUpdate);
        Legajoasig testLegajoasig = legajoasigList.get(legajoasigList.size() - 1);
        assertThat(testLegajoasig.getvCodreg()).isEqualTo(UPDATED_V_CODREG);
        assertThat(testLegajoasig.getvCodzon()).isEqualTo(UPDATED_V_CODZON);
        assertThat(testLegajoasig.getnCorrel()).isEqualTo(UPDATED_N_CORREL);
        assertThat(testLegajoasig.getdFecasig()).isEqualTo(UPDATED_D_FECASIG);
        assertThat(testLegajoasig.getvHostreg()).isEqualTo(UPDATED_V_HOSTREG);
        assertThat(testLegajoasig.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testLegajoasig.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testLegajoasig.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testLegajoasig.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testLegajoasig.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testLegajoasig.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testLegajoasig.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Legajoasig in Elasticsearch
        Legajoasig legajoasigEs = legajoasigSearchRepository.findOne(testLegajoasig.getId());
        assertThat(legajoasigEs).isEqualToComparingFieldByField(testLegajoasig);
    }

    @Test
    @Transactional
    public void updateNonExistingLegajoasig() throws Exception {
        int databaseSizeBeforeUpdate = legajoasigRepository.findAll().size();

        // Create the Legajoasig

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLegajoasigMockMvc.perform(put("/api/legajoasigs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajoasig)))
            .andExpect(status().isCreated());

        // Validate the Legajoasig in the database
        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLegajoasig() throws Exception {
        // Initialize the database
        legajoasigRepository.saveAndFlush(legajoasig);
        legajoasigSearchRepository.save(legajoasig);
        int databaseSizeBeforeDelete = legajoasigRepository.findAll().size();

        // Get the legajoasig
        restLegajoasigMockMvc.perform(delete("/api/legajoasigs/{id}", legajoasig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean legajoasigExistsInEs = legajoasigSearchRepository.exists(legajoasig.getId());
        assertThat(legajoasigExistsInEs).isFalse();

        // Validate the database is empty
        List<Legajoasig> legajoasigList = legajoasigRepository.findAll();
        assertThat(legajoasigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLegajoasig() throws Exception {
        // Initialize the database
        legajoasigRepository.saveAndFlush(legajoasig);
        legajoasigSearchRepository.save(legajoasig);

        // Search the legajoasig
        restLegajoasigMockMvc.perform(get("/api/_search/legajoasigs?query=id:" + legajoasig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(legajoasig.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].dFecasig").value(hasItem(DEFAULT_D_FECASIG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Legajoasig.class);
        Legajoasig legajoasig1 = new Legajoasig();
        legajoasig1.setId(1L);
        Legajoasig legajoasig2 = new Legajoasig();
        legajoasig2.setId(legajoasig1.getId());
        assertThat(legajoasig1).isEqualTo(legajoasig2);
        legajoasig2.setId(2L);
        assertThat(legajoasig1).isNotEqualTo(legajoasig2);
        legajoasig1.setId(null);
        assertThat(legajoasig1).isNotEqualTo(legajoasig2);
    }
}
