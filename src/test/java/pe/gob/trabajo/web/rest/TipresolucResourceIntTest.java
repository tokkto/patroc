package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Tipresoluc;
import pe.gob.trabajo.repository.TipresolucRepository;
import pe.gob.trabajo.repository.search.TipresolucSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipresolucResource REST controller.
 *
 * @see TipresolucResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class TipresolucResourceIntTest {

    private static final String DEFAULT_V_DESTIPRES = "AAAAAAAAAA";
    private static final String UPDATED_V_DESTIPRES = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipresolucRepository tipresolucRepository;

    @Autowired
    private TipresolucSearchRepository tipresolucSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipresolucMockMvc;

    private Tipresoluc tipresoluc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipresolucResource tipresolucResource = new TipresolucResource(tipresolucRepository, tipresolucSearchRepository);
        this.restTipresolucMockMvc = MockMvcBuilders.standaloneSetup(tipresolucResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipresoluc createEntity(EntityManager em) {
        Tipresoluc tipresoluc = new Tipresoluc()
            .vDestipres(DEFAULT_V_DESTIPRES)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipresoluc;
    }

    @Before
    public void initTest() {
        tipresolucSearchRepository.deleteAll();
        tipresoluc = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipresoluc() throws Exception {
        int databaseSizeBeforeCreate = tipresolucRepository.findAll().size();

        // Create the Tipresoluc
        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isCreated());

        // Validate the Tipresoluc in the database
        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeCreate + 1);
        Tipresoluc testTipresoluc = tipresolucList.get(tipresolucList.size() - 1);
        assertThat(testTipresoluc.getvDestipres()).isEqualTo(DEFAULT_V_DESTIPRES);
        assertThat(testTipresoluc.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipresoluc.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipresoluc.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipresoluc.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipresoluc.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipresoluc.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipresoluc.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipresoluc in Elasticsearch
        Tipresoluc tipresolucEs = tipresolucSearchRepository.findOne(testTipresoluc.getId());
        assertThat(tipresolucEs).isEqualToComparingFieldByField(testTipresoluc);
    }

    @Test
    @Transactional
    public void createTipresolucWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipresolucRepository.findAll().size();

        // Create the Tipresoluc with an existing ID
        tipresoluc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isBadRequest());

        // Validate the Tipresoluc in the database
        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDestipresIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresolucRepository.findAll().size();
        // set the field null
        tipresoluc.setvDestipres(null);

        // Create the Tipresoluc, which fails.

        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isBadRequest());

        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresolucRepository.findAll().size();
        // set the field null
        tipresoluc.setnUsuareg(null);

        // Create the Tipresoluc, which fails.

        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isBadRequest());

        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresolucRepository.findAll().size();
        // set the field null
        tipresoluc.settFecreg(null);

        // Create the Tipresoluc, which fails.

        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isBadRequest());

        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresolucRepository.findAll().size();
        // set the field null
        tipresoluc.setnFlgactivo(null);

        // Create the Tipresoluc, which fails.

        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isBadRequest());

        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresolucRepository.findAll().size();
        // set the field null
        tipresoluc.setnSedereg(null);

        // Create the Tipresoluc, which fails.

        restTipresolucMockMvc.perform(post("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isBadRequest());

        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipresolucs() throws Exception {
        // Initialize the database
        tipresolucRepository.saveAndFlush(tipresoluc);

        // Get all the tipresolucList
        restTipresolucMockMvc.perform(get("/api/tipresolucs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipresoluc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDestipres").value(hasItem(DEFAULT_V_DESTIPRES.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipresoluc() throws Exception {
        // Initialize the database
        tipresolucRepository.saveAndFlush(tipresoluc);

        // Get the tipresoluc
        restTipresolucMockMvc.perform(get("/api/tipresolucs/{id}", tipresoluc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipresoluc.getId().intValue()))
            .andExpect(jsonPath("$.vDestipres").value(DEFAULT_V_DESTIPRES.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipresoluc() throws Exception {
        // Get the tipresoluc
        restTipresolucMockMvc.perform(get("/api/tipresolucs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipresoluc() throws Exception {
        // Initialize the database
        tipresolucRepository.saveAndFlush(tipresoluc);
        tipresolucSearchRepository.save(tipresoluc);
        int databaseSizeBeforeUpdate = tipresolucRepository.findAll().size();

        // Update the tipresoluc
        Tipresoluc updatedTipresoluc = tipresolucRepository.findOne(tipresoluc.getId());
        updatedTipresoluc
            .vDestipres(UPDATED_V_DESTIPRES)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipresolucMockMvc.perform(put("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipresoluc)))
            .andExpect(status().isOk());

        // Validate the Tipresoluc in the database
        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeUpdate);
        Tipresoluc testTipresoluc = tipresolucList.get(tipresolucList.size() - 1);
        assertThat(testTipresoluc.getvDestipres()).isEqualTo(UPDATED_V_DESTIPRES);
        assertThat(testTipresoluc.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipresoluc.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipresoluc.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipresoluc.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipresoluc.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipresoluc.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipresoluc.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipresoluc in Elasticsearch
        Tipresoluc tipresolucEs = tipresolucSearchRepository.findOne(testTipresoluc.getId());
        assertThat(tipresolucEs).isEqualToComparingFieldByField(testTipresoluc);
    }

    @Test
    @Transactional
    public void updateNonExistingTipresoluc() throws Exception {
        int databaseSizeBeforeUpdate = tipresolucRepository.findAll().size();

        // Create the Tipresoluc

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipresolucMockMvc.perform(put("/api/tipresolucs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresoluc)))
            .andExpect(status().isCreated());

        // Validate the Tipresoluc in the database
        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipresoluc() throws Exception {
        // Initialize the database
        tipresolucRepository.saveAndFlush(tipresoluc);
        tipresolucSearchRepository.save(tipresoluc);
        int databaseSizeBeforeDelete = tipresolucRepository.findAll().size();

        // Get the tipresoluc
        restTipresolucMockMvc.perform(delete("/api/tipresolucs/{id}", tipresoluc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipresolucExistsInEs = tipresolucSearchRepository.exists(tipresoluc.getId());
        assertThat(tipresolucExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipresoluc> tipresolucList = tipresolucRepository.findAll();
        assertThat(tipresolucList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipresoluc() throws Exception {
        // Initialize the database
        tipresolucRepository.saveAndFlush(tipresoluc);
        tipresolucSearchRepository.save(tipresoluc);

        // Search the tipresoluc
        restTipresolucMockMvc.perform(get("/api/_search/tipresolucs?query=id:" + tipresoluc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipresoluc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDestipres").value(hasItem(DEFAULT_V_DESTIPRES.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipresoluc.class);
        Tipresoluc tipresoluc1 = new Tipresoluc();
        tipresoluc1.setId(1L);
        Tipresoluc tipresoluc2 = new Tipresoluc();
        tipresoluc2.setId(tipresoluc1.getId());
        assertThat(tipresoluc1).isEqualTo(tipresoluc2);
        tipresoluc2.setId(2L);
        assertThat(tipresoluc1).isNotEqualTo(tipresoluc2);
        tipresoluc1.setId(null);
        assertThat(tipresoluc1).isNotEqualTo(tipresoluc2);
    }
}
