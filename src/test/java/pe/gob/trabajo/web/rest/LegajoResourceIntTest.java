package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Legajo;
import pe.gob.trabajo.repository.LegajoRepository;
import pe.gob.trabajo.repository.search.LegajoSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LegajoResource REST controller.
 *
 * @see LegajoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class LegajoResourceIntTest {

    private static final String DEFAULT_V_CODREG = "AA";
    private static final String UPDATED_V_CODREG = "BB";

    private static final String DEFAULT_V_CODZON = "AA";
    private static final String UPDATED_V_CODZON = "BB";

    private static final String DEFAULT_V_NUMEXP = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMEXP = "BBBBBBBBBB";

    private static final String DEFAULT_V_NOMJUZG = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMJUZG = "BBBBBBBBBB";

    private static final String DEFAULT_V_NOMJUEZ = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMJUEZ = "BBBBBBBBBB";

    private static final String DEFAULT_V_ESTLEG = "A";
    private static final String UPDATED_V_ESTLEG = "B";

    private static final LocalDate DEFAULT_D_FECCONC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECCONC = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_CODUSUREG = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUREG = "BBBBBBBBBB";

    private static final String DEFAULT_V_HOSTREG = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTREG = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODUSUMOD = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUMOD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECMOD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECMOD = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_HOSTMOD = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTMOD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECEXP = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECEXP = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_NUMLEG = "AAAAAA";
    private static final String UPDATED_V_NUMLEG = "BBBBBB";

    private static final Integer DEFAULT_N_CODSUC = 1;
    private static final Integer UPDATED_N_CODSUC = 2;

    private static final String DEFAULT_V_FLGINST = "A";
    private static final String UPDATED_V_FLGINST = "B";

    private static final String DEFAULT_V_NUMEXPDA = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMEXPDA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECEXPDA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECEXPDA = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_NOMJUZDA = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMJUZDA = "BBBBBBBBBB";

    private static final String DEFAULT_V_NOMJUEZDA = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMJUEZDA = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODLEGRIG = "AAAAAA";
    private static final String UPDATED_V_CODLEGRIG = "BBBBBB";

    private static final String DEFAULT_V_TIPORIG = "A";
    private static final String UPDATED_V_TIPORIG = "B";

    private static final String DEFAULT_V_FLGARCH = "A";
    private static final String UPDATED_V_FLGARCH = "B";

    private static final String DEFAULT_V_FLGCAUT = "A";
    private static final String UPDATED_V_FLGCAUT = "B";

    private static final String DEFAULT_V_FLGANUL = "A";
    private static final String UPDATED_V_FLGANUL = "B";

    private static final String DEFAULT_V_CODLOC = "AAAAAA";
    private static final String UPDATED_V_CODLOC = "BBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private LegajoRepository legajoRepository;

    @Autowired
    private LegajoSearchRepository legajoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLegajoMockMvc;

    private Legajo legajo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LegajoResource legajoResource = new LegajoResource(legajoRepository, legajoSearchRepository);
        this.restLegajoMockMvc = MockMvcBuilders.standaloneSetup(legajoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Legajo createEntity(EntityManager em) {
        Legajo legajo = new Legajo()
            .vCodreg(DEFAULT_V_CODREG)
            .vCodzon(DEFAULT_V_CODZON)
            .vNumexp(DEFAULT_V_NUMEXP)
            .vNomjuzg(DEFAULT_V_NOMJUZG)
            .vNomjuez(DEFAULT_V_NOMJUEZ)
            .vEstleg(DEFAULT_V_ESTLEG)
            .dFecconc(DEFAULT_D_FECCONC)
            .vCodusureg(DEFAULT_V_CODUSUREG)
            .vHostreg(DEFAULT_V_HOSTREG)
            .vCodusumod(DEFAULT_V_CODUSUMOD)
            .dFecmod(DEFAULT_D_FECMOD)
            .vHostmod(DEFAULT_V_HOSTMOD)
            .dFecexp(DEFAULT_D_FECEXP)
            .vNumleg(DEFAULT_V_NUMLEG)
            .nCodsuc(DEFAULT_N_CODSUC)
            .vFlginst(DEFAULT_V_FLGINST)
            .vNumexpda(DEFAULT_V_NUMEXPDA)
            .dFecexpda(DEFAULT_D_FECEXPDA)
            .vNomjuzda(DEFAULT_V_NOMJUZDA)
            .vNomjuezda(DEFAULT_V_NOMJUEZDA)
            .vCodlegrig(DEFAULT_V_CODLEGRIG)
            .vTiporig(DEFAULT_V_TIPORIG)
            .vFlgarch(DEFAULT_V_FLGARCH)
            .vFlgcaut(DEFAULT_V_FLGCAUT)
            .vFlganul(DEFAULT_V_FLGANUL)
            .vCodloc(DEFAULT_V_CODLOC)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return legajo;
    }

    @Before
    public void initTest() {
        legajoSearchRepository.deleteAll();
        legajo = createEntity(em);
    }

    @Test
    @Transactional
    public void createLegajo() throws Exception {
        int databaseSizeBeforeCreate = legajoRepository.findAll().size();

        // Create the Legajo
        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isCreated());

        // Validate the Legajo in the database
        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeCreate + 1);
        Legajo testLegajo = legajoList.get(legajoList.size() - 1);
        assertThat(testLegajo.getvCodreg()).isEqualTo(DEFAULT_V_CODREG);
        assertThat(testLegajo.getvCodzon()).isEqualTo(DEFAULT_V_CODZON);
        assertThat(testLegajo.getvNumexp()).isEqualTo(DEFAULT_V_NUMEXP);
        assertThat(testLegajo.getvNomjuzg()).isEqualTo(DEFAULT_V_NOMJUZG);
        assertThat(testLegajo.getvNomjuez()).isEqualTo(DEFAULT_V_NOMJUEZ);
        assertThat(testLegajo.getvEstleg()).isEqualTo(DEFAULT_V_ESTLEG);
        assertThat(testLegajo.getdFecconc()).isEqualTo(DEFAULT_D_FECCONC);
        assertThat(testLegajo.getvCodusureg()).isEqualTo(DEFAULT_V_CODUSUREG);
        assertThat(testLegajo.getvHostreg()).isEqualTo(DEFAULT_V_HOSTREG);
        assertThat(testLegajo.getvCodusumod()).isEqualTo(DEFAULT_V_CODUSUMOD);
        assertThat(testLegajo.getdFecmod()).isEqualTo(DEFAULT_D_FECMOD);
        assertThat(testLegajo.getvHostmod()).isEqualTo(DEFAULT_V_HOSTMOD);
        assertThat(testLegajo.getdFecexp()).isEqualTo(DEFAULT_D_FECEXP);
        assertThat(testLegajo.getvNumleg()).isEqualTo(DEFAULT_V_NUMLEG);
        assertThat(testLegajo.getnCodsuc()).isEqualTo(DEFAULT_N_CODSUC);
        assertThat(testLegajo.getvFlginst()).isEqualTo(DEFAULT_V_FLGINST);
        assertThat(testLegajo.getvNumexpda()).isEqualTo(DEFAULT_V_NUMEXPDA);
        assertThat(testLegajo.getdFecexpda()).isEqualTo(DEFAULT_D_FECEXPDA);
        assertThat(testLegajo.getvNomjuzda()).isEqualTo(DEFAULT_V_NOMJUZDA);
        assertThat(testLegajo.getvNomjuezda()).isEqualTo(DEFAULT_V_NOMJUEZDA);
        assertThat(testLegajo.getvCodlegrig()).isEqualTo(DEFAULT_V_CODLEGRIG);
        assertThat(testLegajo.getvTiporig()).isEqualTo(DEFAULT_V_TIPORIG);
        assertThat(testLegajo.getvFlgarch()).isEqualTo(DEFAULT_V_FLGARCH);
        assertThat(testLegajo.getvFlgcaut()).isEqualTo(DEFAULT_V_FLGCAUT);
        assertThat(testLegajo.getvFlganul()).isEqualTo(DEFAULT_V_FLGANUL);
        assertThat(testLegajo.getvCodloc()).isEqualTo(DEFAULT_V_CODLOC);
        assertThat(testLegajo.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testLegajo.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testLegajo.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testLegajo.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testLegajo.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testLegajo.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testLegajo.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Legajo in Elasticsearch
        Legajo legajoEs = legajoSearchRepository.findOne(testLegajo.getId());
        assertThat(legajoEs).isEqualToComparingFieldByField(testLegajo);
    }

    @Test
    @Transactional
    public void createLegajoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = legajoRepository.findAll().size();

        // Create the Legajo with an existing ID
        legajo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        // Validate the Legajo in the database
        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvCodreg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodzonIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvCodzon(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNumexpIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNumexp(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNomjuzgIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNomjuzg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNomjuezIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNomjuez(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvEstlegIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvEstleg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusuregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvCodusureg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvHostreg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusumodIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvCodusumod(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostmodIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvHostmod(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNumlegIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNumleg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCodsucIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setnCodsuc(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlginstIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvFlginst(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNumexpdaIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNumexpda(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNomjuzdaIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNomjuzda(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNomjuezdaIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvNomjuezda(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodlegrigIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvCodlegrig(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvTiporigIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvTiporig(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgarchIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvFlgarch(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgcautIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvFlgcaut(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlganulIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvFlganul(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodlocIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setvCodloc(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setnUsuareg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.settFecreg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setnFlgactivo(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = legajoRepository.findAll().size();
        // set the field null
        legajo.setnSedereg(null);

        // Create the Legajo, which fails.

        restLegajoMockMvc.perform(post("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isBadRequest());

        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLegajos() throws Exception {
        // Initialize the database
        legajoRepository.saveAndFlush(legajo);

        // Get all the legajoList
        restLegajoMockMvc.perform(get("/api/legajos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(legajo.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].vNumexp").value(hasItem(DEFAULT_V_NUMEXP.toString())))
            .andExpect(jsonPath("$.[*].vNomjuzg").value(hasItem(DEFAULT_V_NOMJUZG.toString())))
            .andExpect(jsonPath("$.[*].vNomjuez").value(hasItem(DEFAULT_V_NOMJUEZ.toString())))
            .andExpect(jsonPath("$.[*].vEstleg").value(hasItem(DEFAULT_V_ESTLEG.toString())))
            .andExpect(jsonPath("$.[*].dFecconc").value(hasItem(DEFAULT_D_FECCONC.toString())))
            .andExpect(jsonPath("$.[*].vCodusureg").value(hasItem(DEFAULT_V_CODUSUREG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].vCodusumod").value(hasItem(DEFAULT_V_CODUSUMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecmod").value(hasItem(DEFAULT_D_FECMOD.toString())))
            .andExpect(jsonPath("$.[*].vHostmod").value(hasItem(DEFAULT_V_HOSTMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecexp").value(hasItem(DEFAULT_D_FECEXP.toString())))
            .andExpect(jsonPath("$.[*].vNumleg").value(hasItem(DEFAULT_V_NUMLEG.toString())))
            .andExpect(jsonPath("$.[*].nCodsuc").value(hasItem(DEFAULT_N_CODSUC)))
            .andExpect(jsonPath("$.[*].vFlginst").value(hasItem(DEFAULT_V_FLGINST.toString())))
            .andExpect(jsonPath("$.[*].vNumexpda").value(hasItem(DEFAULT_V_NUMEXPDA.toString())))
            .andExpect(jsonPath("$.[*].dFecexpda").value(hasItem(DEFAULT_D_FECEXPDA.toString())))
            .andExpect(jsonPath("$.[*].vNomjuzda").value(hasItem(DEFAULT_V_NOMJUZDA.toString())))
            .andExpect(jsonPath("$.[*].vNomjuezda").value(hasItem(DEFAULT_V_NOMJUEZDA.toString())))
            .andExpect(jsonPath("$.[*].vCodlegrig").value(hasItem(DEFAULT_V_CODLEGRIG.toString())))
            .andExpect(jsonPath("$.[*].vTiporig").value(hasItem(DEFAULT_V_TIPORIG.toString())))
            .andExpect(jsonPath("$.[*].vFlgarch").value(hasItem(DEFAULT_V_FLGARCH.toString())))
            .andExpect(jsonPath("$.[*].vFlgcaut").value(hasItem(DEFAULT_V_FLGCAUT.toString())))
            .andExpect(jsonPath("$.[*].vFlganul").value(hasItem(DEFAULT_V_FLGANUL.toString())))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getLegajo() throws Exception {
        // Initialize the database
        legajoRepository.saveAndFlush(legajo);

        // Get the legajo
        restLegajoMockMvc.perform(get("/api/legajos/{id}", legajo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(legajo.getId().intValue()))
            .andExpect(jsonPath("$.vCodreg").value(DEFAULT_V_CODREG.toString()))
            .andExpect(jsonPath("$.vCodzon").value(DEFAULT_V_CODZON.toString()))
            .andExpect(jsonPath("$.vNumexp").value(DEFAULT_V_NUMEXP.toString()))
            .andExpect(jsonPath("$.vNomjuzg").value(DEFAULT_V_NOMJUZG.toString()))
            .andExpect(jsonPath("$.vNomjuez").value(DEFAULT_V_NOMJUEZ.toString()))
            .andExpect(jsonPath("$.vEstleg").value(DEFAULT_V_ESTLEG.toString()))
            .andExpect(jsonPath("$.dFecconc").value(DEFAULT_D_FECCONC.toString()))
            .andExpect(jsonPath("$.vCodusureg").value(DEFAULT_V_CODUSUREG.toString()))
            .andExpect(jsonPath("$.vHostreg").value(DEFAULT_V_HOSTREG.toString()))
            .andExpect(jsonPath("$.vCodusumod").value(DEFAULT_V_CODUSUMOD.toString()))
            .andExpect(jsonPath("$.dFecmod").value(DEFAULT_D_FECMOD.toString()))
            .andExpect(jsonPath("$.vHostmod").value(DEFAULT_V_HOSTMOD.toString()))
            .andExpect(jsonPath("$.dFecexp").value(DEFAULT_D_FECEXP.toString()))
            .andExpect(jsonPath("$.vNumleg").value(DEFAULT_V_NUMLEG.toString()))
            .andExpect(jsonPath("$.nCodsuc").value(DEFAULT_N_CODSUC))
            .andExpect(jsonPath("$.vFlginst").value(DEFAULT_V_FLGINST.toString()))
            .andExpect(jsonPath("$.vNumexpda").value(DEFAULT_V_NUMEXPDA.toString()))
            .andExpect(jsonPath("$.dFecexpda").value(DEFAULT_D_FECEXPDA.toString()))
            .andExpect(jsonPath("$.vNomjuzda").value(DEFAULT_V_NOMJUZDA.toString()))
            .andExpect(jsonPath("$.vNomjuezda").value(DEFAULT_V_NOMJUEZDA.toString()))
            .andExpect(jsonPath("$.vCodlegrig").value(DEFAULT_V_CODLEGRIG.toString()))
            .andExpect(jsonPath("$.vTiporig").value(DEFAULT_V_TIPORIG.toString()))
            .andExpect(jsonPath("$.vFlgarch").value(DEFAULT_V_FLGARCH.toString()))
            .andExpect(jsonPath("$.vFlgcaut").value(DEFAULT_V_FLGCAUT.toString()))
            .andExpect(jsonPath("$.vFlganul").value(DEFAULT_V_FLGANUL.toString()))
            .andExpect(jsonPath("$.vCodloc").value(DEFAULT_V_CODLOC.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingLegajo() throws Exception {
        // Get the legajo
        restLegajoMockMvc.perform(get("/api/legajos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLegajo() throws Exception {
        // Initialize the database
        legajoRepository.saveAndFlush(legajo);
        legajoSearchRepository.save(legajo);
        int databaseSizeBeforeUpdate = legajoRepository.findAll().size();

        // Update the legajo
        Legajo updatedLegajo = legajoRepository.findOne(legajo.getId());
        updatedLegajo
            .vCodreg(UPDATED_V_CODREG)
            .vCodzon(UPDATED_V_CODZON)
            .vNumexp(UPDATED_V_NUMEXP)
            .vNomjuzg(UPDATED_V_NOMJUZG)
            .vNomjuez(UPDATED_V_NOMJUEZ)
            .vEstleg(UPDATED_V_ESTLEG)
            .dFecconc(UPDATED_D_FECCONC)
            .vCodusureg(UPDATED_V_CODUSUREG)
            .vHostreg(UPDATED_V_HOSTREG)
            .vCodusumod(UPDATED_V_CODUSUMOD)
            .dFecmod(UPDATED_D_FECMOD)
            .vHostmod(UPDATED_V_HOSTMOD)
            .dFecexp(UPDATED_D_FECEXP)
            .vNumleg(UPDATED_V_NUMLEG)
            .nCodsuc(UPDATED_N_CODSUC)
            .vFlginst(UPDATED_V_FLGINST)
            .vNumexpda(UPDATED_V_NUMEXPDA)
            .dFecexpda(UPDATED_D_FECEXPDA)
            .vNomjuzda(UPDATED_V_NOMJUZDA)
            .vNomjuezda(UPDATED_V_NOMJUEZDA)
            .vCodlegrig(UPDATED_V_CODLEGRIG)
            .vTiporig(UPDATED_V_TIPORIG)
            .vFlgarch(UPDATED_V_FLGARCH)
            .vFlgcaut(UPDATED_V_FLGCAUT)
            .vFlganul(UPDATED_V_FLGANUL)
            .vCodloc(UPDATED_V_CODLOC)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restLegajoMockMvc.perform(put("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLegajo)))
            .andExpect(status().isOk());

        // Validate the Legajo in the database
        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeUpdate);
        Legajo testLegajo = legajoList.get(legajoList.size() - 1);
        assertThat(testLegajo.getvCodreg()).isEqualTo(UPDATED_V_CODREG);
        assertThat(testLegajo.getvCodzon()).isEqualTo(UPDATED_V_CODZON);
        assertThat(testLegajo.getvNumexp()).isEqualTo(UPDATED_V_NUMEXP);
        assertThat(testLegajo.getvNomjuzg()).isEqualTo(UPDATED_V_NOMJUZG);
        assertThat(testLegajo.getvNomjuez()).isEqualTo(UPDATED_V_NOMJUEZ);
        assertThat(testLegajo.getvEstleg()).isEqualTo(UPDATED_V_ESTLEG);
        assertThat(testLegajo.getdFecconc()).isEqualTo(UPDATED_D_FECCONC);
        assertThat(testLegajo.getvCodusureg()).isEqualTo(UPDATED_V_CODUSUREG);
        assertThat(testLegajo.getvHostreg()).isEqualTo(UPDATED_V_HOSTREG);
        assertThat(testLegajo.getvCodusumod()).isEqualTo(UPDATED_V_CODUSUMOD);
        assertThat(testLegajo.getdFecmod()).isEqualTo(UPDATED_D_FECMOD);
        assertThat(testLegajo.getvHostmod()).isEqualTo(UPDATED_V_HOSTMOD);
        assertThat(testLegajo.getdFecexp()).isEqualTo(UPDATED_D_FECEXP);
        assertThat(testLegajo.getvNumleg()).isEqualTo(UPDATED_V_NUMLEG);
        assertThat(testLegajo.getnCodsuc()).isEqualTo(UPDATED_N_CODSUC);
        assertThat(testLegajo.getvFlginst()).isEqualTo(UPDATED_V_FLGINST);
        assertThat(testLegajo.getvNumexpda()).isEqualTo(UPDATED_V_NUMEXPDA);
        assertThat(testLegajo.getdFecexpda()).isEqualTo(UPDATED_D_FECEXPDA);
        assertThat(testLegajo.getvNomjuzda()).isEqualTo(UPDATED_V_NOMJUZDA);
        assertThat(testLegajo.getvNomjuezda()).isEqualTo(UPDATED_V_NOMJUEZDA);
        assertThat(testLegajo.getvCodlegrig()).isEqualTo(UPDATED_V_CODLEGRIG);
        assertThat(testLegajo.getvTiporig()).isEqualTo(UPDATED_V_TIPORIG);
        assertThat(testLegajo.getvFlgarch()).isEqualTo(UPDATED_V_FLGARCH);
        assertThat(testLegajo.getvFlgcaut()).isEqualTo(UPDATED_V_FLGCAUT);
        assertThat(testLegajo.getvFlganul()).isEqualTo(UPDATED_V_FLGANUL);
        assertThat(testLegajo.getvCodloc()).isEqualTo(UPDATED_V_CODLOC);
        assertThat(testLegajo.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testLegajo.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testLegajo.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testLegajo.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testLegajo.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testLegajo.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testLegajo.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Legajo in Elasticsearch
        Legajo legajoEs = legajoSearchRepository.findOne(testLegajo.getId());
        assertThat(legajoEs).isEqualToComparingFieldByField(testLegajo);
    }

    @Test
    @Transactional
    public void updateNonExistingLegajo() throws Exception {
        int databaseSizeBeforeUpdate = legajoRepository.findAll().size();

        // Create the Legajo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLegajoMockMvc.perform(put("/api/legajos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legajo)))
            .andExpect(status().isCreated());

        // Validate the Legajo in the database
        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLegajo() throws Exception {
        // Initialize the database
        legajoRepository.saveAndFlush(legajo);
        legajoSearchRepository.save(legajo);
        int databaseSizeBeforeDelete = legajoRepository.findAll().size();

        // Get the legajo
        restLegajoMockMvc.perform(delete("/api/legajos/{id}", legajo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean legajoExistsInEs = legajoSearchRepository.exists(legajo.getId());
        assertThat(legajoExistsInEs).isFalse();

        // Validate the database is empty
        List<Legajo> legajoList = legajoRepository.findAll();
        assertThat(legajoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLegajo() throws Exception {
        // Initialize the database
        legajoRepository.saveAndFlush(legajo);
        legajoSearchRepository.save(legajo);

        // Search the legajo
        restLegajoMockMvc.perform(get("/api/_search/legajos?query=id:" + legajo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(legajo.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].vNumexp").value(hasItem(DEFAULT_V_NUMEXP.toString())))
            .andExpect(jsonPath("$.[*].vNomjuzg").value(hasItem(DEFAULT_V_NOMJUZG.toString())))
            .andExpect(jsonPath("$.[*].vNomjuez").value(hasItem(DEFAULT_V_NOMJUEZ.toString())))
            .andExpect(jsonPath("$.[*].vEstleg").value(hasItem(DEFAULT_V_ESTLEG.toString())))
            .andExpect(jsonPath("$.[*].dFecconc").value(hasItem(DEFAULT_D_FECCONC.toString())))
            .andExpect(jsonPath("$.[*].vCodusureg").value(hasItem(DEFAULT_V_CODUSUREG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].vCodusumod").value(hasItem(DEFAULT_V_CODUSUMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecmod").value(hasItem(DEFAULT_D_FECMOD.toString())))
            .andExpect(jsonPath("$.[*].vHostmod").value(hasItem(DEFAULT_V_HOSTMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecexp").value(hasItem(DEFAULT_D_FECEXP.toString())))
            .andExpect(jsonPath("$.[*].vNumleg").value(hasItem(DEFAULT_V_NUMLEG.toString())))
            .andExpect(jsonPath("$.[*].nCodsuc").value(hasItem(DEFAULT_N_CODSUC)))
            .andExpect(jsonPath("$.[*].vFlginst").value(hasItem(DEFAULT_V_FLGINST.toString())))
            .andExpect(jsonPath("$.[*].vNumexpda").value(hasItem(DEFAULT_V_NUMEXPDA.toString())))
            .andExpect(jsonPath("$.[*].dFecexpda").value(hasItem(DEFAULT_D_FECEXPDA.toString())))
            .andExpect(jsonPath("$.[*].vNomjuzda").value(hasItem(DEFAULT_V_NOMJUZDA.toString())))
            .andExpect(jsonPath("$.[*].vNomjuezda").value(hasItem(DEFAULT_V_NOMJUEZDA.toString())))
            .andExpect(jsonPath("$.[*].vCodlegrig").value(hasItem(DEFAULT_V_CODLEGRIG.toString())))
            .andExpect(jsonPath("$.[*].vTiporig").value(hasItem(DEFAULT_V_TIPORIG.toString())))
            .andExpect(jsonPath("$.[*].vFlgarch").value(hasItem(DEFAULT_V_FLGARCH.toString())))
            .andExpect(jsonPath("$.[*].vFlgcaut").value(hasItem(DEFAULT_V_FLGCAUT.toString())))
            .andExpect(jsonPath("$.[*].vFlganul").value(hasItem(DEFAULT_V_FLGANUL.toString())))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Legajo.class);
        Legajo legajo1 = new Legajo();
        legajo1.setId(1L);
        Legajo legajo2 = new Legajo();
        legajo2.setId(legajo1.getId());
        assertThat(legajo1).isEqualTo(legajo2);
        legajo2.setId(2L);
        assertThat(legajo1).isNotEqualTo(legajo2);
        legajo1.setId(null);
        assertThat(legajo1).isNotEqualTo(legajo2);
    }
}
