package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Multaconci;
import pe.gob.trabajo.repository.MultaconciRepository;
import pe.gob.trabajo.repository.search.MultaconciSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MultaconciResource REST controller.
 *
 * @see MultaconciResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class MultaconciResourceIntTest {

    private static final Float DEFAULT_F_MONMULTA = 1F;
    private static final Float UPDATED_F_MONMULTA = 2F;

    private static final String DEFAULT_V_NUMRESOSD = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMRESOSD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECRESOSD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECRESOSD = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_CODIGO = "AAAAAAAAAA";
    private static final String UPDATED_V_CODIGO = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private MultaconciRepository multaconciRepository;

    @Autowired
    private MultaconciSearchRepository multaconciSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMultaconciMockMvc;

    private Multaconci multaconci;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MultaconciResource multaconciResource = new MultaconciResource(multaconciRepository, multaconciSearchRepository);
        this.restMultaconciMockMvc = MockMvcBuilders.standaloneSetup(multaconciResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Multaconci createEntity(EntityManager em) {
        Multaconci multaconci = new Multaconci()
            .fMonmulta(DEFAULT_F_MONMULTA)
            .vNumresosd(DEFAULT_V_NUMRESOSD)
            .dFecresosd(DEFAULT_D_FECRESOSD)
            .vCodigo(DEFAULT_V_CODIGO)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return multaconci;
    }

    @Before
    public void initTest() {
        multaconciSearchRepository.deleteAll();
        multaconci = createEntity(em);
    }

    @Test
    @Transactional
    public void createMultaconci() throws Exception {
        int databaseSizeBeforeCreate = multaconciRepository.findAll().size();

        // Create the Multaconci
        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isCreated());

        // Validate the Multaconci in the database
        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeCreate + 1);
        Multaconci testMultaconci = multaconciList.get(multaconciList.size() - 1);
        assertThat(testMultaconci.getfMonmulta()).isEqualTo(DEFAULT_F_MONMULTA);
        assertThat(testMultaconci.getvNumresosd()).isEqualTo(DEFAULT_V_NUMRESOSD);
        assertThat(testMultaconci.getdFecresosd()).isEqualTo(DEFAULT_D_FECRESOSD);
        assertThat(testMultaconci.getvCodigo()).isEqualTo(DEFAULT_V_CODIGO);
        assertThat(testMultaconci.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testMultaconci.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testMultaconci.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testMultaconci.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testMultaconci.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testMultaconci.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testMultaconci.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Multaconci in Elasticsearch
        Multaconci multaconciEs = multaconciSearchRepository.findOne(testMultaconci.getId());
        assertThat(multaconciEs).isEqualToComparingFieldByField(testMultaconci);
    }

    @Test
    @Transactional
    public void createMultaconciWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = multaconciRepository.findAll().size();

        // Create the Multaconci with an existing ID
        multaconci.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isBadRequest());

        // Validate the Multaconci in the database
        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkfMonmultaIsRequired() throws Exception {
        int databaseSizeBeforeTest = multaconciRepository.findAll().size();
        // set the field null
        multaconci.setfMonmulta(null);

        // Create the Multaconci, which fails.

        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isBadRequest());

        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = multaconciRepository.findAll().size();
        // set the field null
        multaconci.setnUsuareg(null);

        // Create the Multaconci, which fails.

        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isBadRequest());

        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = multaconciRepository.findAll().size();
        // set the field null
        multaconci.settFecreg(null);

        // Create the Multaconci, which fails.

        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isBadRequest());

        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = multaconciRepository.findAll().size();
        // set the field null
        multaconci.setnFlgactivo(null);

        // Create the Multaconci, which fails.

        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isBadRequest());

        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = multaconciRepository.findAll().size();
        // set the field null
        multaconci.setnSedereg(null);

        // Create the Multaconci, which fails.

        restMultaconciMockMvc.perform(post("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isBadRequest());

        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMultaconcis() throws Exception {
        // Initialize the database
        multaconciRepository.saveAndFlush(multaconci);

        // Get all the multaconciList
        restMultaconciMockMvc.perform(get("/api/multaconcis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(multaconci.getId().intValue())))
            .andExpect(jsonPath("$.[*].fMonmulta").value(hasItem(DEFAULT_F_MONMULTA.doubleValue())))
            .andExpect(jsonPath("$.[*].vNumresosd").value(hasItem(DEFAULT_V_NUMRESOSD.toString())))
            .andExpect(jsonPath("$.[*].dFecresosd").value(hasItem(DEFAULT_D_FECRESOSD.toString())))
            .andExpect(jsonPath("$.[*].vCodigo").value(hasItem(DEFAULT_V_CODIGO.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getMultaconci() throws Exception {
        // Initialize the database
        multaconciRepository.saveAndFlush(multaconci);

        // Get the multaconci
        restMultaconciMockMvc.perform(get("/api/multaconcis/{id}", multaconci.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(multaconci.getId().intValue()))
            .andExpect(jsonPath("$.fMonmulta").value(DEFAULT_F_MONMULTA.doubleValue()))
            .andExpect(jsonPath("$.vNumresosd").value(DEFAULT_V_NUMRESOSD.toString()))
            .andExpect(jsonPath("$.dFecresosd").value(DEFAULT_D_FECRESOSD.toString()))
            .andExpect(jsonPath("$.vCodigo").value(DEFAULT_V_CODIGO.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingMultaconci() throws Exception {
        // Get the multaconci
        restMultaconciMockMvc.perform(get("/api/multaconcis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMultaconci() throws Exception {
        // Initialize the database
        multaconciRepository.saveAndFlush(multaconci);
        multaconciSearchRepository.save(multaconci);
        int databaseSizeBeforeUpdate = multaconciRepository.findAll().size();

        // Update the multaconci
        Multaconci updatedMultaconci = multaconciRepository.findOne(multaconci.getId());
        updatedMultaconci
            .fMonmulta(UPDATED_F_MONMULTA)
            .vNumresosd(UPDATED_V_NUMRESOSD)
            .dFecresosd(UPDATED_D_FECRESOSD)
            .vCodigo(UPDATED_V_CODIGO)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restMultaconciMockMvc.perform(put("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMultaconci)))
            .andExpect(status().isOk());

        // Validate the Multaconci in the database
        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeUpdate);
        Multaconci testMultaconci = multaconciList.get(multaconciList.size() - 1);
        assertThat(testMultaconci.getfMonmulta()).isEqualTo(UPDATED_F_MONMULTA);
        assertThat(testMultaconci.getvNumresosd()).isEqualTo(UPDATED_V_NUMRESOSD);
        assertThat(testMultaconci.getdFecresosd()).isEqualTo(UPDATED_D_FECRESOSD);
        assertThat(testMultaconci.getvCodigo()).isEqualTo(UPDATED_V_CODIGO);
        assertThat(testMultaconci.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testMultaconci.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testMultaconci.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testMultaconci.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testMultaconci.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testMultaconci.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testMultaconci.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Multaconci in Elasticsearch
        Multaconci multaconciEs = multaconciSearchRepository.findOne(testMultaconci.getId());
        assertThat(multaconciEs).isEqualToComparingFieldByField(testMultaconci);
    }

    @Test
    @Transactional
    public void updateNonExistingMultaconci() throws Exception {
        int databaseSizeBeforeUpdate = multaconciRepository.findAll().size();

        // Create the Multaconci

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMultaconciMockMvc.perform(put("/api/multaconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multaconci)))
            .andExpect(status().isCreated());

        // Validate the Multaconci in the database
        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMultaconci() throws Exception {
        // Initialize the database
        multaconciRepository.saveAndFlush(multaconci);
        multaconciSearchRepository.save(multaconci);
        int databaseSizeBeforeDelete = multaconciRepository.findAll().size();

        // Get the multaconci
        restMultaconciMockMvc.perform(delete("/api/multaconcis/{id}", multaconci.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean multaconciExistsInEs = multaconciSearchRepository.exists(multaconci.getId());
        assertThat(multaconciExistsInEs).isFalse();

        // Validate the database is empty
        List<Multaconci> multaconciList = multaconciRepository.findAll();
        assertThat(multaconciList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMultaconci() throws Exception {
        // Initialize the database
        multaconciRepository.saveAndFlush(multaconci);
        multaconciSearchRepository.save(multaconci);

        // Search the multaconci
        restMultaconciMockMvc.perform(get("/api/_search/multaconcis?query=id:" + multaconci.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(multaconci.getId().intValue())))
            .andExpect(jsonPath("$.[*].fMonmulta").value(hasItem(DEFAULT_F_MONMULTA.doubleValue())))
            .andExpect(jsonPath("$.[*].vNumresosd").value(hasItem(DEFAULT_V_NUMRESOSD.toString())))
            .andExpect(jsonPath("$.[*].dFecresosd").value(hasItem(DEFAULT_D_FECRESOSD.toString())))
            .andExpect(jsonPath("$.[*].vCodigo").value(hasItem(DEFAULT_V_CODIGO.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Multaconci.class);
        Multaconci multaconci1 = new Multaconci();
        multaconci1.setId(1L);
        Multaconci multaconci2 = new Multaconci();
        multaconci2.setId(multaconci1.getId());
        assertThat(multaconci1).isEqualTo(multaconci2);
        multaconci2.setId(2L);
        assertThat(multaconci1).isNotEqualTo(multaconci2);
        multaconci1.setId(null);
        assertThat(multaconci1).isNotEqualTo(multaconci2);
    }
}
