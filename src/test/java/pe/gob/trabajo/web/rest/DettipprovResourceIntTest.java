package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Dettipprov;
import pe.gob.trabajo.repository.DettipprovRepository;
import pe.gob.trabajo.repository.search.DettipprovSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DettipprovResource REST controller.
 *
 * @see DettipprovResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class DettipprovResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DettipprovRepository dettipprovRepository;

    @Autowired
    private DettipprovSearchRepository dettipprovSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDettipprovMockMvc;

    private Dettipprov dettipprov;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DettipprovResource dettipprovResource = new DettipprovResource(dettipprovRepository, dettipprovSearchRepository);
        this.restDettipprovMockMvc = MockMvcBuilders.standaloneSetup(dettipprovResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dettipprov createEntity(EntityManager em) {
        Dettipprov dettipprov = new Dettipprov()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return dettipprov;
    }

    @Before
    public void initTest() {
        dettipprovSearchRepository.deleteAll();
        dettipprov = createEntity(em);
    }

    @Test
    @Transactional
    public void createDettipprov() throws Exception {
        int databaseSizeBeforeCreate = dettipprovRepository.findAll().size();

        // Create the Dettipprov
        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isCreated());

        // Validate the Dettipprov in the database
        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeCreate + 1);
        Dettipprov testDettipprov = dettipprovList.get(dettipprovList.size() - 1);
        assertThat(testDettipprov.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testDettipprov.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testDettipprov.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDettipprov.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDettipprov.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDettipprov.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testDettipprov.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDettipprov.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Dettipprov in Elasticsearch
        Dettipprov dettipprovEs = dettipprovSearchRepository.findOne(testDettipprov.getId());
        assertThat(dettipprovEs).isEqualToComparingFieldByField(testDettipprov);
    }

    @Test
    @Transactional
    public void createDettipprovWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dettipprovRepository.findAll().size();

        // Create the Dettipprov with an existing ID
        dettipprov.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isBadRequest());

        // Validate the Dettipprov in the database
        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = dettipprovRepository.findAll().size();
        // set the field null
        dettipprov.setvDescrip(null);

        // Create the Dettipprov, which fails.

        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isBadRequest());

        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = dettipprovRepository.findAll().size();
        // set the field null
        dettipprov.setnUsuareg(null);

        // Create the Dettipprov, which fails.

        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isBadRequest());

        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = dettipprovRepository.findAll().size();
        // set the field null
        dettipprov.settFecreg(null);

        // Create the Dettipprov, which fails.

        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isBadRequest());

        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = dettipprovRepository.findAll().size();
        // set the field null
        dettipprov.setnFlgactivo(null);

        // Create the Dettipprov, which fails.

        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isBadRequest());

        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = dettipprovRepository.findAll().size();
        // set the field null
        dettipprov.setnSedereg(null);

        // Create the Dettipprov, which fails.

        restDettipprovMockMvc.perform(post("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isBadRequest());

        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDettipprovs() throws Exception {
        // Initialize the database
        dettipprovRepository.saveAndFlush(dettipprov);

        // Get all the dettipprovList
        restDettipprovMockMvc.perform(get("/api/dettipprovs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dettipprov.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDettipprov() throws Exception {
        // Initialize the database
        dettipprovRepository.saveAndFlush(dettipprov);

        // Get the dettipprov
        restDettipprovMockMvc.perform(get("/api/dettipprovs/{id}", dettipprov.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dettipprov.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDettipprov() throws Exception {
        // Get the dettipprov
        restDettipprovMockMvc.perform(get("/api/dettipprovs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDettipprov() throws Exception {
        // Initialize the database
        dettipprovRepository.saveAndFlush(dettipprov);
        dettipprovSearchRepository.save(dettipprov);
        int databaseSizeBeforeUpdate = dettipprovRepository.findAll().size();

        // Update the dettipprov
        Dettipprov updatedDettipprov = dettipprovRepository.findOne(dettipprov.getId());
        updatedDettipprov
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDettipprovMockMvc.perform(put("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDettipprov)))
            .andExpect(status().isOk());

        // Validate the Dettipprov in the database
        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeUpdate);
        Dettipprov testDettipprov = dettipprovList.get(dettipprovList.size() - 1);
        assertThat(testDettipprov.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testDettipprov.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testDettipprov.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDettipprov.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDettipprov.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDettipprov.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testDettipprov.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDettipprov.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Dettipprov in Elasticsearch
        Dettipprov dettipprovEs = dettipprovSearchRepository.findOne(testDettipprov.getId());
        assertThat(dettipprovEs).isEqualToComparingFieldByField(testDettipprov);
    }

    @Test
    @Transactional
    public void updateNonExistingDettipprov() throws Exception {
        int databaseSizeBeforeUpdate = dettipprovRepository.findAll().size();

        // Create the Dettipprov

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDettipprovMockMvc.perform(put("/api/dettipprovs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dettipprov)))
            .andExpect(status().isCreated());

        // Validate the Dettipprov in the database
        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDettipprov() throws Exception {
        // Initialize the database
        dettipprovRepository.saveAndFlush(dettipprov);
        dettipprovSearchRepository.save(dettipprov);
        int databaseSizeBeforeDelete = dettipprovRepository.findAll().size();

        // Get the dettipprov
        restDettipprovMockMvc.perform(delete("/api/dettipprovs/{id}", dettipprov.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dettipprovExistsInEs = dettipprovSearchRepository.exists(dettipprov.getId());
        assertThat(dettipprovExistsInEs).isFalse();

        // Validate the database is empty
        List<Dettipprov> dettipprovList = dettipprovRepository.findAll();
        assertThat(dettipprovList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDettipprov() throws Exception {
        // Initialize the database
        dettipprovRepository.saveAndFlush(dettipprov);
        dettipprovSearchRepository.save(dettipprov);

        // Search the dettipprov
        restDettipprovMockMvc.perform(get("/api/_search/dettipprovs?query=id:" + dettipprov.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dettipprov.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dettipprov.class);
        Dettipprov dettipprov1 = new Dettipprov();
        dettipprov1.setId(1L);
        Dettipprov dettipprov2 = new Dettipprov();
        dettipprov2.setId(dettipprov1.getId());
        assertThat(dettipprov1).isEqualTo(dettipprov2);
        dettipprov2.setId(2L);
        assertThat(dettipprov1).isNotEqualTo(dettipprov2);
        dettipprov1.setId(null);
        assertThat(dettipprov1).isNotEqualTo(dettipprov2);
    }
}
