package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Pasepj;
import pe.gob.trabajo.repository.PasepjRepository;
import pe.gob.trabajo.repository.search.PasepjSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PasepjResource REST controller.
 *
 * @see PasepjResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class PasepjResourceIntTest {

    private static final String DEFAULT_V_CODREG = "AA";
    private static final String UPDATED_V_CODREG = "BB";

    private static final String DEFAULT_V_CODZON = "AA";
    private static final String UPDATED_V_CODZON = "BB";

    private static final String DEFAULT_V_CODTDCIDE = "AA";
    private static final String UPDATED_V_CODTDCIDE = "BB";

    private static final Integer DEFAULT_N_CORREL = 1;
    private static final Integer UPDATED_N_CORREL = 2;

    private static final String DEFAULT_V_CODSIS = "AAA";
    private static final String UPDATED_V_CODSIS = "BBB";

    private static final Integer DEFAULT_N_CODSUC = 1;
    private static final Integer UPDATED_N_CODSUC = 2;

    private static final String DEFAULT_V_CODCON = "AAAAAAAAAA";
    private static final String UPDATED_V_CODCON = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECPAS = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECPAS = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_D_FECRECEP = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECRECEP = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_CODUSUREC = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUREC = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODUSUREG = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUREG = "BBBBBBBBBB";

    private static final String DEFAULT_V_HOSTREG = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTREG = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODUSUMOD = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUMOD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECMOD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECMOD = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_HOSTMOD = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTMOD = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODSISDES = "AAA";
    private static final String UPDATED_V_CODSISDES = "BBB";

    private static final LocalDate DEFAULT_D_FECDES = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECDES = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_D_FECCON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECCON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_CODTRAR = "AAAAAAAAAA";
    private static final String UPDATED_V_CODTRAR = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODTDCIDR = "AA";
    private static final String UPDATED_V_CODTDCIDR = "BB";

    private static final Integer DEFAULT_N_CORRELR = 1;
    private static final Integer UPDATED_N_CORRELR = 2;

    private static final String DEFAULT_V_OBSPAS = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSPAS = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODTRAC = "AAAAAAAAAA";
    private static final String UPDATED_V_CODTRAC = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODTDCIDI = "AA";
    private static final String UPDATED_V_CODTDCIDI = "BB";

    private static final Integer DEFAULT_N_CORRELC = 1;
    private static final Integer UPDATED_N_CORRELC = 2;

    private static final String DEFAULT_V_CODLOC = "AAAAAA";
    private static final String UPDATED_V_CODLOC = "BBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private PasepjRepository pasepjRepository;

    @Autowired
    private PasepjSearchRepository pasepjSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPasepjMockMvc;

    private Pasepj pasepj;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PasepjResource pasepjResource = new PasepjResource(pasepjRepository, pasepjSearchRepository);
        this.restPasepjMockMvc = MockMvcBuilders.standaloneSetup(pasepjResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pasepj createEntity(EntityManager em) {
        Pasepj pasepj = new Pasepj()
            .vCodreg(DEFAULT_V_CODREG)
            .vCodzon(DEFAULT_V_CODZON)
            .vCodtdcide(DEFAULT_V_CODTDCIDE)
            .nCorrel(DEFAULT_N_CORREL)
            .vCodsis(DEFAULT_V_CODSIS)
            .nCodsuc(DEFAULT_N_CODSUC)
            .vCodcon(DEFAULT_V_CODCON)
            .dFecpas(DEFAULT_D_FECPAS)
            .dFecrecep(DEFAULT_D_FECRECEP)
            .vCodusurec(DEFAULT_V_CODUSUREC)
            .vCodusureg(DEFAULT_V_CODUSUREG)
            .vHostreg(DEFAULT_V_HOSTREG)
            .vCodusumod(DEFAULT_V_CODUSUMOD)
            .dFecmod(DEFAULT_D_FECMOD)
            .vHostmod(DEFAULT_V_HOSTMOD)
            .vCodsisdes(DEFAULT_V_CODSISDES)
            .dFecdes(DEFAULT_D_FECDES)
            .dFeccon(DEFAULT_D_FECCON)
            .vCodtrar(DEFAULT_V_CODTRAR)
            .vCodtdcidr(DEFAULT_V_CODTDCIDR)
            .nCorrelr(DEFAULT_N_CORRELR)
            .vObspas(DEFAULT_V_OBSPAS)
            .vCodtrac(DEFAULT_V_CODTRAC)
            .vCodtdcidi(DEFAULT_V_CODTDCIDI)
            .nCorrelc(DEFAULT_N_CORRELC)
            .vCodloc(DEFAULT_V_CODLOC)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return pasepj;
    }

    @Before
    public void initTest() {
        pasepjSearchRepository.deleteAll();
        pasepj = createEntity(em);
    }

    @Test
    @Transactional
    public void createPasepj() throws Exception {
        int databaseSizeBeforeCreate = pasepjRepository.findAll().size();

        // Create the Pasepj
        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isCreated());

        // Validate the Pasepj in the database
        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeCreate + 1);
        Pasepj testPasepj = pasepjList.get(pasepjList.size() - 1);
        assertThat(testPasepj.getvCodreg()).isEqualTo(DEFAULT_V_CODREG);
        assertThat(testPasepj.getvCodzon()).isEqualTo(DEFAULT_V_CODZON);
        assertThat(testPasepj.getvCodtdcide()).isEqualTo(DEFAULT_V_CODTDCIDE);
        assertThat(testPasepj.getnCorrel()).isEqualTo(DEFAULT_N_CORREL);
        assertThat(testPasepj.getvCodsis()).isEqualTo(DEFAULT_V_CODSIS);
        assertThat(testPasepj.getnCodsuc()).isEqualTo(DEFAULT_N_CODSUC);
        assertThat(testPasepj.getvCodcon()).isEqualTo(DEFAULT_V_CODCON);
        assertThat(testPasepj.getdFecpas()).isEqualTo(DEFAULT_D_FECPAS);
        assertThat(testPasepj.getdFecrecep()).isEqualTo(DEFAULT_D_FECRECEP);
        assertThat(testPasepj.getvCodusurec()).isEqualTo(DEFAULT_V_CODUSUREC);
        assertThat(testPasepj.getvCodusureg()).isEqualTo(DEFAULT_V_CODUSUREG);
        assertThat(testPasepj.getvHostreg()).isEqualTo(DEFAULT_V_HOSTREG);
        assertThat(testPasepj.getvCodusumod()).isEqualTo(DEFAULT_V_CODUSUMOD);
        assertThat(testPasepj.getdFecmod()).isEqualTo(DEFAULT_D_FECMOD);
        assertThat(testPasepj.getvHostmod()).isEqualTo(DEFAULT_V_HOSTMOD);
        assertThat(testPasepj.getvCodsisdes()).isEqualTo(DEFAULT_V_CODSISDES);
        assertThat(testPasepj.getdFecdes()).isEqualTo(DEFAULT_D_FECDES);
        assertThat(testPasepj.getdFeccon()).isEqualTo(DEFAULT_D_FECCON);
        assertThat(testPasepj.getvCodtrar()).isEqualTo(DEFAULT_V_CODTRAR);
        assertThat(testPasepj.getvCodtdcidr()).isEqualTo(DEFAULT_V_CODTDCIDR);
        assertThat(testPasepj.getnCorrelr()).isEqualTo(DEFAULT_N_CORRELR);
        assertThat(testPasepj.getvObspas()).isEqualTo(DEFAULT_V_OBSPAS);
        assertThat(testPasepj.getvCodtrac()).isEqualTo(DEFAULT_V_CODTRAC);
        assertThat(testPasepj.getvCodtdcidi()).isEqualTo(DEFAULT_V_CODTDCIDI);
        assertThat(testPasepj.getnCorrelc()).isEqualTo(DEFAULT_N_CORRELC);
        assertThat(testPasepj.getvCodloc()).isEqualTo(DEFAULT_V_CODLOC);
        assertThat(testPasepj.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testPasepj.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testPasepj.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testPasepj.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testPasepj.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testPasepj.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testPasepj.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Pasepj in Elasticsearch
        Pasepj pasepjEs = pasepjSearchRepository.findOne(testPasepj.getId());
        assertThat(pasepjEs).isEqualToComparingFieldByField(testPasepj);
    }

    @Test
    @Transactional
    public void createPasepjWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pasepjRepository.findAll().size();

        // Create the Pasepj with an existing ID
        pasepj.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        // Validate the Pasepj in the database
        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodregIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodreg(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodzonIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodzon(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodtdcideIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodtdcide(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCorrelIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnCorrel(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodsisIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodsis(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCodsucIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnCodsuc(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodconIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodcon(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusurecIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodusurec(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusuregIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodusureg(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostregIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvHostreg(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusumodIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodusumod(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostmodIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvHostmod(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodsisdesIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodsisdes(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodtrarIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodtrar(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodtdcidrIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodtdcidr(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCorrelrIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnCorrelr(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvObspasIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvObspas(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodtracIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodtrac(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodtdcidiIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodtdcidi(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCorrelcIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnCorrelc(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodlocIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setvCodloc(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnUsuareg(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.settFecreg(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnFlgactivo(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = pasepjRepository.findAll().size();
        // set the field null
        pasepj.setnSedereg(null);

        // Create the Pasepj, which fails.

        restPasepjMockMvc.perform(post("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isBadRequest());

        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPasepjs() throws Exception {
        // Initialize the database
        pasepjRepository.saveAndFlush(pasepj);

        // Get all the pasepjList
        restPasepjMockMvc.perform(get("/api/pasepjs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pasepj.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].vCodtdcide").value(hasItem(DEFAULT_V_CODTDCIDE.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].nCodsuc").value(hasItem(DEFAULT_N_CODSUC)))
            .andExpect(jsonPath("$.[*].vCodcon").value(hasItem(DEFAULT_V_CODCON.toString())))
            .andExpect(jsonPath("$.[*].dFecpas").value(hasItem(DEFAULT_D_FECPAS.toString())))
            .andExpect(jsonPath("$.[*].dFecrecep").value(hasItem(DEFAULT_D_FECRECEP.toString())))
            .andExpect(jsonPath("$.[*].vCodusurec").value(hasItem(DEFAULT_V_CODUSUREC.toString())))
            .andExpect(jsonPath("$.[*].vCodusureg").value(hasItem(DEFAULT_V_CODUSUREG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].vCodusumod").value(hasItem(DEFAULT_V_CODUSUMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecmod").value(hasItem(DEFAULT_D_FECMOD.toString())))
            .andExpect(jsonPath("$.[*].vHostmod").value(hasItem(DEFAULT_V_HOSTMOD.toString())))
            .andExpect(jsonPath("$.[*].vCodsisdes").value(hasItem(DEFAULT_V_CODSISDES.toString())))
            .andExpect(jsonPath("$.[*].dFecdes").value(hasItem(DEFAULT_D_FECDES.toString())))
            .andExpect(jsonPath("$.[*].dFeccon").value(hasItem(DEFAULT_D_FECCON.toString())))
            .andExpect(jsonPath("$.[*].vCodtrar").value(hasItem(DEFAULT_V_CODTRAR.toString())))
            .andExpect(jsonPath("$.[*].vCodtdcidr").value(hasItem(DEFAULT_V_CODTDCIDR.toString())))
            .andExpect(jsonPath("$.[*].nCorrelr").value(hasItem(DEFAULT_N_CORRELR)))
            .andExpect(jsonPath("$.[*].vObspas").value(hasItem(DEFAULT_V_OBSPAS.toString())))
            .andExpect(jsonPath("$.[*].vCodtrac").value(hasItem(DEFAULT_V_CODTRAC.toString())))
            .andExpect(jsonPath("$.[*].vCodtdcidi").value(hasItem(DEFAULT_V_CODTDCIDI.toString())))
            .andExpect(jsonPath("$.[*].nCorrelc").value(hasItem(DEFAULT_N_CORRELC)))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getPasepj() throws Exception {
        // Initialize the database
        pasepjRepository.saveAndFlush(pasepj);

        // Get the pasepj
        restPasepjMockMvc.perform(get("/api/pasepjs/{id}", pasepj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pasepj.getId().intValue()))
            .andExpect(jsonPath("$.vCodreg").value(DEFAULT_V_CODREG.toString()))
            .andExpect(jsonPath("$.vCodzon").value(DEFAULT_V_CODZON.toString()))
            .andExpect(jsonPath("$.vCodtdcide").value(DEFAULT_V_CODTDCIDE.toString()))
            .andExpect(jsonPath("$.nCorrel").value(DEFAULT_N_CORREL))
            .andExpect(jsonPath("$.vCodsis").value(DEFAULT_V_CODSIS.toString()))
            .andExpect(jsonPath("$.nCodsuc").value(DEFAULT_N_CODSUC))
            .andExpect(jsonPath("$.vCodcon").value(DEFAULT_V_CODCON.toString()))
            .andExpect(jsonPath("$.dFecpas").value(DEFAULT_D_FECPAS.toString()))
            .andExpect(jsonPath("$.dFecrecep").value(DEFAULT_D_FECRECEP.toString()))
            .andExpect(jsonPath("$.vCodusurec").value(DEFAULT_V_CODUSUREC.toString()))
            .andExpect(jsonPath("$.vCodusureg").value(DEFAULT_V_CODUSUREG.toString()))
            .andExpect(jsonPath("$.vHostreg").value(DEFAULT_V_HOSTREG.toString()))
            .andExpect(jsonPath("$.vCodusumod").value(DEFAULT_V_CODUSUMOD.toString()))
            .andExpect(jsonPath("$.dFecmod").value(DEFAULT_D_FECMOD.toString()))
            .andExpect(jsonPath("$.vHostmod").value(DEFAULT_V_HOSTMOD.toString()))
            .andExpect(jsonPath("$.vCodsisdes").value(DEFAULT_V_CODSISDES.toString()))
            .andExpect(jsonPath("$.dFecdes").value(DEFAULT_D_FECDES.toString()))
            .andExpect(jsonPath("$.dFeccon").value(DEFAULT_D_FECCON.toString()))
            .andExpect(jsonPath("$.vCodtrar").value(DEFAULT_V_CODTRAR.toString()))
            .andExpect(jsonPath("$.vCodtdcidr").value(DEFAULT_V_CODTDCIDR.toString()))
            .andExpect(jsonPath("$.nCorrelr").value(DEFAULT_N_CORRELR))
            .andExpect(jsonPath("$.vObspas").value(DEFAULT_V_OBSPAS.toString()))
            .andExpect(jsonPath("$.vCodtrac").value(DEFAULT_V_CODTRAC.toString()))
            .andExpect(jsonPath("$.vCodtdcidi").value(DEFAULT_V_CODTDCIDI.toString()))
            .andExpect(jsonPath("$.nCorrelc").value(DEFAULT_N_CORRELC))
            .andExpect(jsonPath("$.vCodloc").value(DEFAULT_V_CODLOC.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingPasepj() throws Exception {
        // Get the pasepj
        restPasepjMockMvc.perform(get("/api/pasepjs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePasepj() throws Exception {
        // Initialize the database
        pasepjRepository.saveAndFlush(pasepj);
        pasepjSearchRepository.save(pasepj);
        int databaseSizeBeforeUpdate = pasepjRepository.findAll().size();

        // Update the pasepj
        Pasepj updatedPasepj = pasepjRepository.findOne(pasepj.getId());
        updatedPasepj
            .vCodreg(UPDATED_V_CODREG)
            .vCodzon(UPDATED_V_CODZON)
            .vCodtdcide(UPDATED_V_CODTDCIDE)
            .nCorrel(UPDATED_N_CORREL)
            .vCodsis(UPDATED_V_CODSIS)
            .nCodsuc(UPDATED_N_CODSUC)
            .vCodcon(UPDATED_V_CODCON)
            .dFecpas(UPDATED_D_FECPAS)
            .dFecrecep(UPDATED_D_FECRECEP)
            .vCodusurec(UPDATED_V_CODUSUREC)
            .vCodusureg(UPDATED_V_CODUSUREG)
            .vHostreg(UPDATED_V_HOSTREG)
            .vCodusumod(UPDATED_V_CODUSUMOD)
            .dFecmod(UPDATED_D_FECMOD)
            .vHostmod(UPDATED_V_HOSTMOD)
            .vCodsisdes(UPDATED_V_CODSISDES)
            .dFecdes(UPDATED_D_FECDES)
            .dFeccon(UPDATED_D_FECCON)
            .vCodtrar(UPDATED_V_CODTRAR)
            .vCodtdcidr(UPDATED_V_CODTDCIDR)
            .nCorrelr(UPDATED_N_CORRELR)
            .vObspas(UPDATED_V_OBSPAS)
            .vCodtrac(UPDATED_V_CODTRAC)
            .vCodtdcidi(UPDATED_V_CODTDCIDI)
            .nCorrelc(UPDATED_N_CORRELC)
            .vCodloc(UPDATED_V_CODLOC)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restPasepjMockMvc.perform(put("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPasepj)))
            .andExpect(status().isOk());

        // Validate the Pasepj in the database
        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeUpdate);
        Pasepj testPasepj = pasepjList.get(pasepjList.size() - 1);
        assertThat(testPasepj.getvCodreg()).isEqualTo(UPDATED_V_CODREG);
        assertThat(testPasepj.getvCodzon()).isEqualTo(UPDATED_V_CODZON);
        assertThat(testPasepj.getvCodtdcide()).isEqualTo(UPDATED_V_CODTDCIDE);
        assertThat(testPasepj.getnCorrel()).isEqualTo(UPDATED_N_CORREL);
        assertThat(testPasepj.getvCodsis()).isEqualTo(UPDATED_V_CODSIS);
        assertThat(testPasepj.getnCodsuc()).isEqualTo(UPDATED_N_CODSUC);
        assertThat(testPasepj.getvCodcon()).isEqualTo(UPDATED_V_CODCON);
        assertThat(testPasepj.getdFecpas()).isEqualTo(UPDATED_D_FECPAS);
        assertThat(testPasepj.getdFecrecep()).isEqualTo(UPDATED_D_FECRECEP);
        assertThat(testPasepj.getvCodusurec()).isEqualTo(UPDATED_V_CODUSUREC);
        assertThat(testPasepj.getvCodusureg()).isEqualTo(UPDATED_V_CODUSUREG);
        assertThat(testPasepj.getvHostreg()).isEqualTo(UPDATED_V_HOSTREG);
        assertThat(testPasepj.getvCodusumod()).isEqualTo(UPDATED_V_CODUSUMOD);
        assertThat(testPasepj.getdFecmod()).isEqualTo(UPDATED_D_FECMOD);
        assertThat(testPasepj.getvHostmod()).isEqualTo(UPDATED_V_HOSTMOD);
        assertThat(testPasepj.getvCodsisdes()).isEqualTo(UPDATED_V_CODSISDES);
        assertThat(testPasepj.getdFecdes()).isEqualTo(UPDATED_D_FECDES);
        assertThat(testPasepj.getdFeccon()).isEqualTo(UPDATED_D_FECCON);
        assertThat(testPasepj.getvCodtrar()).isEqualTo(UPDATED_V_CODTRAR);
        assertThat(testPasepj.getvCodtdcidr()).isEqualTo(UPDATED_V_CODTDCIDR);
        assertThat(testPasepj.getnCorrelr()).isEqualTo(UPDATED_N_CORRELR);
        assertThat(testPasepj.getvObspas()).isEqualTo(UPDATED_V_OBSPAS);
        assertThat(testPasepj.getvCodtrac()).isEqualTo(UPDATED_V_CODTRAC);
        assertThat(testPasepj.getvCodtdcidi()).isEqualTo(UPDATED_V_CODTDCIDI);
        assertThat(testPasepj.getnCorrelc()).isEqualTo(UPDATED_N_CORRELC);
        assertThat(testPasepj.getvCodloc()).isEqualTo(UPDATED_V_CODLOC);
        assertThat(testPasepj.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testPasepj.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testPasepj.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testPasepj.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testPasepj.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testPasepj.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testPasepj.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Pasepj in Elasticsearch
        Pasepj pasepjEs = pasepjSearchRepository.findOne(testPasepj.getId());
        assertThat(pasepjEs).isEqualToComparingFieldByField(testPasepj);
    }

    @Test
    @Transactional
    public void updateNonExistingPasepj() throws Exception {
        int databaseSizeBeforeUpdate = pasepjRepository.findAll().size();

        // Create the Pasepj

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPasepjMockMvc.perform(put("/api/pasepjs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pasepj)))
            .andExpect(status().isCreated());

        // Validate the Pasepj in the database
        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePasepj() throws Exception {
        // Initialize the database
        pasepjRepository.saveAndFlush(pasepj);
        pasepjSearchRepository.save(pasepj);
        int databaseSizeBeforeDelete = pasepjRepository.findAll().size();

        // Get the pasepj
        restPasepjMockMvc.perform(delete("/api/pasepjs/{id}", pasepj.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean pasepjExistsInEs = pasepjSearchRepository.exists(pasepj.getId());
        assertThat(pasepjExistsInEs).isFalse();

        // Validate the database is empty
        List<Pasepj> pasepjList = pasepjRepository.findAll();
        assertThat(pasepjList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPasepj() throws Exception {
        // Initialize the database
        pasepjRepository.saveAndFlush(pasepj);
        pasepjSearchRepository.save(pasepj);

        // Search the pasepj
        restPasepjMockMvc.perform(get("/api/_search/pasepjs?query=id:" + pasepj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pasepj.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].vCodtdcide").value(hasItem(DEFAULT_V_CODTDCIDE.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].nCodsuc").value(hasItem(DEFAULT_N_CODSUC)))
            .andExpect(jsonPath("$.[*].vCodcon").value(hasItem(DEFAULT_V_CODCON.toString())))
            .andExpect(jsonPath("$.[*].dFecpas").value(hasItem(DEFAULT_D_FECPAS.toString())))
            .andExpect(jsonPath("$.[*].dFecrecep").value(hasItem(DEFAULT_D_FECRECEP.toString())))
            .andExpect(jsonPath("$.[*].vCodusurec").value(hasItem(DEFAULT_V_CODUSUREC.toString())))
            .andExpect(jsonPath("$.[*].vCodusureg").value(hasItem(DEFAULT_V_CODUSUREG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].vCodusumod").value(hasItem(DEFAULT_V_CODUSUMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecmod").value(hasItem(DEFAULT_D_FECMOD.toString())))
            .andExpect(jsonPath("$.[*].vHostmod").value(hasItem(DEFAULT_V_HOSTMOD.toString())))
            .andExpect(jsonPath("$.[*].vCodsisdes").value(hasItem(DEFAULT_V_CODSISDES.toString())))
            .andExpect(jsonPath("$.[*].dFecdes").value(hasItem(DEFAULT_D_FECDES.toString())))
            .andExpect(jsonPath("$.[*].dFeccon").value(hasItem(DEFAULT_D_FECCON.toString())))
            .andExpect(jsonPath("$.[*].vCodtrar").value(hasItem(DEFAULT_V_CODTRAR.toString())))
            .andExpect(jsonPath("$.[*].vCodtdcidr").value(hasItem(DEFAULT_V_CODTDCIDR.toString())))
            .andExpect(jsonPath("$.[*].nCorrelr").value(hasItem(DEFAULT_N_CORRELR)))
            .andExpect(jsonPath("$.[*].vObspas").value(hasItem(DEFAULT_V_OBSPAS.toString())))
            .andExpect(jsonPath("$.[*].vCodtrac").value(hasItem(DEFAULT_V_CODTRAC.toString())))
            .andExpect(jsonPath("$.[*].vCodtdcidi").value(hasItem(DEFAULT_V_CODTDCIDI.toString())))
            .andExpect(jsonPath("$.[*].nCorrelc").value(hasItem(DEFAULT_N_CORRELC)))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pasepj.class);
        Pasepj pasepj1 = new Pasepj();
        pasepj1.setId(1L);
        Pasepj pasepj2 = new Pasepj();
        pasepj2.setId(pasepj1.getId());
        assertThat(pasepj1).isEqualTo(pasepj2);
        pasepj2.setId(2L);
        assertThat(pasepj1).isNotEqualTo(pasepj2);
        pasepj1.setId(null);
        assertThat(pasepj1).isNotEqualTo(pasepj2);
    }
}
